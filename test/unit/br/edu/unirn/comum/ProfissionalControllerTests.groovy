package br.edu.unirn.comum



import org.junit.*
import grails.test.mixin.*

@TestFor(ProfissionalController)
@Mock(Profissional)
class ProfissionalControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/profissional/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.profissionalInstanceList.size() == 0
        assert model.profissionalInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.profissionalInstance != null
    }

    void testSave() {
        controller.save()

        assert model.profissionalInstance != null
        assert view == '/profissional/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/profissional/show/1'
        assert controller.flash.message != null
        assert Profissional.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/profissional/list'

        populateValidParams(params)
        def profissional = new Profissional(params)

        assert profissional.save() != null

        params.id = profissional.id

        def model = controller.show()

        assert model.profissionalInstance == profissional
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/profissional/list'

        populateValidParams(params)
        def profissional = new Profissional(params)

        assert profissional.save() != null

        params.id = profissional.id

        def model = controller.edit()

        assert model.profissionalInstance == profissional
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/profissional/list'

        response.reset()

        populateValidParams(params)
        def profissional = new Profissional(params)

        assert profissional.save() != null

        // test invalid parameters in update
        params.id = profissional.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/profissional/edit"
        assert model.profissionalInstance != null

        profissional.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/profissional/show/$profissional.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        profissional.clearErrors()

        populateValidParams(params)
        params.id = profissional.id
        params.version = -1
        controller.update()

        assert view == "/profissional/edit"
        assert model.profissionalInstance != null
        assert model.profissionalInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/profissional/list'

        response.reset()

        populateValidParams(params)
        def profissional = new Profissional(params)

        assert profissional.save() != null
        assert Profissional.count() == 1

        params.id = profissional.id

        controller.delete()

        assert Profissional.count() == 0
        assert Profissional.get(profissional.id) == null
        assert response.redirectedUrl == '/profissional/list'
    }
}
