package br.edu.unirn.comum



import org.junit.*
import grails.test.mixin.*

@TestFor(SalaController)
@Mock(Sala)
class SalaControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/sala/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.salaInstanceList.size() == 0
        assert model.salaInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.salaInstance != null
    }

    void testSave() {
        controller.save()

        assert model.salaInstance != null
        assert view == '/sala/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/sala/show/1'
        assert controller.flash.message != null
        assert Sala.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/sala/list'

        populateValidParams(params)
        def sala = new Sala(params)

        assert sala.save() != null

        params.id = sala.id

        def model = controller.show()

        assert model.salaInstance == sala
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/sala/list'

        populateValidParams(params)
        def sala = new Sala(params)

        assert sala.save() != null

        params.id = sala.id

        def model = controller.edit()

        assert model.salaInstance == sala
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/sala/list'

        response.reset()

        populateValidParams(params)
        def sala = new Sala(params)

        assert sala.save() != null

        // test invalid parameters in update
        params.id = sala.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/sala/edit"
        assert model.salaInstance != null

        sala.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/sala/show/$sala.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        sala.clearErrors()

        populateValidParams(params)
        params.id = sala.id
        params.version = -1
        controller.update()

        assert view == "/sala/edit"
        assert model.salaInstance != null
        assert model.salaInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/sala/list'

        response.reset()

        populateValidParams(params)
        def sala = new Sala(params)

        assert sala.save() != null
        assert Sala.count() == 1

        params.id = sala.id

        controller.delete()

        assert Sala.count() == 0
        assert Sala.get(sala.id) == null
        assert response.redirectedUrl == '/sala/list'
    }
}
