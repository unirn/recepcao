package br.edu.unirn.comum



import org.junit.*
import grails.test.mixin.*

@TestFor(ClinicaController)
@Mock(Clinica)
class ClinicaControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/clinica/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.clinicaInstanceList.size() == 0
        assert model.clinicaInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.clinicaInstance != null
    }

    void testSave() {
        controller.save()

        assert model.clinicaInstance != null
        assert view == '/clinica/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/clinica/show/1'
        assert controller.flash.message != null
        assert Clinica.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/clinica/list'

        populateValidParams(params)
        def clinica = new Clinica(params)

        assert clinica.save() != null

        params.id = clinica.id

        def model = controller.show()

        assert model.clinicaInstance == clinica
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/clinica/list'

        populateValidParams(params)
        def clinica = new Clinica(params)

        assert clinica.save() != null

        params.id = clinica.id

        def model = controller.edit()

        assert model.clinicaInstance == clinica
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/clinica/list'

        response.reset()

        populateValidParams(params)
        def clinica = new Clinica(params)

        assert clinica.save() != null

        // test invalid parameters in update
        params.id = clinica.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/clinica/edit"
        assert model.clinicaInstance != null

        clinica.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/clinica/show/$clinica.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        clinica.clearErrors()

        populateValidParams(params)
        params.id = clinica.id
        params.version = -1
        controller.update()

        assert view == "/clinica/edit"
        assert model.clinicaInstance != null
        assert model.clinicaInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/clinica/list'

        response.reset()

        populateValidParams(params)
        def clinica = new Clinica(params)

        assert clinica.save() != null
        assert Clinica.count() == 1

        params.id = clinica.id

        controller.delete()

        assert Clinica.count() == 0
        assert Clinica.get(clinica.id) == null
        assert response.redirectedUrl == '/clinica/list'
    }
}
