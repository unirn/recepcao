package br.edu.unirn.consulta



import org.junit.*
import grails.test.mixin.*

@TestFor(StatusConsultaController)
@Mock(StatusConsulta)
class StatusConsultaControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/statusConsulta/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.statusConsultaInstanceList.size() == 0
        assert model.statusConsultaInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.statusConsultaInstance != null
    }

    void testSave() {
        controller.save()

        assert model.statusConsultaInstance != null
        assert view == '/statusConsulta/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/statusConsulta/show/1'
        assert controller.flash.message != null
        assert StatusConsulta.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/statusConsulta/list'

        populateValidParams(params)
        def statusConsulta = new StatusConsulta(params)

        assert statusConsulta.save() != null

        params.id = statusConsulta.id

        def model = controller.show()

        assert model.statusConsultaInstance == statusConsulta
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/statusConsulta/list'

        populateValidParams(params)
        def statusConsulta = new StatusConsulta(params)

        assert statusConsulta.save() != null

        params.id = statusConsulta.id

        def model = controller.edit()

        assert model.statusConsultaInstance == statusConsulta
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/statusConsulta/list'

        response.reset()

        populateValidParams(params)
        def statusConsulta = new StatusConsulta(params)

        assert statusConsulta.save() != null

        // test invalid parameters in update
        params.id = statusConsulta.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/statusConsulta/edit"
        assert model.statusConsultaInstance != null

        statusConsulta.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/statusConsulta/show/$statusConsulta.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        statusConsulta.clearErrors()

        populateValidParams(params)
        params.id = statusConsulta.id
        params.version = -1
        controller.update()

        assert view == "/statusConsulta/edit"
        assert model.statusConsultaInstance != null
        assert model.statusConsultaInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/statusConsulta/list'

        response.reset()

        populateValidParams(params)
        def statusConsulta = new StatusConsulta(params)

        assert statusConsulta.save() != null
        assert StatusConsulta.count() == 1

        params.id = statusConsulta.id

        controller.delete()

        assert StatusConsulta.count() == 0
        assert StatusConsulta.get(statusConsulta.id) == null
        assert response.redirectedUrl == '/statusConsulta/list'
    }
}
