package br.edu.unirn.consulta



import org.junit.*
import grails.test.mixin.*

@TestFor(FilaEsperaController)
@Mock(FilaEspera)
class FilaEsperaControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/filaEspera/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.filaEsperaInstanceList.size() == 0
        assert model.filaEsperaInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.filaEsperaInstance != null
    }

    void testSave() {
        controller.save()

        assert model.filaEsperaInstance != null
        assert view == '/filaEspera/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/filaEspera/show/1'
        assert controller.flash.message != null
        assert FilaEspera.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/filaEspera/list'

        populateValidParams(params)
        def filaEspera = new FilaEspera(params)

        assert filaEspera.save() != null

        params.id = filaEspera.id

        def model = controller.show()

        assert model.filaEsperaInstance == filaEspera
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/filaEspera/list'

        populateValidParams(params)
        def filaEspera = new FilaEspera(params)

        assert filaEspera.save() != null

        params.id = filaEspera.id

        def model = controller.edit()

        assert model.filaEsperaInstance == filaEspera
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/filaEspera/list'

        response.reset()

        populateValidParams(params)
        def filaEspera = new FilaEspera(params)

        assert filaEspera.save() != null

        // test invalid parameters in update
        params.id = filaEspera.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/filaEspera/edit"
        assert model.filaEsperaInstance != null

        filaEspera.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/filaEspera/show/$filaEspera.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        filaEspera.clearErrors()

        populateValidParams(params)
        params.id = filaEspera.id
        params.version = -1
        controller.update()

        assert view == "/filaEspera/edit"
        assert model.filaEsperaInstance != null
        assert model.filaEsperaInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/filaEspera/list'

        response.reset()

        populateValidParams(params)
        def filaEspera = new FilaEspera(params)

        assert filaEspera.save() != null
        assert FilaEspera.count() == 1

        params.id = filaEspera.id

        controller.delete()

        assert FilaEspera.count() == 0
        assert FilaEspera.get(filaEspera.id) == null
        assert response.redirectedUrl == '/filaEspera/list'
    }
}
