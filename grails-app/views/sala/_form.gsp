<%@ page import="br.edu.unirn.comum.Sala" %>
<div class="form-group">
    <label class="col-md-2 control-label" for="descricao">Nome da Sala</label>

    <div class="col-md-4">
        <input name="nome" type="text" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm" placeholder="Nome..." value="${salaInstance?.nome}">                                          
    </div>
</div> 

<div class="form-group">
    <label class="col-md-2 control-label" for="valor">Clinica</label>

    <div class="col-md-3">
   		<g:select id="clinica" name="clinica.id" from="${br.edu.unirn.comum.Clinica.list()}" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" optionKey="id" optionValue="descricao" value="${salaInstance?.clinica?.id}" class="form-control input-sm"/>
    </div>
</div>  


