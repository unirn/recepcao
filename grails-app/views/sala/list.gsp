
<%@ page import="br.edu.unirn.comum.Sala" %>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'paciente.label', default: 'Sala')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<div class="col-md-12">
                    
    <ol class="breadcrumb">
      <li><a href="${createLink(uri: '/')}">Vis&atilde;o Geral</a></li>
      <g:if test="${actionName == 'buscaSimples'}">
      	<li><g:link action="list">Salas</g:link></li> 
      	<li class="active">Resultado da Busca</li>  
      </g:if>
      <g:else>
      	<li class="active">Salas</li>
      </g:else>         
    </ol>

</div><!-- /.col-md-12 -->

<div class="col-md-12">
    
    <div class="panel panel-default">

        <div class="panel-heading">
            Salas
        </div><!-- /.panel-heading -->
        
        <div class="panel-body">
         	<g:if test="${flash.message}">
         		<div class="alert alert-dismissable alert-success">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					   <a href="${createLink(action: 'edit',params:[id:flash.id] )}" class="alert-link"><strong>${flash.nome} - </strong></a>
					   ${flash.message}	
				</div>
         	</g:if>
         	
            <fieldset>
            	<div class="row">
	            	<div class="col-md-6">
	            	<g:if test="${actionName == 'buscaSimples'}">
	            		<div class="btn-group">
		                      <g:link action="list" type="button" class="btn btn-default"><span class="icon-arrow-left"></span> Voltar</g:link>
		                </div>
	            	</g:if>
		                <div class="btn-group">
		                		<g:link action="create" class="btn btn-success"><span class="icon-plus"></span> Nova Sala</g:link>
		                </div>
		                <div class="btn-group">
                        	<button type="button" class="btn" id="deleteChecked" data-controller="${controllerName}"><span class="icon-trash"></span> Inativar</button>
                		</div>  
	               	</div>
	                <div class="col-md-3 pull-right">
	
	                    <g:form action="buscaSimples" role="search">

                            <div class="input-group">
                                <input name="nomeBuscaSimples" type="text" class="form-control" placeholder="Pesquisar...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><span class="icon-search"></span></button>
                                </span>
                            </div>

                    	</g:form>
						
	                </div><!-- /.col-md-6 -->
            	</div>
            </fieldset>
            
            	<g:render template="resultList"/>
            
        </div><!-- /.panel-body -->
        

    </div><!-- /.panel -->

</div><!-- /.col-md-12 -->	
<g:javascript>
	configuracoesGerais.ativarMenu("configuracoesMenu");
</g:javascript>
</body>
</html>
