	<p class="help-block" id="qntdRegistro">Quantidade - ${salaInstanceTotal}</p>
            
            <table class="table table-hover table-condensed table-bordered">
            <thead>
                <tr>
                    <th style="width:19px;">
						<span id="popoverAlert"></span>
                        <input type="checkbox" id="checkTodos" value="">
                    </th>
	                <g:if test="${actionName == 'buscaSimples'}">
	                    <g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="descricao" title="Descri&ccedil;&atilde;o" />
	                </g:if>
	                <g:else>
	                	<g:sortableColumn  property="nome" title="Nome" />
	    	        </g:else>
                    <th>Cl&iacute;nica</th>
					<th style="width: 10%;">Editar</th>
                </tr>
            </thead>
            <tbody>
                <g:each in="${salaInstanceList}" var="salaInstance">
	                
	                <tr id="${salaInstance?.id}">
	                    <td>
	                        <input type="checkbox" class="checkSimples" data-registroId="${salaInstance?.id}" data-campo="${controllerName}ID" value="">
	                    </td>
	                    <td>${salaInstance?.nome}</td>
	                    <td>${salaInstance?.clinica?.descricao}</td>
	                    <td style="text-align: center;"><g:link class="btn btn-xs btn-default" action="edit" id="${salaInstance?.id}">Editar</g:link></td>
	                </tr>
	                
	           </g:each>
            </tbody>
        </table>
        <div class="row pagination-table">
	        <div class="pagination pagination-centered col-lg-12">
                <g:if test="${actionName == 'buscaSimples' }">
                    <g:paginate params="${[nomeBuscaSimples: nomeBuscaSimples]}" total="${salaInstanceTotal}" />
                </g:if>
                <g:else>
                	<g:paginate total="${salaInstanceTotal}" />
    	        </g:else>
			</div>
		</div>