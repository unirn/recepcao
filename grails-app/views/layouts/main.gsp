<%@page import="br.edu.unirn.tipos.TipoUsuario" %>
<!DOCTYPE html>
<html>
<head>
<title><g:layoutTitle
		default="Clínicas Integradas - Núcleo DEV UNIRN" /></title>
		
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes"> 
<r:require modules="bootstrap" />

<g:layoutHead />
<r:layoutResources />

<link href="${resource(dir: 'css', file: 'font-awesome.css')}" rel="stylesheet">
<link href="${resource(dir: 'css/custom-theme', file: 'jquery-ui-1.10.0.custom.css')}" rel="stylesheet">
<link href="${resource(dir: 'js/plugins/bootstrap-colorpicker/css/', file: 'bootstrap-colorpicker.min.css')}" rel="stylesheet">
<link href="${resource(dir: 'css', file: 'style.css')}" rel="stylesheet">

<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

</head>
<body>
<div class="navbar  navbar-fixed-top">
      
    <div class="navbar-inner">  

      <div class="container">
      
        <div class="navbar-header">
      
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="${createLink(uri: '/')}"><i class="icon-ambulance icon-lg"></i> Recep&ccedil;&atilde;o</a>
      
        </div><!-- /.navbar-header -->
        
        <div class="navbar-collapse collapse">

          <ul class="nav navbar-nav pull-right">
            
            <li><a href="javascript:void(0);" id="linkAbrirChamado">Sugest&otilde;es</a></li>
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">${session.usuario.login} <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="${createLink(controller:'login',action:'logout') }">Sair</a></li>
              </ul>
            </li>
          
          </ul><!-- /.nav -->

        </div><!-- /.navbar-collapse -->

      </div><!-- /.container -->

    </div><!-- /.navbar-inner -->

</div><!-- /.navbar-fixed-top -->
    

<div class="subnavbar">

	<div class="subnavbar-inner">
	
		<div class="container">

			<ul class="mainnav">


				<li class="active" id="inicioMenu">
					<a href="${createLink(uri: '/')}">
						<i class="icon-bar-chart"></i>
						<span>Vis&atilde;o Geral</span>
					</a>	    				
				</li>
				
				<li id="pacienteMenu">
					<a href="${createLink(controller:'paciente', action:'list')}">					
						<i class="icon-male"></i>
						<span>Paciente</span>
					</a>  									
				</li>
				
				<li id="consultaMenu">
					<a href="${createLink(controller:'consulta', action:'list')}">					
						<i class="icon-stethoscope"></i>
						<span>Consulta</span>
					</a>  									
				</li>
				
				<li id="filaEsperaMenu">
					<a href="${createLink(controller:'filaEspera', action:'list')}">				
						<i class="icon-list"></i>
						<span>Fila de Espera</span>
					</a>  									
				</li>
				
				<li class="dropdown" id="configuracoesMenu">					
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-cogs"></i>
						<span>Configura&ccedil;&otilde;es</span>
					</a>	
				
					<ul class="dropdown-menu">
                        <li><a href="${createLink(controller:'clinica', action:'list')}">Cl&iacute;nica</a></li>
                        <li><a href="${createLink(controller:'sala', action:'list')}">Sala</a></li>
                        <li><a href="${createLink(controller:'agenda', action:'list')}">Agenda</a></li>
                        <li><a href="${createLink(controller:'statusConsulta', action:'list')}">Status Consulta</a></li>
                        <li><a href="${createLink(controller:'cidade', action:'list')}">Cidade</a></li>
                        <li><a href="${createLink(controller:'usuario', action:'list')}">Usu&aacute;rio</a></li>
                    </ul>    				
				</li>
				
			
			</ul>

		</div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->

    
<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	    	
	    	<div class="row">

	    		<div class="col-md-12">
	    			
	    			<g:layoutBody />

	    		</div><!-- /.col-md-12 -->


	    	</div><!-- /.row -->


	    </div><!-- /.container -->

	</div><!-- /.main-inner -->

</div><!-- /.main -->
    
    
    
 
<%--<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    <div class="col-md-3">
                        <h4>
                            UNI-RN</h4>
                        <ul>
                            <li><a href="http://www.unirn.edu.br/2013/">UNI-RN</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="col-md-3">
                    </div>
                    <!-- /span3 -->
                    <div class="col-md-3">
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                    </div>
                    <!-- /col-md-3 -->
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
--%><div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2013 <a href="javascript:void(0);">N&uacute;cleo de Desenvolvimento UNI-RN</a>
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->


<div id="abrirChamado" title="Abertura de Sugestão">
		<g:formRemote method="post" name="frmChamado"
			url="[controller:'chamado',action:'aberturaChamado']"
			onComplete="resetForm('#frmChamado');closeDialog('#abrirChamado');refreshPage();">
			<div class='col-md-12'>
				<div class="row">
					<div class='col-md-12'>
						<div class='form-group'>
							<label for="titulo"> T&iacute;tulo <span
								class="required-indicator"></span>
							</label>
							<g:textField class="form-control input-sm" name="titulo"
								/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class='col-md-12'>
						<div class='form-group'>
							<label for="descricao" class="control-label"> Descri&ccedil;&atilde;o </label>
							<g:textArea name="mensagem" rows="6" class="form-control"/>
						</div>
					</div>
				</div>
				<div class="row">
						<div class='col-md-12'>
							<g:actionSubmit class="btn btn-success pull-right" value="Enviar Sugestão" />
						</div>
				</div>
			</div>
		</g:formRemote>
</div>


<!-- javascript
        ================================================== -->
        <g:javascript src="jquery-ui-1.10.4.custom.min.js"/>
		<g:javascript src="validate/jquery.metadata.js"/> 
        <g:javascript src="validate/jquery.validate.js"/>
		<g:javascript src="jquery.maskedinput.min.js" />
		<g:javascript src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js" />
		<g:javascript src="jquery.maskMoney.min.js"/>
		
		<g:javascript src="base.js"/>  
		<g:javascript library="application" /> 
		<r:layoutResources />
</body>
</html>