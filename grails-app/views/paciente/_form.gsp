<%@ page import="br.edu.unirn.comum.Paciente" %>
<%@ page import="br.edu.unirn.comum.Pessoa" %>

<div class="form-group">
    <label class="col-md-2 control-label" for="numeroProntuario">N&uacute;m. Prontu&aacute;rio</label>

    <div class="col-md-2">
        <input name="numeroProntuario" type="text" class="form-control input-sm" placeholder="N&uacute;mero protu&aacute;rio..." value="${pacienteInstance?.numeroProntuario}">                                          
    </div>
</div>  

<div class="form-group">
    <label class="col-md-2 control-label" for="nome">Nome *</label>

    <div class="col-md-6">
        <input name="nome" type="text" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm" placeholder="Nome Completo..." value="${pacienteInstance?.pessoa?.nome}">                                          
    </div>
</div> 


<div class="form-group">
    <label class="col-md-2 control-label" for="nomeMae">Nome M&atilde;e</label>

    <div class="col-md-4">
        <input name="nomeMae" type="text" class="form-control input-sm" placeholder="Nome da m&atilde;e..." value="${pacienteInstance?.pessoa?.nomeMae}">                                          
    </div>
    
    <label class="col-md-1 control-label" for="nomePai">Nome Pai</label>

    <div class="col-md-4">
        <input name="nomePai" type="text" class="form-control input-sm" placeholder="Nome do pai..." value="${pacienteInstance?.pessoa?.nomePai}">                                          
    </div>
</div> 

 <div class="form-group">
    <label class="col-md-2 control-label" for="rg">RG</label>

    <div class="col-md-2">
        <input name="rg" type="text" class="form-control input-sm" placeholder="N&uacute;mero RG..." value="${pacienteInstance?.pessoa?.rg}">                                          
    </div>

    <label class="col-md-1 control-label" for="cpf">CPF</label>

    <div class="col-md-2">
        <input name="cpf" type="text" class="form-control input-sm" placeholder="N&uacute;mero CPF..." value="${pacienteInstance?.pessoa?.cpf}">                                          
    </div>
</div>


<div class="form-group">
    <label class="col-md-2 control-label" for="dataNascimento">Data Nascimento *</label>

    <div class="col-md-2">
    	<g:jQueryDatePicker name="dataNascimento" classField="form-control input-sm" placeholder="Data Nascimento..." value="${pacienteInstance?.pessoa?.dataNascimento}" required=""/>
    </div>
    <label class="col-md-1 control-label" for="sexo">Sexo</label>

    <div class="col-md-2">
       <g:select name="sexo" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm" from="${br.edu.unirn.tipos.Sexo?.values()}" keys="${br.edu.unirn.tipos.Sexo.values()*.name()}" value="${pacienteInstance?.pessoa?.sexo?.name()}"/>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label" for="ocupacao">Ocupa&ccedil;&atilde;o *</label>

    <div class="col-md-4">
        <input name="ocupacao" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" type="text" class="form-control input-sm" placeholder="Ocupa&ccedil;&atilde;o..." value="${pacienteInstance?.pessoa?.ocupacao}">                                          
    </div>

    <label class="col-md-2 control-label" for="grauInstrucao">Grau de Inst. *</label>

    <div class="col-md-3">
    	<g:select name="grauInstrucao"  data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm" from="${br.edu.unirn.tipos.GrauInstrucao?.values()}" keys="${br.edu.unirn.tipos.GrauInstrucao.values()*.name()}" value="${pacienteInstance?.pessoa?.grauInstrucao?.name()}" noSelection="['':'--- Escolha ---']"/>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label" for="logradouro">Logradouro *</label>

    <div class="col-md-6">
        <input name="logradouro" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" type="text" class="form-control input-sm" placeholder="Logradouro..." value="${pacienteInstance?.pessoa?.logradouro}">                                          
    </div>
    
    <label class="col-md-1 control-label" for="cep">CEP</label>

    <div class="col-md-2">
        <input name="cep" type="text" class="form-control input-sm" placeholder="Cep..." value="${pacienteInstance?.pessoa?.cep}">                                          
    </div>
</div> 

<div class="form-group">
    <label class="col-md-2 control-label" for="numero">N&uacute;mero</label>

    <div class="col-md-2">
        <input name="numero" type="text" class="form-control input-sm" placeholder="N&uacute;mero..." value="${pacienteInstance?.pessoa?.numero}">                                          
    </div>

    <label class="col-md-1 control-label" for="bairro">Bairro</label>

    <div class="col-md-2">
        <input name="bairro" type="text" class="form-control input-sm" placeholder="Bairro..." value="${pacienteInstance?.pessoa?.bairro}">                                          
    </div>
    
     <label class="col-md-1 control-label" for="cidade.id">Cidade</label>

    <div class="col-md-2">
    	<g:select name="cidade.id" id="pessoa.cidade" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm" from="${br.edu.unirn.comum.Cidade.list()}" optionKey="id"  value="${pacienteInstance?.pessoa?.cidade?.id}"/>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label" for="telefoneContato">Tel. Fixo</label>

    <div class="col-md-2">
        <input name="telefoneContato" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" type="text" class="form-control input-sm" placeholder="N&uacute;mero Telefone..." value="${pacienteInstance?.pessoa?.telefoneContato}">                                          
    </div>

    <label class="col-md-1 control-label" for="telefoneCelular">Celular</label>

    <div class="col-md-2">
        <input name="telefoneCelular" type="text" class="form-control input-sm" placeholder="N&uacute;mero Telefone..." value="${pacienteInstance?.pessoa?.telefoneCelular}">                                          
    </div>
    
     <label class="col-md-1 control-label" for="email">Email</label>

    <div class="col-md-3">
        <input name="email" type="email" class="form-control input-sm" placeholder="Email..." value="${pacienteInstance?.pessoa?.email}">                                          
    </div>
</div>

 <div class="form-group">
    <label class="col-md-2 control-label" for="vinculoInstituicao.id">Vinc. Instituicao</label>

    <div class="col-md-2">
       <g:select name="vinculoInstituicao.id" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" id="vinculoInstituicao" class="form-control input-sm" from="${br.edu.unirn.comum.VinculoInstituicao.list()}" optionKey="id" value="${pacienteInstance?.vinculoInstituicao?.id}"/>
    </div>

    <label class="col-md-1 control-label" for="encaminhado.id">Encaminhado</label>

    <div class="col-md-3">
       <g:select name="encaminhado.id" id="encaminhado" class="form-control input-sm" from="${br.edu.unirn.comum.Clinica.list()}" optionKey="id" value="${pacienteInstance?.encaminhado?.id}" noSelection="['null': '-- Escolha --']"/>	
    </div>
</div> 

<div class="form-group">
    <label class="col-md-2 control-label" for="observacao">Observa&ccedil;&atilde;o</label>
	
    <div class="col-md-5">		
			<richui:richTextEditor name="observacao" class="form-control input-sm hidden-xs" value="${pacienteInstance?.pessoa?.observacao}" width="600"/>
    </div>
</div>
                 
                
        
