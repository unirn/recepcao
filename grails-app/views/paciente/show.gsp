
<%@ page import="br.edu.unirn.comum.Paciente" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'paciente.label', default: 'Paciente')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>		
		<g:set value="${pacienteInstance.pessoa}" var="pessoaInstance"/>
		
		<div id="show-paciente" class="hero-unit" role="main">
			<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
					<span>Paciente</span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
						<div class="alert" role="status">
							 <button type="button" class="close" data-dismiss="alert">&times;</button>
							 <strong>${flash.message}</strong>
						</div>
					</g:if>
					<ol class="property-list paciente">
						<g:if test="${pacienteInstance?.numeroProntuario}">
						<li class="control-group">
							<span id="numeroProntuario-label" class="property-label"><g:message code="paciente.numeroProntuario.label" default="Número Protuário" /></span>
							
								<span class="property-value" aria-labelledby="numeroProntuario-label">${pacienteInstance?.numeroProntuario?.encodeAsHTML()}</span>
							
						</li>
						</g:if>
					
						<g:if test="${pacienteInstance?.vinculoInstituicao}">
						<li class="control-group">
							<span id="vinculoInstituicao-label" class="property-label"><g:message code="paciente.vinculoInstituicao.label" default="Vinculo Instituicao" /></span>
							
								<span class="property-value" aria-labelledby="vinculoInstituicao-label">${pacienteInstance?.vinculoInstituicao?.encodeAsHTML()}</span>
							
						</li>
						</g:if>
					
						<g:if test="${pacienteInstance?.encaminhado}">
						<li class="control-group">
							<span id="encaminhado-label" class="property-label"><g:message code="paciente.encaminhado.label" default="Encaminhado" /></span>
							
								<span class="property-value" aria-labelledby="encaminhado-label">${pacienteInstance?.encaminhado?.encodeAsHTML()}</span>
							
						</li>
						</g:if>
						<g:if test="${pessoaInstance?.nome}">
						<li class="control-group">
							<span id="nome-label" class="property-label"><g:message code="pessoa.nome.label" default="Nome" /></span>
							
								<span class="property-value" aria-labelledby="nome-label"><g:fieldValue bean="${pessoaInstance}" field="nome"/></span>
							
						</li>
						</g:if>
						<g:if test="${pacienteInstance?.inativo}">
						<li class="control-group">
							<span id="inativo-label" class="property-label"><g:message code="paciente.inativo.label" default="Inativo" /></span>
							
								<span class="property-value" aria-labelledby="inativo-label"><g:formatBoolean boolean="${pacienteInstance?.inativo}"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.nomePai}">
						<li class="control-group">
							<span id="nomePai-label" class="property-label"><g:message code="pessoa.nomePai.label" default="Nome Pai" /></span>
							
								<span class="property-value" aria-labelledby="nomePai-label"><g:fieldValue bean="${pessoaInstance}" field="nomePai"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.nomeMae}">
						<li class="control-group">
							<span id="nomeMae-label" class="property-label"><g:message code="pessoa.nomeMae.label" default="Nome Mae" /></span>
							
								<span class="property-value" aria-labelledby="nomeMae-label"><g:fieldValue bean="${pessoaInstance}" field="nomeMae"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.ocupacao}">
						<li class="control-group">
							<span id="ocupacao-label" class="property-label"><g:message code="pessoa.ocupacao.label" default="Ocupacao" /></span>
							
								<span class="property-value" aria-labelledby="ocupacao-label"><g:fieldValue bean="${pessoaInstance}" field="ocupacao"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.rg}">
						<li class="control-group">
							<span id="rg-label" class="property-label"><g:message code="pessoa.rg.label" default="Rg" /></span>
							
								<span class="property-value" aria-labelledby="rg-label"><g:fieldValue bean="${pessoaInstance}" field="rg"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.cpf}">
						<li class="control-group">
							<span id="cpf-label" class="property-label"><g:message code="pessoa.cpf.label" default="Cpf" /></span>
							
								<span class="property-value" aria-labelledby="cpf-label"><g:fieldValue bean="${pessoaInstance}" field="cpf"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.email}">
						<li class="control-group">
							<span id="email-label" class="property-label"><g:message code="pessoa.email.label" default="Email" /></span>
							
								<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${pessoaInstance}" field="email"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.sexo}">
						<li class="control-group">
							<span id="sexo-label" class="property-label"><g:message code="pessoa.sexo.label" default="Sexo" /></span>
							
								<span class="property-value" aria-labelledby="sexo-label"><g:fieldValue bean="${pessoaInstance}" field="sexo"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.dataNascimento}">
						<li class="control-group">
							<span id="dataNascimento-label" class="property-label"><g:message code="pessoa.dataNascimento.label" default="Data Nascimento" /></span>
							
								<span class="property-value" aria-labelledby="dataNascimento-label"><g:formatDate date="${pessoaInstance?.dataNascimento}" /></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.grauInstrucao}">
						<li class="control-group">
							<span id="grauInstrucao-label" class="property-label"><g:message code="pessoa.grauInstrucao.label" default="Grau Instrucao" /></span>
							
								<span class="property-value" aria-labelledby="grauInstrucao-label"><g:fieldValue bean="${pessoaInstance}" field="grauInstrucao"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.logradouro}">
						<li class="control-group">
							<span id="logradouro-label" class="property-label"><g:message code="pessoa.logradouro.label" default="Logradouro" /></span>
							
								<span class="property-value" aria-labelledby="logradouro-label"><g:fieldValue bean="${pessoaInstance}" field="logradouro"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.bairro}">
						<li class="control-group">
							<span id="bairro-label" class="property-label"><g:message code="pessoa.bairro.label" default="Bairro" /></span>
							
								<span class="property-value" aria-labelledby="bairro-label"><g:fieldValue bean="${pessoaInstance}" field="bairro"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.cidade}">
						<li class="control-group">
							<span id="cidade-label" class="property-label"><g:message code="pessoa.cidade.label" default="Cidade" /></span>
							
								<span class="property-value" aria-labelledby="cidade-label">${pessoaInstance?.cidade?.encodeAsHTML()}</span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.telefoneContato}">
						<li class="control-group">
							<span id="telefoneContato-label" class="property-label"><g:message code="pessoa.telefoneContato.label" default="Telefone Contato" /></span>
							
								<span class="property-value" aria-labelledby="telefoneContato-label"><g:fieldValue bean="${pessoaInstance}" field="telefoneContato"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.cep}">
						<li class="control-group">
							<span id="cep-label" class="property-label"><g:message code="pessoa.cep.label" default="Cep" /></span>
							
								<span class="property-value" aria-labelledby="cep-label"><g:fieldValue bean="${pessoaInstance}" field="cep"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.dateCreated}">
						<li class="control-group">
							<span id="dateCreated-label" class="property-label"><g:message code="pessoa.dateCreated.label" default="Date Created" /></span>
							
								<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${pessoaInstance?.dateCreated}" /></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.lastUpdated}">
						<li class="control-group">
							<span id="lastUpdated-label" class="property-label"><g:message code="pessoa.lastUpdated.label" default="Last Updated" /></span>
							
								<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${pessoaInstance?.lastUpdated}" /></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.numero}">
						<li class="control-group">
							<span id="numero-label" class="property-label"><g:message code="pessoa.numero.label" default="Numero" /></span>
							
								<span class="property-value" aria-labelledby="numero-label"><g:fieldValue bean="${pessoaInstance}" field="numero"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.telefoneCelular}">
						<li class="control-group">
							<span id="telefoneCelular-label" class="property-label"><g:message code="pessoa.telefoneCelular.label" default="Telefone Celular" /></span>
							
								<span class="property-value" aria-labelledby="telefoneCelular-label"><g:fieldValue bean="${pessoaInstance}" field="telefoneCelular"/></span>
							
						</li>
						</g:if>
					
					</ol>
					<g:form>
						<fieldset class="buttons">
							<g:hiddenField name="id" value="${pacienteInstance?.id}" />
							<g:link class="btn btn-primary" action="edit" id="${pacienteInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
							<g:actionSubmit class="btn" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</fieldset>
					</g:form>
				</div>
			</div>
		</div>
	</body>
</html>
