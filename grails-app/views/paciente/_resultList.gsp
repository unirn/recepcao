<%@ page import="br.edu.unirn.comum.Paciente"%>

	<p class="help-block" id="qntdRegistro">Quantidade - ${pacienteInstanceTotal}</p>
            
            <table class="table table-hover table-condensed table-bordered">
            <thead>
                <tr>
                    <th style="width:19px;">
						<span id="popoverAlert"></span>
                        <input type="checkbox" id="checkTodos" value="">
                    </th>
                    <g:if test="${actionName == 'buscaAvancada'}">
	                    <g:sortableColumn params="${[nomeBuscaAvancada: nomeBuscaAvancada,pacienteIdBuscaAvancada:pacienteIdBuscaAvancada]}" property="numeroProntuario" title="Núm. Pront." />
    	                <g:sortableColumn params="${[nomeBuscaAvancada: nomeBuscaAvancada,pacienteIdBuscaAvancada:pacienteIdBuscaAvancada]}" property="pessoa.nome" title="Nome Paciente" />
                    </g:if>
	                <g:elseif test="${actionName == 'buscaSimples'}">
	                    <g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="numeroProntuario" title="Núm. Pront." />
	    	            <g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="pessoa.nome" title="Nome Paciente" />
	                </g:elseif>
	                <g:else>
	                	<g:sortableColumn  property="numeroProntuario" title="Núm. Pront." />
	    	            <g:sortableColumn  property="pessoa.nome" title="Nome Paciente" />
	    	        </g:else>
                    <th>Telefone Contato</th>
					<th>Telefone Celular</th>
					<th>Hist.</th>
					<th>Editar</th>
                </tr>
            </thead>
            <tbody>
                <g:each in="${pacienteInstanceList}" var="pacienteInstance">
	                
	                <tr id="${pacienteInstance?.id}">
	                    <td>
	                        <input type="checkbox" class="checkSimples" data-registroId="${pacienteInstance?.id}" data-campo="${controllerName}ID" value="">
	                    </td>
	                    <td>${pacienteInstance?.numeroProntuario.toString()}</td>
	                    <td>${pacienteInstance?.pessoa.nome}</td>
	                    <td>${pacienteInstance?.pessoa.telefoneContato}</td>
	                    <td>${pacienteInstance?.pessoa.telefoneCelular}</td>
	                    <td style="text-align: center;"><g:link class="btn btn-xs btn-default" action="historicoAlteracaoConsulta" id="${pacienteInstance?.id}" controller="alteracaoStatusConsulta"><g:img file="skin/database_edit.png"/></g:link></td>
	                    <td style="text-align: center;"><g:link class="btn btn-xs btn-default" action="edit" id="${pacienteInstance?.id}">Editar</g:link></td>
	                </tr>
	                
	           </g:each>
            </tbody>
        </table>
        <div class="row pagination-table">
	        <div class="pagination pagination-centered col-lg-12">
	        	<g:if test="${actionName == 'buscaAvancada' }">
	                   <g:paginate params="${[nomeBuscaAvancada: nomeBuscaAvancada,pacienteIdBuscaAvancada:pacienteIdBuscaAvancada]}" total="${pacienteInstanceTotal}" />
                    </g:if>
	                <g:elseif test="${actionName == 'buscaSimples' }">
	                    <g:paginate params="${[nomeBuscaSimples: nomeBuscaSimples]}" total="${pacienteInstanceTotal}" />
	                </g:elseif>
	                <g:else>
	                	<g:paginate total="${pacienteInstanceTotal}" />
	    	        </g:else>
			</div>
		</div>