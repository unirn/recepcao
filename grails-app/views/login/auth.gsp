<!doctype html>
<html lang="en">
<head>
<title>Clínicas Integradas - Núcleo DEV UNIRN</title>
<r:require module="bootstrap" />
<r:layoutResources />

<style type="text/css">
/* Override some defaults */
html,body {
	background-color: #eee;
}

body {
	padding-top: 100px;
}

.container {
	width: 300px;
}

.navbar-inverse .navbar-brand {
    color: #FFFFFF;
}

.btn{
background: none repeat scroll 0 0 #00BA8B !important;
}


.navbar {
background: none repeat scroll 0 0 #00BA8B !important;
    border-radius: 0 0 0 0;
    padding: 7px 0;
}

/* The white background content wrapper */
.container>.content {
	background-color: #fff;
	padding: 20px;
	margin: 0 -20px;
	-webkit-border-radius: 10px 10px 10px 10px;
	-moz-border-radius: 10px 10px 10px 10px;
	border-radius: 10px 10px 10px 10px;
	-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .15);
	-moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .15);
	box-shadow: 0 1px 2px rgba(0, 0, 0, .15);
}

.login-form {
	margin-left: 40px;
	margin-right: 40px;
}

legend {
	margin-right: -50px;
	font-weight: bold;
	color: #404040;
}
</style>
</head>
<%@page import="grails.util.Environment"%>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="navbar-brand" href="${createLink(uri: '/')}" class="brand">Sistema das
					Cl&iacute;nicas Integradas - UNIRN</a>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="content">
			<div class="row">
					<div class="login-form ">
						<h2>Autentica&ccedil;&atilde;o</h2>
						<g:form action="autenticar" controller="login" autocomplete='off'>
							<g:if test="${flash.mensagem}">
								<div class="alert alert-error">
									${flash.mensagem}
								</div>
							</g:if>
							<fieldset>
								<div class="form-group">
									<input class="form-control" type='text' name='login' id='login'
										placeholder="Usuário"
										value="${!Environment.developmentMode?'':'admin'}" />
								</div>
								<div class="form-group">
									<input class="form-control" type='password' name='senha'
										value="${!Environment.developmentMode?'':'admin'}" id='senha'
										placeholder="Senha" />
								</div>
								<button class="btn" type="submit">Login</button>
							</fieldset>
						</g:form>
						<g:link controller="usuario" action="solicitar">Solicitar Cadastro</g:link>
					</div>
			</div><!-- /row -->
		</div>
	</div>
	<!-- /container -->
</body>
<script type='text/javascript'>
	
</script>
</html>