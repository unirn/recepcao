<!doctype html>
<html>
<head>
<meta name="layout" content="main">
<title>Busca Paciente</title>
</head>
<body>
	<div id="busca-paciente" class="contentCrud" role="main">
		<g:if test="${flash.message}">
			<div class="alert" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:form action="consulta" class="form-horizontal">
			<legend>Busca de Paciente</legend>
			<fieldset >
				<div
					class="control-group required">
					<label class="control-label" for="idPaciente"> ID. Paciente
					</label>
					<div class="controls">
						<g:textField name="numeroProntuario" value="${identificador}"/>
					</div>
				</div>
				<div
					class="control-group required">
					<label class="control-label" for="nomePaciente"> Nome Paciente
					</label>
					<div class="controls">
						<g:textField name="nome" value="${nome}"/>
					</div>
				</div>
				
				<div class="form-actions">
					<g:submitButton name="consulta" class="btn btn-primary" value="Consultar" />
				</div>
			</fieldset>
			
		</g:form>
		<g:if test="${pacienteLista}">
			<table class="table table-hover">
			<thead>
				<tr>
					<g:sortableColumn property="pessoa.nome" title="Nome Paciente" />
					<g:sortableColumn property="id" title="ID" />
					<th>Telefone Contato</th>
					<th>Telefone Celular</th>
					<th>Editar</th>
					<th>Remover</th>
				</tr>
			</thead>
			<tbody>
				<g:each in="${pacienteLista}" var="pacienteInstance">
					<tr>
						<td><g:link action="show" id="${pacienteInstance.id}" controller="paciente">
								${fieldValue(bean: pacienteInstance, field: "pessoa")}
							</g:link></td>
						<td>
							${fieldValue(bean: pacienteInstance, field: "id")}
						</td>
						
						<td>
							${pacienteInstance.pessoa.telefoneContato}
						</td>

						<td>
							${pacienteInstance.pessoa.telefoneCelular}
						</td>

						<td><g:link class="btn" action="edit"
								id="${pacienteInstance?.id}" controller="paciente">
								<g:message code="default.button.edit.label" default="Edit" />
							</g:link></td>
						<td><g:form controller="paciente">
								<g:hiddenField name="id" value="${pacienteInstance?.id}" />
								<g:actionSubmit class="btn" action="delete"
									value="Inativar"
									onclick="return confirm('Deseja realmente Inativar o Paciente?');" />
							</g:form></td>
					</tr>
				</g:each>
			</tbody>
		</table>
		</g:if>
	</div>
</body>
</html>
