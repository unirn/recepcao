<!doctype html>
<html>
<head>
<meta name="layout" content="main">
<title>Busca Profissional</title>
</head>
<body>
	<div id="busca-profissional" class="contentCrud" role="main">
		<g:if test="${flash.message}">
			<div class="alert" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:form action="consulta" class="form-horizontal">
			<legend>Busca de Profissional</legend>
			<fieldset >
				<div
					class="control-group required">
					<label class="control-label" for="idPaciente"> ID. Profissional
					</label>
					<div class="controls">
						<g:textField name="id" value="${identificador}"/>
					</div>
				</div>
				<div
					class="control-group required">
					<label class="control-label" for="nomeProfissional"> Nome Profissional
					</label>
					<div class="controls">
						<g:textField name="nome" value="${nome}"/>
					</div>
				</div>
				
				<div class="form-actions">
					<g:submitButton name="consulta" class="btn btn-primary" value="Consultar" />
				</div>
			</fieldset>
			
		</g:form>
		<g:if test="${profissionalLista}">
			<table class="table table-hover">
			<thead>
				<tr>
					<g:sortableColumn property="pessoa.nome" title="Nome Paciente" />
					<g:sortableColumn property="id" title="ID" />
					<th>Papel</th>
					<th>Telefone Contato</th>
					<th>Telefone Celular</th>
					<th>Editar</th>
					<th>Remover</th>
				</tr>
			</thead>
			<tbody>
				<g:each in="${profissionalLista}" var="profissionalInstance">
					<tr>
						<td><g:link action="show" id="${profissionalInstance.id}">
								${fieldValue(bean: profissionalInstance, field: "pessoa")}
							</g:link></td>
						<td>
							${fieldValue(bean: profissionalInstance, field: "id")}
						</td>
						<td>
							${fieldValue(bean: profissionalInstance, field: "papel")}
						</td>	

						<td>
							${profissionalInstance.pessoa.telefoneContato}
						</td>

						<td>
							${profissionalInstance.pessoa.telefoneCelular}
						</td>

						<td><g:link class="btn" action="edit"
								id="${profissionalInstance?.id}">
								<g:message code="default.button.edit.label" default="Edit" />
							</g:link></td>
						<td><g:form>
								<g:hiddenField name="id" value="${profissionalInstance?.id}" />
								<g:actionSubmit class="btn" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</g:form></td>
					</tr>
				</g:each>
			</tbody>
		</table>
		</g:if>
	</div>
</body>
</html>
