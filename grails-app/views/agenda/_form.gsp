<%@ page import="br.edu.unirn.consulta.Agenda" %>


<div class="form-group">
    <label class="col-lg-2 col-md-2 control-label" for="clinica.id">Cl&iacute;nica</label>

    <div class="col-lg-3 col-md-3">
    	<g:select id="clinica" name="clinica.id" from="${br.edu.unirn.comum.Clinica.list()}" optionKey="id" value="${agendaInstance?.clinica?.id}" 
    	data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm"/>
    	
    </div>
</div>

<div class="form-group">
    <label class="col-lg-2 col-md-2 control-label" for="horaInicio">Hora Inicio</label>

    <div class="col-lg-2 col-md-2">
    	<g:textField name="horaInicio" pattern="${agendaInstance.constraints.horaInicio.matches}" required="" value="${agendaInstance?.horaInicio}" 
    	data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm" placeholder="Ex.: 14:00"/>
    </div>
    
    <label class="col-lg-1 col-md-1 control-label" for="horaFim">Hora Fim</label>

    <div class="col-lg-2 col-md-2">
    	<g:textField name="horaFim" pattern="${agendaInstance.constraints.horaFim.matches}" value="${agendaInstance?.horaFim}" 
    	data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm" placeholder="Ex.: 18:00"/>
    </div>
    
    <label class="col-lg-1 col-md-1 control-label" for="duracao">Dura&ccedil;&atilde;o</label>

    <div class="col-lg-2 col-md-2">
    	<g:field name="duracao" value="${fieldValue(bean: agendaInstance, field: 'duracao')}"  type="float" 
    	data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm"/> 
		<a id="exemploDuracao" href="#" title="Ex.: 1,2 (1 hora e 20 min.) | 0,4 (40 min.)">Ajuda</a>
		<richui:tooltip id="exemploDuracao" />
    </div>
    
</div> 
 
<div class="form-group">
    <label class="col-lg-2 col-md-2 control-label" for="turno">Turno</label>

    <div class="col-lg-2 col-md-2">
    	<g:select name="turno" from="${br.edu.unirn.tipos.Turno?.values()}" keys="${br.edu.unirn.tipos.Turno.values()*.name()}"  value="${agendaInstance?.turno?.name()}"
		data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm"/>
    </div>
</div>

