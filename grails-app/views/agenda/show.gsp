
<%@ page import="br.edu.unirn.consulta.Agenda" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'agenda.label', default: 'Agenda')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="show-agenda" class="contentCrud" role="main">
			<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
					<span>Agenda</span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
						<div class="alert" role="status">
							 <button type="button" class="close" data-dismiss="alert">&times;</button>
							 <strong>${flash.message}</strong>
						</div>
					</g:if>
					<ol class="property-list agenda">
					
						<g:if test="${agendaInstance?.horaInicio}">
						<li class="control-group">
							<span id="horaInicio-label" class="property-label"><g:message code="agenda.horaInicio.label" default="Hora Inicio" /></span>
							
								<span class="property-value" aria-labelledby="horaInicio-label"><g:fieldValue bean="${agendaInstance}" field="horaInicio"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${agendaInstance?.horaFim}">
						<li class="control-group">
							<span id="horaFim-label" class="property-label"><g:message code="agenda.horaFim.label" default="Hora Fim" /></span>
							
								<span class="property-value" aria-labelledby="horaFim-label"><g:fieldValue bean="${agendaInstance}" field="horaFim"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${agendaInstance?.clinica}">
						<li class="control-group">
							<span id="clinica-label" class="property-label"><g:message code="agenda.clinica.label" default="Clinica" /></span>
							
								<span class="property-value" aria-labelledby="clinica-label"><g:link controller="clinica" action="show" id="${agendaInstance?.clinica?.id}">${agendaInstance?.clinica?.encodeAsHTML()}</g:link></span>
							
						</li>
						</g:if>
					
						<g:if test="${agendaInstance?.duracao}">
						<li class="control-group">
							<span id="duracao-label" class="property-label"><g:message code="agenda.duracao.label" default="Duracao" /></span>
							
								<span class="property-value" aria-labelledby="duracao-label"><g:fieldValue bean="${agendaInstance}" field="duracao"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${agendaInstance?.turno}">
						<li class="control-group">
							<span id="turno-label" class="property-label"><g:message code="agenda.turno.label" default="Turno" /></span>
							
								<span class="property-value" aria-labelledby="turno-label"><g:fieldValue bean="${agendaInstance}" field="turno"/></span>
							
						</li>
						</g:if>
					
					</ol>
					<g:form>
						<fieldset class="buttons">
							<g:hiddenField name="id" value="${agendaInstance?.id}" />
							<g:link class="edit" action="edit" id="${agendaInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
							<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</fieldset>
					</g:form>
				</div>
			</div>
		</div>
	</body>
</html>
