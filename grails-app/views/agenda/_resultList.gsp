<%@ page import="br.edu.unirn.consulta.Agenda" %>

	<p class="help-block" id="qntdRegistro">Quantidade - ${agendaInstanceTotal}</p>
            
            <table class="table table-hover table-condensed table-bordered">
            <thead>
                <tr>
                    <th style="width:19px;">
						<span id="popoverAlert"></span>
                        <input type="checkbox" id="checkTodos" value="">
                    </th>
	                <g:if test="${actionName == 'buscaSimples'}">
	                    <g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="clinica" title="Cl&iacute;nica" />
	                    <g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="horaInicio" title="${message(code: 'agenda.horaInicio.label', default: 'Hora Inicio')}" />
						<g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="horaFim" title="${message(code: 'agenda.horaFim.label', default: 'Hora Fim')}" />
						<g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="duracao" title="${message(code: 'agenda.duracao.label', default: 'Dura&ccedil;&atilde;o')}" />
						<g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="turno" title="${message(code: 'agenda.turno.label', default: 'Turno')}" />
	                </g:if>
	                <g:else>
	                	<g:sortableColumn property="clinica.descricao" title="Cl&iacute;nica" />
	                	<g:sortableColumn property="horaInicio" title="${message(code: 'agenda.horaInicio.label', default: 'Hora Inicio')}" />
						<g:sortableColumn property="horaFim" title="${message(code: 'agenda.horaFim.label', default: 'Hora Fim')}" />
						<g:sortableColumn property="duracao" title="${message(code: 'agenda.duracao.label', default: 'Dura&ccedil;&atilde;o')}" />
						<g:sortableColumn property="turno" title="${message(code: 'agenda.turno.label', default: 'Turno')}" />
	    	        </g:else>
	    	        
					<th style="width: 10%;">Editar</th>
                </tr>
            </thead>
            <tbody>
                <g:each in="${agendaInstanceList}" var="agendaInstance">
	                
	                <tr id="${agendaInstance?.id}">
	                    <td>
	                        <input type="checkbox" class="checkSimples" data-registroId="${agendaInstance?.id}" data-campo="${controllerName}ID" value="">
	                    </td>
	                    <td>${fieldValue(bean: agendaInstance, field: "clinica")}</td>
							
						<td>${fieldValue(bean: agendaInstance, field: "horaInicio")}</td>
						
						<td>${fieldValue(bean: agendaInstance, field: "horaFim")}</td>
					
						<td>${fieldValue(bean: agendaInstance, field: "duracao")}</td>
					
						<td>${fieldValue(bean: agendaInstance, field: "turno")}</td>
						
	                    <td style="text-align: center;"><g:link class="btn btn-xs btn-default" action="edit" id="${agendaInstance?.id}">Editar</g:link></td>
	                </tr>
	                
	           </g:each>
            </tbody>
        </table>
        <div class="row pagination-table">
	        <div class="pagination pagination-centered col-lg-12">
                <g:if test="${actionName == 'buscaSimples' }">
                    <g:paginate params="${[nomeBuscaSimples: nomeBuscaSimples]}" total="${agendaInstanceTotal}" />
                </g:if>
                <g:else>
                	<g:paginate total="${agendaInstanceTotal}" />
    	        </g:else>
			</div>
		</div>