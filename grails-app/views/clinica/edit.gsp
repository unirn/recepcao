<%@ page import="br.edu.unirn.comum.Clinica" %>
<!doctype html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'paciente.label', default: 'Clinica')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
	<resource:richTextEditor />
</head>
<body>
<div class="col-lg-12 col-md-12">
    
    <ol class="breadcrumb">
      <li><a href="${createLink(uri: '/')}">Vis&atilde;o Geral</a></li>
      <li><g:link action="list">Cl&iacute;nicas</g:link></li>
      <li class="active">Editar</li>        
    </ol>

</div><!-- /.col-md-12 -->

<div class="col-lg-12 col-md-12">
    
    <div class="panel panel-default">

        <div class="panel-heading">
           Editar Cl&iacute;nica
        </div><!-- /.panel-heading -->
        
        <div class="panel-body">
		<g:if test="${flash.message}">
			<div class="alert alert-dismissable alert-danger">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  ${flash.message}	
			</div>
		</g:if>
		<g:form name="delete" action="delete">
			<g:hiddenField name="id" value="${clinicaInstance?.id}"/>
		</g:form>
        <g:form action="update" class="form-horizontal formValidate" role="form">
        	<fieldset class="groupFields"> 
        	
				<g:hiddenField name="id" value="${clinicaInstance?.id}"/>		
				
				<g:render template="form" />
        		
        		<div class="btn-group">
                       <g:link action="list" class="btn btn-default pull-right"><span class="icon-arrow-left"></span> Voltar</g:link>
                </div> 
                <div class="btn-group">
                		<button type="button" class="btn linkDelete" ><span class="icon-trash"></span> Inativar</button>
                </div> 
                <div class="btn-group">
                		<button class="btn btn-success pull-right" type="submit"><span class="icon-pencil"></span> Editar</button>
                </div>  
                
                
            </fieldset>
        </g:form>
            
        </div><!-- /.panel-body -->
        

    </div><!-- /.panel -->

</div><!-- /.col-md-12 -->
<g:javascript>
	configuracoesGerais.ativarMenu("configuracoesMenu");
</g:javascript>
</body>
</html>

