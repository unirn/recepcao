
<%@ page import="br.edu.unirn.comum.Clinica" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'clinica.label', default: 'Clinica')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="show-clinica" class="contentCrud" role="main">
			<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
					<span><g:message code="default.show.label" args="[entityName]" /></span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
						<div class="alert" role="status">
							 <button type="button" class="close" data-dismiss="alert">&times;</button>
							 <strong>${flash.message}</strong>
						</div>
					</g:if>			
					<ol class="property-list clinica">
					
						<g:if test="${clinicaInstance?.descricao}">
						<li class="fieldcontain">
							<span id="descricao-label" class="property-label"><g:message code="clinica.descricao.label" default="Descricao" /></span>
							
								<span class="property-value" aria-labelledby="descricao-label"><g:fieldValue bean="${clinicaInstance}" field="descricao"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${clinicaInstance?.valor}">
						<li class="fieldcontain">
							<span id="valor-label" class="property-label"><g:message code="clinica.valor.label" default="Valor" /></span>
							
								<span class="property-value" aria-labelledby="valor-label"><g:fieldValue bean="${clinicaInstance}" field="valor"/></span>
							
						</li>
						</g:if>
					
					</ol>
					<g:form class="form">
						<div class="form-actions">
							<g:hiddenField name="id" value="${clinicaInstance?.id}" />
							<g:link class="btn btn-primary" action="edit" id="${clinicaInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
							<g:actionSubmit class="btn" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</div>
					</g:form>
				</div>
			</div>
		</div>
	</body>
</html>
