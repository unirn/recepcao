<%@ page import="br.edu.unirn.comum.Clinica" %>

<div class="form-group">
    <label class="col-md-2 control-label" for="descricao">Nome da Cl&iacute;nica</label>

    <div class="col-md-4">
        <input name="descricao" type="text" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm" placeholder="Nome..." value="${clinicaInstance?.descricao}">                                          
    </div>
</div> 

<div class="form-group">
    <label class="col-md-2 control-label" for="tempvalor">Valor da Consulta</label>

    <div class="col-md-2">
	    <div class="input-group">
	  		<span class="input-group-addon">R$</span>
	        <input name="valor" type="text" class="form-control input-sm" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}"  placeholder="Valor..." value="${clinicaInstance?.valor}">                                          
	    </div>
    </div>
</div>  
