<%@ page import="br.edu.unirn.comum.Clinica"%>

	<p class="help-block" id="qntdRegistro">Quantidade - ${clinicaInstanceTotal}</p>
            
            <table class="table table-hover table-condensed table-bordered">
            <thead>
                <tr>
                    <th style="width:19px;">
						<span id="popoverAlert"></span>
                        <input type="checkbox" id="checkTodos" value="">
                    </th>
	                <g:if test="${actionName == 'buscaSimples'}">
	                    <g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="descricao" title="Descri&ccedil;&atilde;o" />
	                </g:if>
	                <g:else>
	                	<g:sortableColumn  property="descricao" title="Descri&ccedil;&atilde;o" />
	    	        </g:else>
                    <th>Valor</th>
					<th style="width: 10%;">Editar</th>
                </tr>
            </thead>
            <tbody>
                <g:each in="${clinicaInstanceList}" var="clinicaInstance">
	                
	                <tr id="${clinicaInstance?.id}">
	                    <td>
	                        <input type="checkbox" class="checkSimples" data-registroId="${clinicaInstance?.id}" data-campo="${controllerName}ID" value="">
	                    </td>
	                    <td>${clinicaInstance?.descricao}</td>
	                    <td>R$ ${clinicaInstance?.valor}</td>
	                    <td style="text-align: center;"><g:link class="btn btn-xs btn-default" action="edit" id="${clinicaInstance?.id}">Editar</g:link></td>
	                </tr>
	                
	           </g:each>
            </tbody>
        </table>
        <div class="row pagination-table">
	        <div class="pagination pagination-centered col-lg-12">
                <g:if test="${actionName == 'buscaSimples' }">
                    <g:paginate params="${[nomeBuscaSimples: nomeBuscaSimples]}" total="${clinicaInstanceTotal}" />
                </g:if>
                <g:else>
                	<g:paginate total="${clinicaInstanceTotal}" />
    	        </g:else>
			</div>
		</div>