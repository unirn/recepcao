<%@ page import="br.edu.unirn.consulta.FilaEspera" %>

	<p class="help-block" id="qntdRegistro">Quantidade - ${filaEsperaInstanceTotal}</p>
            
            <table class="table table-hover table-condensed table-bordered">
            <thead>
                <tr>
                    <th style="width:19px;">
						<span id="popoverAlert"></span>
                        <input type="checkbox" id="checkTodos" value="">
                    </th>
                    <g:if test="${actionName == 'buscaAvancada'}">
	                    <g:sortableColumn params="${[nomeBuscaAvancada:nomeBuscaAvancada,clinicaBuscaAvancada:clinicaBuscaAvancada,turnoBuscaAvancada:turnoBuscaAvancada,statusBuscaAvancada:statusBuscaAvancada]}" property="nome" title="Nome Paciente" />
    	                <th>Idade</th>
    	                <g:sortableColumn params="${[nomeBuscaAvancada:nomeBuscaAvancada,clinicaBuscaAvancada:clinicaBuscaAvancada,turnoBuscaAvancada:turnoBuscaAvancada,statusBuscaAvancada:statusBuscaAvancada]}" property="clinica.descricao" title="Cl&iacute;nica" />
                    </g:if>
	                <g:elseif test="${actionName == 'buscaSimples'}">
	                    <g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="nome" title="Nome Paciente" />
	    	            <th>Idade</th>
	    	            <g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="clinica.descricao" title="Cl&iacute;nica" />
	                </g:elseif>
	                <g:else>
	                	<g:sortableColumn  property="nome" title="Nome Paciente" />
	    	            <th>Idade</th>
	    	            <g:sortableColumn  property="clinica.descricao" title="Cl&iacute;nica" />
	    	        </g:else>
                    <th>Turno</th>
					<th>Status</th>
					<th>Marcar Consulta</th>
					<th>Editar</th>
                </tr>
            </thead>
            <tbody>
                <g:each in="${filaEsperaInstanceList}" var="filaEsperaInstance">
	                
	                <tr id="${filaEsperaInstance?.id}">
	                    <td>
	                        <input type="checkbox" class="checkSimples" data-registroId="${filaEsperaInstance?.id}" data-campo="${controllerName}ID" value="">
	                    </td>
	                    <td>${filaEsperaInstance?.nome}</td>
	                    <td>${filaEsperaInstance?.idade}</td>
	                    <td>${filaEsperaInstance?.clinica?.descricao}</td>
	                    <td>${filaEsperaInstance?.turno}</td>
	                    <td>${filaEsperaInstance?.statusFilaEspera?.nome}</td>
	                    <td style="text-align: center;"><g:link class="btn btn-xs btn-default" action="marcarConsultaFilaEspera" id="${filaEsperaInstance?.id}" controller="paciente">Marcar</g:link></td>
	                    <td style="text-align: center;"><g:link class="btn btn-xs btn-default" action="edit" id="${filaEsperaInstance?.id}">Editar</g:link></td>
	                </tr>
	                
	           </g:each>
            </tbody>
        </table>
        <div class="row pagination-table">
	        <div class="pagination pagination-centered col-lg-12">
	        	<g:if test="${actionName == 'buscaAvancada' }">
	                   <g:paginate params="${[nomeBuscaAvancada:nomeBuscaAvancada,clinicaBuscaAvancada:clinicaBuscaAvancada,turnoBuscaAvancada:turnoBuscaAvancada,statusBuscaAvancada:statusBuscaAvancada]}" total="${filaEsperaInstanceTotal}" />
                    </g:if>
	                <g:elseif test="${actionName == 'buscaSimples' }">
	                    <g:paginate params="${[nomeBuscaSimples: nomeBuscaSimples]}" total="${filaEsperaInstanceTotal}" />
	                </g:elseif>
	                <g:else>
	                	<g:paginate total="${filaEsperaInstanceTotal}" />
	    	        </g:else>
			</div>
		</div>