<%@ page import="br.edu.unirn.consulta.FilaEspera" %>


<div class="form-group">
    <label class="col-md-2 control-label" for="nome">Nome</label>

    <div class="col-md-5">
    	<g:jQueryAutoCompletePessoa id="pessoa" name="pessoa.id" value="${filaEsperaInstance?.pessoa?.id}" name="${filaEsperaInstance?.nome}"></g:jQueryAutoCompletePessoa>
    </div>
</div>  

 <div class="form-group">
    <label class="col-md-2 control-label" for="telefoneContato">Tel. Contato</label>

    <div class="col-md-2">
        <input name="telefoneContato" type="text" class="form-control input-sm" placeholder="Telefone Contato...." value="${filaEsperaInstance?.telefoneContato}">                                          
    </div>
    
    <label class="col-md-1 control-label" for="idade">Idade</label>

    <div class="col-md-1">
        <input name="idade" type="text" class="form-control input-sm" placeholder="Idade" value="${filaEsperaInstance?.idade}">                                          
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label" for="turno">Turno</label>

    <div class="col-md-2">
    	<g:select name="turno" class="form-control input-sm" from="${br.edu.unirn.tipos.Turno?.values()}" keys="${br.edu.unirn.tipos.Turno.values()*.name()}" required="" value="${filaEsperaInstance?.turno?.name()}"/>
    </div>
    
    <label class="col-md-1 control-label" for="clinica.id">Cl&iacute;nica</label>

    <div class="col-md-2">
        <g:select id="clinica" name="clinica.id" from="${br.edu.unirn.comum.Clinica.list()}" optionKey="id" required="" value="${filaEsperaInstance?.clinica?.id}" class="form-control input-sm"/>                                         
    </div>
</div> 


<div class="form-group">
    <label class="col-md-2 control-label" for="motivo">Motivo</label>
	
    <div class="col-md-7">		
			<richui:richTextEditor name="motivo" class="form-control input-sm hidden-xs" value="${filaEsperaInstance?.motivo}" width="600"/>
    </div>
</div>