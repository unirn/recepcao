<!doctype html>
<%@page import="br.edu.unirn.tipos.StatusFilaEspera"%>
<%@page import="br.edu.unirn.consulta.StatusConsulta"%>
<%@page import="br.edu.unirn.tipos.Turno"%>
<%@page import="br.edu.unirn.comum.Clinica"%>
<html>
<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'filaEspera.label', default: 'Lista de Espera')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<div class="col-md-12">
                    
    <ol class="breadcrumb">
      <li><a href="${createLink(uri: '/')}">Visao Geral</a></li>
      <g:if test="${actionName == 'buscaSimples' || actionName == 'buscaAvancada'}">
      	<li><g:link action="list">Lista de Espera</g:link></li> 
      	<li class="active">Resultado da Busca</li>  
      </g:if>
      <g:else>
      	<li class="active">Lista de Espera</li> 
      </g:else>  
    </ol>

</div><!-- /.col-md-12 -->

<div class="col-md-12">
    
    <div class="panel panel-default">

        <div class="panel-heading">
            Lista de Espera
        </div><!-- /.panel-heading -->
        
        <div class="panel-body">
         	<g:if test="${flash.message}">
         		<div class="alert alert-dismissable alert-success">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					   <a href="${createLink(action: 'edit',params:[id:flash.id] )}" class="alert-link"><strong>${flash.nome} - </strong></a>
					   ${flash.message}	
				</div>
         	</g:if>
         	
            <fieldset>
            	<g:if test="${actionName == 'buscaSimples' || actionName == 'buscaAvancada'}">
	            		<div class="btn-group">
		                      <g:link action="list" type="button" class="btn btn-default"><span class="icon-arrow-left"></span> Voltar</g:link>
		                </div>
	            </g:if>
                <div class="btn-group">
                		<g:link action="create" class="btn btn-success"><span class="icon-plus"></span> Novo Cadastro</g:link>
                </div> 
                <div class="btn-group">
                        <button type="button" class="btn" id="deleteChecked" data-controller="${controllerName}"><span class="icon-trash"></span> Desistir</button>
                </div>  
               	
                <div class="col-sm-3 pull-right">

                    <g:form action="buscaSimples" role="search">

                            <div class="input-group">
                                <input name="nomeBuscaSimples" type="text" class="form-control" placeholder="Pesquisar...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><span class="icon-search"></span></button>
                                </span>
                            </div>

                            <a href="javascript:void(0);" class="abrirBuscaAvancada">Busca Avan&ccedil;ada <b class="caret"></b></a>
                            
                    </g:form>
                    
                </div><!-- /.col-sm-3 -->
            </fieldset>
            
            <fieldset class="well form-buscaAvancada" style="display: none;">
              <g:form action="buscaAvancada" class="" role="form">
                    <div class="row">
                         <div class="col-lg-3">
                            <div class="form-group">
                                <label for="nomeBuscaAvancada">Nome Paciente</label>
                                <input name="nomeBuscaAvancada" type="text" class="form-control input-sm" placeholder="Nome Paciente">                                          
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="clinicaBuscaAvancada">Cl&iacute;nicica</label>
                                <g:select name="clinicaBuscaAvancada" class="form-control input-sm" from="${Clinica.list()}" optionKey="id" optionValue="descricao" noSelection="['':'-- Selecione --']" />
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="turnoBuscaAvancada">Turno</label>
                                <g:select name="turnoBuscaAvancada" class="form-control input-sm" from="${Turno.values()}" keys="${Turno.values()*.name()}" noSelection="['':'-- Selecione --']" />                                        
                            </div>
                        </div> 
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="statusBuscaAvancada">Status</label>
                                <g:select name="statusBuscaAvancada" class="form-control input-sm" from="${StatusFilaEspera.values()}" keys="${StatusFilaEspera.values()*.name()}" optionValue="nome" noSelection="['':'-- Selecione --']" />          
                            </div>
                        </div>    
                        <div class="col-lg-2">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-info btn-sm btn-busca-avancada">Pesquisar</button>
                            </div> 
                        </div>                                                                     
                    </div>
              </g:form>
            </fieldset>
            
            <g:render template="resultList"/>
            
        </div><!-- /.panel-body -->
        

    </div><!-- /.panel -->

</div><!-- /.col-md-12 -->	
<g:javascript>
	configuracoesGerais.ativarMenu("filaEsperaMenu");
</g:javascript>
</body>
</html>