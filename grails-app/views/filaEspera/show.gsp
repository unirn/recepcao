
<%@ page import="br.edu.unirn.consulta.FilaEspera" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'filaEspera.label', default: 'FilaEspera')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="show-filaEspera" class="contentCrud" role="main">
			<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
					<span>Lista Espera</span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
						<div class="alert" role="status">
							 <button type="button" class="close" data-dismiss="alert">&times;</button>
							 <strong>${flash.message}</strong>
						</div>
					</g:if>
					<ol class="property-list filaEspera">
					
						<g:if test="${filaEsperaInstance?.pessoa}">
						<li class="fieldcontain">
							<span id="pessoa-label" class="property-label"><g:message code="filaEspera.pessoa.label" default="Pessoa" /></span>
							
								<span class="property-value" aria-labelledby="pessoa-label"><g:link controller="pessoa" action="show" id="${filaEsperaInstance?.pessoa?.id}">${filaEsperaInstance?.pessoa?.encodeAsHTML()}</g:link></span>
							
						</li>
						</g:if>
					
						<g:if test="${filaEsperaInstance?.motivo}">
						<li class="fieldcontain">
							<span id="motivo-label" class="property-label"><g:message code="filaEspera.motivo.label" default="Motivo" /></span>
									<span class="property-value" aria-labelledby="motivo-label">${filaEsperaInstance.motivo.decodeHTML()} </span>
						</li>
						</g:if>
					
						<g:if test="${filaEsperaInstance?.clinica}">
						<li class="fieldcontain">
							<span id="clinica-label" class="property-label"><g:message code="filaEspera.clinica.label" default="Clinica" /></span>
							
								<span class="property-value" aria-labelledby="clinica-label"><g:link controller="clinica" action="show" id="${filaEsperaInstance?.clinica?.id}">${filaEsperaInstance?.clinica?.encodeAsHTML()}</g:link></span>
							
						</li>
						</g:if>
						
						<g:if test="${filaEsperaInstance?.statusFilaEspera}">
						<li class="fieldcontain">
							<span id="statusFilaEspera-label" class="property-label"><g:message code="filaEspera.statusFilaEspera.label" default="Status da Fila Espera" />:</span>
							
								<span class="property-value" aria-labelledby="statusFilaEspera-label">${filaEsperaInstance?.statusFilaEspera}</span>
							
						</li>
						</g:if>
					
						<g:if test="${filaEsperaInstance?.dateCreated}">
						<li class="fieldcontain">
							<span id="dateCreated-label" class="property-label"><g:message code="filaEspera.dateCreated.label" default="Date Created" /></span>
							
								<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${filaEsperaInstance?.dateCreated}" /></span>
							
						</li>
						</g:if>
					
						<g:if test="${filaEsperaInstance?.idade}">
						<li class="fieldcontain">
							<span id="idade-label" class="property-label"><g:message code="filaEspera.idade.label" default="Idade" /></span>
							
								<span class="property-value" aria-labelledby="idade-label"><g:fieldValue bean="${filaEsperaInstance}" field="idade"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${filaEsperaInstance?.nome}">
						<li class="fieldcontain">
							<span id="nome-label" class="property-label"><g:message code="filaEspera.nome.label" default="Nome" /></span>
							
								<span class="property-value" aria-labelledby="nome-label"><g:fieldValue bean="${filaEsperaInstance}" field="nome"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${filaEsperaInstance?.telefoneContato}">
						<li class="fieldcontain">
							<span id="telefoneContato-label" class="property-label"><g:message code="filaEspera.telefoneContato.label" default="Telefone Contato" /></span>
							
								<span class="property-value" aria-labelledby="telefoneContato-label"><g:fieldValue bean="${filaEsperaInstance}" field="telefoneContato"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${filaEsperaInstance?.turno}">
						<li class="fieldcontain">
							<span id="turno-label" class="property-label"><g:message code="filaEspera.turno.label" default="Turno" /></span>
							
								<span class="property-value" aria-labelledby="turno-label"><g:fieldValue bean="${filaEsperaInstance}" field="turno"/></span>
							
						</li>
						</g:if>
					
					</ol>
					<g:form class="form">
						<div class="form-actions">
							<g:hiddenField name="id" value="${filaEsperaInstance?.id}" />
							<g:link class="btn btn-primary" action="edit" id="${filaEsperaInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
							<g:actionSubmit class="btn" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</div>
					</g:form>
				</div>
			</div>
		</div>
	</body>
</html>
