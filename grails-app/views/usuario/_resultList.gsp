<%@ page import="br.edu.unirn.seguranca.Usuario"%>

	<p class="help-block" id="qntdRegistro">Quantidade - ${usuarioInstanceTotal}</p>
            
            <table class="table table-hover table-condensed table-bordered">
            <thead>
                <tr>
                    <th style="width:19px;">
						<span id="popoverAlert"></span>
                        <input type="checkbox" id="checkTodos" value="">
                    </th>
	                <g:if test="${actionName == 'buscaSimples'}">
	                    <g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="login" title="${message(code: 'usuario.login.label', default: 'Login')}" />
						<g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="nome" title="${message(code: 'usuario.nome.label', default: 'Nome')}" />
						<g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="email" title="${message(code: 'usuario.email.label', default: 'E-Mail')}" />
						<g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="habilitado" title="${message(code: 'usuario.habilitado.label', default: 'Habilitado')}" />
	                </g:if>
	                <g:else>
	                	<g:sortableColumn property="login" title="${message(code: 'usuario.login.label', default: 'Login')}" />
						<g:sortableColumn property="nome" title="${message(code: 'usuario.nome.label', default: 'Nome')}" />
						<g:sortableColumn property="email" title="${message(code: 'usuario.email.label', default: 'E-Mail')}" />
						<g:sortableColumn property="habilitado" title="${message(code: 'usuario.habilitado.label', default: 'Habilitado')}" />
	    	        </g:else>
                    
					<th style="width: 10%;">Editar</th>
                </tr>
            </thead>
            <tbody>
                <g:each in="${usuarioInstanceList}" var="usuarioInstance">
	                
	                <tr id="${usuarioInstance?.id}">
	                    <td>
	                        <input type="checkbox" class="checkSimples" data-registroId="${usuarioInstance?.id}" data-campo="${controllerName}ID" value="">
	                    </td>
	                    <td>${fieldValue(bean: usuarioInstance, field: "login")}</td>
						
						<td>${fieldValue(bean: usuarioInstance, field: "nome")}</td>
							
						<td>${fieldValue(bean: usuarioInstance, field: "email")}</td>
						
						<td><g:formatBoolean boolean="${usuarioInstance?.habilitado}" /></td>
	                    <td style="text-align: center;"><g:link class="btn btn-xs btn-default" action="edit" id="${usuarioInstance?.id}">Editar</g:link></td>
	                </tr>
	                
	           </g:each>
            </tbody>
        </table>
        <div class="row pagination-table">
	        <div class="pagination pagination-centered col-lg-12">
                <g:if test="${actionName == 'buscaSimples' }">
                    <g:paginate params="${[nomeBuscaSimples: nomeBuscaSimples]}" total="${usuarioInstanceTotal}" />
                </g:if>
                <g:else>
                	<g:paginate total="${usuarioInstanceTotal}" />
    	        </g:else>
			</div>
		</div>