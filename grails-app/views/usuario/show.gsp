
<%@ page import="br.edu.unirn.seguranca.Usuario" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="show-usuario" class="contentCrud" role="main">
			<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
					<span>Usu&aacute;rio</span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
						<div class="alert" role="status">
							 <button type="button" class="close" data-dismiss="alert">&times;</button>
							 <strong>${flash.message}</strong>
						</div>
					</g:if>
					<ol class="property-list usuario">
					
						<g:if test="${usuarioInstance?.nome}">
						<li class="fieldcontain">
							<span id="nome-label" class="property-label"><g:message code="usuario.nome.label" default="Nome" /></span>
							
								<span class="property-value" aria-labelledby="nome-label"><g:fieldValue bean="${usuarioInstance}" field="nome"/></span>
							
						</li>
						</g:if>
						
						<g:if test="${usuarioInstance?.email}">
						<li class="fieldcontain">
							<span id="email-label" class="property-label"><g:message code="usuario.email.label" default="E-mail" /></span>
							
								<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${usuarioInstance}" field="email"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${usuarioInstance?.login}">
						<li class="fieldcontain">
							<span id="username-label" class="property-label"><g:message code="usuario.username.label" default="Login" /></span>
							
								<span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${usuarioInstance}" field="login"/></span>
							
						</li>
						</g:if>
					
					
						<g:if test="${usuarioInstance?.habilitado}">
						<li class="fieldcontain">
							<span id="enabled-label" class="property-label"><g:message code="usuario.enabled.label" default="Habilitado" /></span>
							
								<span class="property-value" aria-labelledby="enabled-label"><g:formatBoolean boolean="${usuarioInstance?.habilitado}" /></span>
							
						</li>
						</g:if>
						
						<g:if test="${usuarioInstance?.ultimoAcesso}">
						<li class="fieldcontain">
							<span id="enabled-label" class="property-label"><g:message code="usuario.enabled.label" default="Ultimo Acesso" /></span>
							
								<span class="property-value" aria-labelledby="enabled-label"><g:formatDate format="dd/MM/yyyy" date="${usuarioInstance?.ultimoAcesso}" true="Sim" false="Não"/></span>
							
						</li>
						</g:if>
								
						
					</ol>
					<g:form class="form">
						<div class="form-actions">
							<g:hiddenField name="id" value="${usuarioInstance?.id}" />
							<g:link class="btn btn-primary" action="edit" id="${usuarioInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
						</div>
					</g:form>
				</div>
			</div>
		</div>
	</body>
</html>
