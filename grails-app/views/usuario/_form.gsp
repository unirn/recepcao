<%@ page import="br.edu.unirn.seguranca.Usuario" %>
<%@ page import="br.edu.unirn.tipos.TipoUsuario" %>

<div class="form-group">
    <label class="col-md-2 control-label" for="nome">Nome</label>

    <div class="col-md-4">
   		<g:textField name="nome"  value="${usuarioInstance?.nome}" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm" placeholder="Nome..."/>
    </div>
</div> 

<div class="form-group">
    <label class="col-md-2 control-label" for="login">Login</label>

    <div class="col-md-3">
    	<g:textField name="login"  value="${usuarioInstance?.login}" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm" placeholder="Login..."/>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label" for="email">E-mail</label>

    <div class="col-md-3">
    	<g:textField type="email" name="email"  value="${usuarioInstance?.email}" data-validate="{required: true,email:true messages:{required:'Campo obrigatorio',email:'Digite um E-mail valido'}}" class="form-control input-sm" placeholder="Email..."/>
    </div>
</div>  


<div class="form-group">
    <label class="col-md-2 control-label" for="senha">Senha</label>

    <div class="col-md-2">
    	<g:passwordField name="senha" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm"/>
    </div>
    
    <label class="col-md-1 control-label" for="senhaConfirm">Conf.Senha</label>

    <div class="col-md-2">
    	<g:passwordField name="senhaConfirm" data-validate="{required: true,equalTo:'#senha' messages:{required:'Campo obrigatorio',equalTo:'Senhas diferentes'}}" class="form-control input-sm"/>
    </div>
</div>  

<div class="form-group">
	<label class="col-md-2 control-label" for="tipoUsuario">Tipo Usuario</label>

    <div class="col-md-2">
    	<g:select name="tipoUsuario" from="${TipoUsuario.values()}" keys="${TipoUsuario.values()*.name()}" optionValue="descricao" class="form-control input-sm"/>
    </div>
</div>

<div class="form-group">
	<div class="col-lg-offset-2 col-lg-10 col-md-offset-2 col-md-10">
		<div class="checkbox">
		    <label>
		      <g:checkBox name="habilitado" value="${usuarioInstance?.habilitado}" /> Habilitado
		    </label>
		</div>
	</div>
</div>