<%@ page import="br.edu.unirn.seguranca.Usuario"%>
<!doctype html>
<html lang="en">
<head>
<title>Clínicas Integradas - Núcleo DEV UNIRN</title>
<r:require module="bootstrap" />
<r:layoutResources />

<style type="text/css">
/* Override some defaults */
html,body {
	background-color: #eee;
}

body {
	padding-top: 100px;
}

.container {
	width: 300px;
}

/* The white background content wrapper */
.container>.content {
	background-color: #fff;
	padding: 20px;
	margin: 0 -20px;
	-webkit-border-radius: 10px 10px 10px 10px;
	-moz-border-radius: 10px 10px 10px 10px;
	border-radius: 10px 10px 10px 10px;
	-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .15);
	-moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .15);
	box-shadow: 0 1px 2px rgba(0, 0, 0, .15);
}

.login-form {
	margin-left: 35px;
}

legend {
	margin-right: -50px;
	font-weight: bold;
	color: #404040;
}
</style>
</head>

<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a href="${createLink(uri: '/')}" class="brand">Sistema das
					Clínicas Integradas - UNIRN</a>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="content">
			<div class="row">
				<div class="login-form">
					<g:hasErrors bean="${usuarioInstance}">
						<ul class="errors" role="alert">
							<g:eachError bean="${usuarioInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</g:hasErrors>
					<g:if test="${flash.message}">
						<div class="alert alert-success">
							${flash.message}
						</div>
					</g:if>
					<h2>Solicitação de Cadastro</h2>
					<g:form action="efetivarSolicitacao" autocomplete='off'>
						<fieldset>
							<div
								class="clearfix ${hasErrors(bean: usuarioInstance, field: 'nome', 'error')} required">
								<g:textField name="nome" required=""
									value="${usuarioInstance?.nome}" placeholder="Nome" />
							</div>

							<div
								class="clearfix ${hasErrors(bean: usuarioInstance, field: 'email', 'error')} required">
								<g:textField name="email" required=""
									value="${usuarioInstance?.email}" placeholder="E-mail" />
							</div>

							<div
								class="clearfix ${hasErrors(bean: usuarioInstance, field: 'username', 'error')} required">

								<g:textField name="username" required=""
									value="${usuarioInstance?.username}" placeholder="Login" />
							</div>

							<div
								class="clearfix ${hasErrors(bean: usuarioInstance, field: 'password', 'error')} required">

								<g:passwordField name="password" required="" placeholder="Senha" />
							</div>

						</fieldset>
						<div class="form-actions">
							<g:submitButton name="create" class="btn btn-primary"
								value="Solicitar Cadastro" />
							<g:link controller="login" class="btn">Voltar</g:link>
						</div>
					</g:form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
