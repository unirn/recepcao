<%@ page import="br.edu.unirn.comum.Paciente"%>

	<p class="help-block" id="qntdRegistro">Quantidade - ${consultaInstanceTotal}</p>
            
            <table class="table table-hover table-condensed table-bordered">
            <thead>
                <tr>
                    <g:if test="${actionName == 'buscaAvancada'}">
	                    <g:sortableColumn params="${[nomePacienteBuscaAvancada:nomePacienteBuscaAvancada,dataInicialBuscaAvancada: dataInicialBuscaAvancada,dataFimBuscaAvancada:dataFimBuscaAvancada,clinicaIdBuscaAvancada:clinicaIdBuscaAvancada,statusConsultaIdBuscaAvancada:statusConsultaIdBuscaAvancada]}" 
	                    property="id" title="Núm. Pront." />
    	                <g:sortableColumn params="${[nomePacienteBuscaAvancada:nomePacienteBuscaAvancada,dataInicialBuscaAvancada: dataInicialBuscaAvancada,dataFimBuscaAvancada:dataFimBuscaAvancada,clinicaIdBuscaAvancada:clinicaIdBuscaAvancada,statusConsultaIdBuscaAvancada:statusConsultaIdBuscaAvancada]}" 
    	                property="paciente.pessoa.nome" title="Nome Paciente" />
    	                <g:sortableColumn params="${[nomePacienteBuscaAvancada:nomePacienteBuscaAvancada,dataInicialBuscaAvancada: dataInicialBuscaAvancada,dataFimBuscaAvancada:dataFimBuscaAvancada,clinicaIdBuscaAvancada:clinicaIdBuscaAvancada,statusConsultaIdBuscaAvancada:statusConsultaIdBuscaAvancada]}" 
    	                property="dataConsulta" title="Data" />
                    </g:if>
	                <g:elseif test="${actionName == 'buscaSimples'}">
	                    <g:sortableColumn params="${[dataInicialBuscaSimples: dataInicialBuscaSimples,dataFimBuscaSimples:dataFimBuscaSimples]}" property="id" title="Núm. Pront." />
	    	            <g:sortableColumn params="${[dataInicialBuscaSimples: dataInicialBuscaSimples,dataFimBuscaSimples:dataFimBuscaSimples]}" property="paciente.pessoa.nome" title="Nome Paciente" />
	                	<g:sortableColumn params="${[dataInicialBuscaSimples: dataInicialBuscaSimples,dataFimBuscaSimples:dataFimBuscaSimples]}" property="dataConsulta" title="data" />
	                	
	                </g:elseif>
	                <g:else>
	                	<g:sortableColumn  property="paciente.numeroProntuario" title="Núm. Pront." />
	    	            <g:sortableColumn  property="paciente.pessoa.nome" title="Nome Paciente" />
	    	            <g:sortableColumn  property="dataConsulta" title="Data" />
	    	        </g:else>
                    <th>Tel.Contato</th>
				    <th>Sala</th>
				    <th>Hor&aacute;rio</th>
				    <th>Status Pg.</th>
				    <th>Reag.</th>
				    <th>Hist.</th>
				    <th>Status</th>
				</tr>
			</thead>
			<tbody>
				<g:each var="consultaInstance" in="${consultaInstanceList}">
					<tr id="Consulta${consultaInstance.id}">
						<td>${consultaInstance.paciente.numeroProntuario}</td>
						<td>${consultaInstance.paciente.pessoa.nome}</td>
						<td><g:formatDate date="${consultaInstance.dataConsulta}" format="dd/MM/yyyy"/> </td>
						<td>${consultaInstance.paciente.pessoa.telefoneContato}<g:if test="${consultaInstance.paciente.pessoa.telefoneCelular}">/${consultaInstance.paciente.pessoa.telefoneCelular}</g:if> </td>
						<td>${consultaInstance.sala.nome} (${consultaInstance.sala.clinica.descricao})</td>
						<td>${consultaInstance.horarioConsulta}</td>
						<td id="consulta-${consultaInstance.id}" style="background-color: ${consultaInstance?.statusPagamento?.corListagem}" class='td-status'><a href="#" onclick="alterarPagamentoConsulta(${consultaInstance.id})">${consultaInstance.statusPagamento}</a></td>
						<td><g:link action="reagendamento" id="${consultaInstance.id}"><g:img file="skin/next.png" onclick="return confirm('Deseja realmente reagendar a consulta!?');"/></g:link></td>
						<td><g:link action="historicoAlteracaoConsulta" id="${consultaInstance.paciente.id}" controller="alteracaoStatusConsulta"><g:img file="skin/database_edit.png"/></g:link></td>
						<td style="background-color: ${consultaInstance?.statusConsulta?.corListagem}" class='td-status'><a href="#" onclick="alterarStatusConsulta(${consultaInstance.statusConsulta.id},${consultaInstance.id})">${consultaInstance.statusConsulta}</a></td>
					</tr>
				</g:each>
            </tbody>
        </table>
        <div class="row pagination-table">
	        <div class="pagination pagination-centered col-lg-12">
	        	<g:if test="${actionName == 'buscaAvancada' }">
	                   <g:paginate params="${[nomePacienteBuscaAvancada:nomePacienteBuscaAvancada,dataInicialBuscaAvancada: dataInicialBuscaAvancada,dataFimBuscaAvancada:dataFimBuscaAvancada,clinicaIdBuscaAvancada:clinicaIdBuscaAvancada,statusConsultaIdBuscaAvancada:statusConsultaIdBuscaAvancada]}" 
	                   total="${consultaInstanceTotal}" />
                    </g:if>
	                <g:elseif test="${actionName == 'buscaSimples' }">
	                    <g:paginate params="${[dataInicialBuscaSimples: dataInicialBuscaSimples,dataFimBuscaSimples:dataFimBuscaSimples]}" total="${consultaInstanceTotal}" />
	                </g:elseif>
	                <g:else>
	                	<g:paginate total="${consultaInstanceTotal}" />
	    	        </g:else>
			</div>
		</div>