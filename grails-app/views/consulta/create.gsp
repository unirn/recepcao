<%@ page import="br.edu.unirn.consulta.Consulta" %>
<!doctype html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'consulta.label', default: 'Consulta')}" />
<title><g:message code="default.create.label"
		args="[entityName]" /></title>
		<resource:richTextEditor />
</head>
<body>
<div class="row">
<div class="col-md-12">
    
    <ol class="breadcrumb">
      <li><a href="${createLink(uri: '/')}">Visao Geral</a></li>
      <li><g:link action="list">Consulta</g:link></li>
      <li class="active">Cadastrar</li>        
    </ol>

</div><!-- /.col-md-12 -->
</div>
<div class="row">
	<div class="col-md-12">
	    
	    <div class="panel panel-default">
		
	        <div class="panel-heading">
	           Nova Consulta
	        </div><!-- /.panel-heading -->
	        
	        <div class="panel-body">
	        <g:if test="${flash.message}">
				<div class="alert alert-dismissable alert-danger">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					  ${flash.message}	
				</div>
			</g:if>
	        <g:form action="save" class="form-horizontal formValidate" role="form">
	        	<fieldset class="groupFields" id="dadosConsulta"> 
							
					<g:render template="form" />
	        		
	        		<div class="btn-group">
	        			   <g:link action="list" class="btn btn-default pull-right"><span class="icon-arrow-left"></span> Voltar</g:link>
	                </div> 
	                <div class="btn-group">
	                        <button type="submit" class="btn btn-success pull-right">Salvar <span class="icon-ok"></span></button>
	                </div>  
	                
	                
	            </fieldset>
	        </g:form>
	            
	        </div><!-- /.panel-body -->
	        
	
	    </div><!-- /.panel -->
	
	</div><!-- /.col-md-12 -->
</div>
<div id="modalCadastroPaciente" title="Cadastro Rapido de Paciente">
	<g:form name="cadastarPacienteRapido" controller="paciente" action="cadastroRapido">
		<div class='col-md-12'>
			<div class="row">
				<div class='col-md-12'>
					<div class='form-group'>
						<label for="nome">
							Nome do Paciente *
						</label>
						<div class="controls">
							<g:field type="text" name="nome" required="" class="form-control input-sm" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class='col-md-4'>
					<div class='form-group'>
						<label for="telefoneContato">
							Telefone *
						</label>
						<div class="controls">
							<g:field type="text" name="telefoneContato" required="" class="form-control input-sm"/>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<label for="telefoneContato">
						E-mail
					</label>
					<div class="controls">
						<g:field type="email" name="email"  class="form-control input-sm"/>
					</div>
				</div>
			</div>
		
			<div class="row">
				<div class='col-md-12'>
					<g:submitButton name="enviar" value="Cadastrar" class='btn btn-success pull-right'/>
				</div>
			</div>
		</div>
	</g:form>
</div>
<g:javascript>
	configuracoesGerais.ativarMenu("consultaMenu");
</g:javascript>
</body>
</html>
