<!doctype html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'paciente.label', default: 'Consulta')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<div class="col-md-12">
                    
    <ol class="breadcrumb">
      <li><a href="${createLink(uri: '/')}">Visao Geral</a></li>
      <g:if test="${actionName == 'buscaSimples' || actionName == 'buscaAvancada'}">
      	<li><g:link action="list">Consultas</g:link></li> 
      	<li class="active">Resultado da Busca</li>  
      </g:if>
      <g:else>
      	<li class="active">Consultas</li>
      </g:else>         
    </ol>

</div><!-- /.col-md-12 -->

<div class="col-md-12">
    
    <div class="panel panel-default">

        <div class="panel-heading">
            Consultas
        </div><!-- /.panel-heading -->
        
        <div class="panel-body">
         	<g:if test="${flash.message}">
         		<div class="alert alert-dismissable alert-success">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					   <a href="${createLink(action: 'edit',params:[id:flash.id] )}" class="alert-link"><strong>${flash.nome} - </strong></a>
					   ${flash.message}	
				</div>
         	</g:if>
         	
            <fieldset>
            	<div class="row">
	            	<div class="col-md-6">
	            	<g:if test="${actionName == 'buscaSimples' || actionName == 'buscaAvancada'}">
	            		<div class="btn-group">
		                      <g:link action="list" type="button" class="btn btn-default"><span class="icon-arrow-left"></span> Voltar</g:link>
		                </div>
	            	</g:if>
		                <div class="btn-group">
		                		<g:link action="create" class="btn btn-success"><span class="icon-calendar"></span> Marcar Consulta</g:link>
		                </div> 
	               	</div>
	                <div class="col-md-6 pull-right">
	
	                    <g:form action="buscaSimples" class=" formValidate" role="form">
	                    
						  <div class="form-group">
						  <div class="col-md-5">
						    <g:jQueryDatePicker name="dataInicialBuscaSimples" classField="form-control" placeholder="Data inicial.." required=""/>
						    </div>
						    <div class="col-md-5" style="padding-left: 0px; padding-right: 0px;">
						    <g:jQueryDatePicker name="dataFimBuscaSimples" classField="form-control" placeholder="Data final.." required=""/>
						    </div>
						    <div class="col-md-2" style="padding-left: 0px;">
						     <button type="submit" class="btn btn-default" ><span class="icon-search"></span></button>
						    </div>
						  </div>
						 
						</g:form>
						<a href="javascript:void(0);" class="abrirBuscaAvancada" style="margin-left: 15px;">Busca Avan&ccedil;ada <b class="caret" ></b></a>
						
	                </div><!-- /.col-md-6 -->
            	</div>
            </fieldset>
            
            <fieldset class="well form-buscaAvancada" style="display: none;">
              <g:form action="buscaAvancada" class="" role="form">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="dataInicialBuscaAvancada">Data In&iacute;cio</label>
                                <g:jQueryDatePicker name="dataInicialBuscaAvancada" classField="form-control input-sm" placeholder="Data inicial.."/>                                
                            </div>
                        </div>
                         <div class="col-lg-2">
                            <div class="form-group">
                                <label for="dataFimBuscaAvancada">Data Fim</label>
                                <g:jQueryDatePicker name="dataFimBuscaAvancada" classField="form-control input-sm" placeholder="Data final.."/>                                    
                            </div>
                        </div>
                         <div class="col-lg-2">
                            <div class="form-group">
                                <label for="statusConsultaIdBuscaAvancada">Status Consulta</label>
                                <g:select id="statusConsultaIdBuscaAvancada" class="form-control input-sm" name="statusConsultaIdBuscaAvancada" from="${br.edu.unirn.consulta.StatusConsulta.list()}" optionKey="id" value="${consultaInstance?.statusConsulta?.id}" noSelection="${['':'-- Todas --']}"/>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                    	<div class="col-lg-4">
                            <div class="form-group">
                                <label for="nomePacienteBuscaAvancada">Nome Paciente</label>
                                <g:textField id="nomePacienteBuscaAvancada" class="form-control input-sm" name="nomePacienteBuscaAvancada" placeholder="Nome"/>              
                            </div>
                        </div>
                        
                    	<div class="col-lg-3">
                            <div class="form-group">
                                <label for="clinicaIdBuscaAvancada">Cl&iacute;nica</label>
                                <g:select id="clinicaIdBuscaAvancada" class="form-control input-sm" name="clinicaIdBuscaAvancada" from="${br.edu.unirn.comum.Clinica.list()}" optionKey="id" value="${clinicaInstance?.id}" noSelection="${['':'-- Todas --']}"/>              
                            </div>
                        </div>
                      
                        <div class="col-lg-4">
                            <div class="btn-group pull-right">
                                <button type="submit" class="btn btn-info btn-sm btn-busca-avancada">Pesquisar</button>
                            </div> 
                        </div> 
                    </div>
              </g:form>
            </fieldset>
            
            	<g:render template="resultList"/>
            
        </div><!-- /.panel-body -->
        

    </div><!-- /.panel -->

</div><!-- /.col-md-12 -->	
<g:render template="alterarStatusConsulta"/>
<g:render template="alterarPagamentoConsulta"/>
<g:render template="terminarCadastroPaciente"/>
<g:javascript>
	configuracoesGerais.ativarMenu("consultaMenu");
</g:javascript>
</body>
</html>
