<%@ page import="br.edu.unirn.consulta.Consulta" %>


<div class="form-group">
    <label class="col-md-2 control-label" for="pacienteAutoComplete">Paciente <span class="required-indicator">*</span></label>
    <div class="col-md-6">
    	<g:JQueryAutoCompletePaciente required="" value="${consultaInstance?.paciente?.id}"/><a href="javascript:void(0);" id="linkCadastroPaciente">Cadastrar Paciente</a>                                         
    </div>
</div>  

<div class="form-group">
    <label class="col-md-2 control-label" for="dataConsulta">Data Consulta <span class="required-indicator">*</span></label>

    <div class="col-md-2">
    	<g:jQueryDatePicker name="dataConsulta"  classField="form-control input-sm" value="${consultaInstance?.dataConsulta}" oncomplete="atualizarComboHorario();" required=""/>
    </div>
    <label class="col-md-1 control-label" for="turno">Turno <span class="required-indicator">*</span></label>

    <div class="col-md-2">
    	<g:select name="turno" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm" from="${br.edu.unirn.tipos.Turno?.values()}" keys="${br.edu.unirn.tipos.Turno.values()*.name()}" required="" value="${consultaInstance?.turno?.name()}"/>
    </div>
    
</div>

<div class="form-group">
    <label class="col-md-2 control-label" for="clinica.id">Cl&iacute;nica <span class="required-indicator">*</span></label>

    <div class="col-md-2">
    	<g:select id="clinica" name="clinica.id" from="${br.edu.unirn.comum.Clinica.list()}" optionKey="id" required="" value="${consultaInstance?.sala?.clinica?.id}" class="form-control input-sm" noSelection="${[null:'-- Selecione --']}"/>
    </div>
    <div class="col-md-2">
    	<g:select id="sala" name="sala.id" from="${br.edu.unirn.comum.Sala.list()}" optionKey="id" required="" value="${consultaInstance?.sala?.id}" class="form-control input-sm" style="display:none;" noSelection="${[null:'-- Selecione --']}"/>
	</div>
</div>  

<div class="form-group">
    <label class="col-md-2 control-label" for="clinicaIndicacao.id">Cl&iacute;n. Indica&ccedil;&atilde;o <span class="required-indicator">*</span></label>

    <div class="col-md-2">
    	<g:select id="clinicaIndicacao" name="clinicaIndicacao.id" from="${br.edu.unirn.comum.Clinica.list()}" optionKey="id" value="${consultaInstance?.salaIndicacao?.clinica?.id}" class="form-control input-sm" noSelection="${[null:'-- Selecione --']}"/>
    </div>
    <div class="col-md-2">
    	<g:select id="salaIndicacao" name="salaIndicacao.id" from="${br.edu.unirn.comum.Sala.list()}" optionKey="id" value="${consultaInstance?.salaIndicacao?.id}" class="form-control input-sm" style="display:none;" noSelection="${[null:'-- Selecione --']}"/>
	</div>
</div>  

<div class="form-group">
    <label class="col-md-2 control-label" for="horarioConsulta">Hor&aacute;rio Consulta <span class="required-indicator">*</span></label>

    <div class="col-md-3">
    	<g:comboHora clinicaID="${consultaInstance?.sala?.clinica?.id}" dataConsulta="${consultaInstance?.dataConsulta}" turno="${consultaInstance?.turno}"
				horaConsulta="${consultaInstance?.horarioConsulta}" name="horarioConsulta"
				pacienteID="${consultaInstance?.paciente?.id}"/>
    </div>
</div> 	
	

<g:if test="${consultaInstance?.id >0}">
	<div class="form-group">
	    <label class="col-md-2 control-label" for="statusConsulta">Status Consulta</label>
	
	    <div class="col-md-2">
	    	<g:select id="statusConsulta" name="statusConsulta.id" class="form-control input-sm" from="${br.edu.unirn.consulta.StatusConsulta.list()}" value="${consultaInstance?.statusConsulta?.id}" optionKey="id"/>
	    </div>
	</div> 
</g:if>

<div class="form-group">
    <label class="col-md-2 control-label" for="justificativa">Justificativa</label>
	
    <div class="col-md-7">
    		<richui:richTextEditor name="justificativa" class="form-control input-sm hidden-xs" value="${consultaInstance?.justificativa}" width="600"/>		
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label" for="motivo">Motivo</label>
	
    <div class="col-md-7">
    		<richui:richTextEditor name="motivo" class="form-control input-sm hidden-xs" value="${consultaInstance?.motivo}" width="600"/>		
    </div>
</div>
