
<%@ page import="br.edu.unirn.consulta.Consulta" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'consulta.label', default: 'Consulta')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="show-consulta" class="contentCrud" role="main">
			<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
					<span>Consulta</span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
						<div class="alert" role="status">
							 <button type="button" class="close" data-dismiss="alert">&times;</button>
							 <strong>${flash.message}</strong>
						</div>
					</g:if>
					<ol class="property-list consulta">
					
						<g:if test="${consultaInstance?.dataConsulta}">
						<li class="control-group">
							<span id="dataConsulta-label" class="property-label"><g:message code="consulta.dataConsulta.label" default="Data Consulta" /></span>
							
								<span class="property-value" aria-labelledby="dataConsulta-label"><g:formatDate date="${consultaInstance?.dataConsulta}" format="dd/MM/yyyy"/></span>
							
						</li>
						</g:if>
						
						<g:if test="${consultaInstance?.usuarioMarcacao}">
						<li class="control-group">
							<span id="usuarioMarcacao-label" class="property-label"><g:message code="consulta.usuarioMarcacao.label" default="Usuário Marcação" /></span>
							
								<span class="property-value" aria-labelledby="usuarioMarcacao-label">${consultaInstance?.usuarioMarcacao}</span>
							
						</li>
						</g:if>
						<g:if test="${consultaInstance?.horarioConsulta}">
						<li class="control-group">
							<span id="horarioConsulta-label" class="property-label"><g:message code="consulta.horarioConsulta.label" default="Horario Consulta" /></span>
							
								<span class="property-value" aria-labelledby="horarioConsulta-label"><g:fieldValue bean="${consultaInstance}" field="horarioConsulta"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${consultaInstance?.justificativa}">
						<li class="control-group">
							<span id="justificativa-label" class="property-label"><g:message code="consulta.justificativa.label" default="Justificativa" /></span>
							
								<span class="property-value" aria-labelledby="justificativa-label"><g:fieldValue bean="${consultaInstance}" field="justificativa"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${consultaInstance?.motivo}">
						<li class="control-group">
							<span id="motivo-label" class="property-label"><g:message code="consulta.motivo.label" default="Motivo" /></span>
							
								<span class="property-value" aria-labelledby="motivo-label"><g:fieldValue bean="${consultaInstance}" field="motivo"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${consultaInstance?.paciente}">
						<li class="control-group">
							<span id="paciente-label" class="property-label"><g:message code="consulta.paciente.label" default="Paciente" /></span>
							
								<span class="property-value" aria-labelledby="paciente-label"><g:link controller="paciente" action="show" id="${consultaInstance?.paciente?.id}">${consultaInstance?.paciente?.encodeAsHTML()}</g:link></span>
							
						</li>
						</g:if>
					
						<g:if test="${consultaInstance?.sala}">
						<li class="control-group">
							<span id="sala-label" class="property-label"><g:message code="consulta.sala.label" default="Sala" /></span>
							
								<span class="property-value" aria-labelledby="sala-label">${consultaInstance?.sala?.clinica?.encodeAsHTML()} - ${consultaInstance?.sala?.encodeAsHTML()}</span>
							
						</li>
						</g:if>
					
						<g:if test="${consultaInstance?.salaIndicacao}">
						<li class="control-group">
							<span id="salaIndicacao-label" class="property-label"><g:message code="consulta.salaIndicacao.label" default="Sala Indicacao" /></span>
							
								<span class="property-value" aria-labelledby="salaIndicacao-label">${consultaInstance?.salaIndicacao?.clinica?.encodeAsHTML()} - ${consultaInstance?.salaIndicacao?.encodeAsHTML()}</span>
							
						</li>
						</g:if>
					
						<g:if test="${consultaInstance?.statusConsulta}">
						<li class="control-group">
							<span id="statusConsulta-label" class="property-label"><g:message code="consulta.statusConsulta.label" default="Status Consulta" /></span>
							
								<span class="property-value" aria-labelledby="statusConsulta-label">${consultaInstance?.statusConsulta?.encodeAsHTML()}</span>
							
						</li>
						</g:if>
						
						<g:if test="${consultaInstance?.valor}">
						<li class="control-group">
							<span id="valor-label" class="property-label"><g:message code="consulta.valor.label" default="Valor Consulta" /></span>
							
								<span class="property-value" aria-labelledby="Valor-label">${consultaInstance?.valor?.encodeAsHTML()}</span>
							
						</li>
						</g:if>
						
						<g:if test="${consultaInstance?.statusPagamento}">
						<li class="control-group">
							<span id="statusPagamento-label" class="property-label"><g:message code="consulta.statusPagamento.label" default="Status Pagamento" /></span>
							
								<span class="property-value" aria-labelledby="statusPagamento-label">${consultaInstance?.statusPagamento?.encodeAsHTML()}</span>
							
						</li>
						</g:if>
					
					</ol>
					<g:form>
						<fieldset class="buttons">
							<g:hiddenField name="id" value="${consultaInstance?.id}" />
							<g:link class="edit" action="edit" id="${consultaInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
							<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</fieldset>
					</g:form>
				</div>
			</div>
		</div>
	</body>
</html>
