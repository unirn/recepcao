<div id="terminarCadastroPaciente" title="Aten&ccedil;&atilde;o">
	<div class='col-md-12'>
		<div class="row">
			<div class="well">Algumas informa&ccedil;&otilde;es importantes do paciente 
			<span id='pacienteAlt'></span> est&atilde;o pendentes. &Eacute; necess&aacute;rio <strong>Concluir 
			o Cadastro</strong> para alterar o status do pagamento!</div>
			<g:form>
				<g:hiddenField name="id"/>
				<a id='btnConcluirCadastro' href='${g.createLink(controller:'paciente',action:'edit')}' class='btn btn-info btn-block'>Concluir Cadastro</a>
			</g:form>
		</div>
	</div>
</div>