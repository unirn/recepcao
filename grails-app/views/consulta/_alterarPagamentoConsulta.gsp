<div id="alteraPagamentoConsulta" title="Alteração de Pagamento da Consulta">
		<g:formRemote method="post" name="frmAlteraPagamento" before="removeMaskMoney();"
			url="[controller:'alteracaoStatusPagamento',action:'update']"
			onSuccess="resetForm('#frmAlteraStatusPagamento');fecharAlterarStatusPagamento('#alteraPagamentoConsulta',data)">
			<div class='col-md-12'>
				<g:hiddenField name="consulta.id" id="consulta" />
				<div class="row">
					<div class='col-md-6'>
						<div class='form-group'>
							<label for="statusPagamento"> Status
								Pagamento </label>
							<g:select id="statusPagamento" name="statusPagamento.id"
								from="${br.edu.unirn.consulta.StatusPagamento.list()}"
								optionKey="id" class="form-control input-sm"/>
						</div>
					</div>
					<div class='col-md-6'>
						<div class='form-group'>
							<label for="valor"> Valor </label>
							<g:textField name="valor" id="valor" class="form-control input-sm"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class='col-md-12'>
						<label class="control-label" for="valorAtual"> Valor Atual</label> <span
						id="valorAtual"></span>
					</div>
				</div>
	
				<div class="row">
					<div class='col-md-12'>
						<g:actionSubmit class="btn btn-success  pull-right" value="Realizar Alteração" />
					</div>
				</div>
			</div>
		</g:formRemote>
</div>