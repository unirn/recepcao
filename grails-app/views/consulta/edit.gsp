<%@ page import="br.edu.unirn.consulta.Consulta" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'consulta.label', default: 'Consulta')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
		<resource:richTextEditor />
	</head>
	<body>
		<div id="edit-consulta" class="contentCrud" role="main">
			<g:hasErrors bean="${consultaInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${consultaInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
						<span>Editar Consulta</span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
						<div class="alert" role="status">
							 <button type="button" class="close" data-dismiss="alert">&times;</button>
							 <strong>${flash.message}</strong>
						</div>
					</g:if>
				<g:form method="post" class="form-horizontal">
					<g:hiddenField name="id" value="${consultaInstance?.id}" />
					<g:hiddenField name="version" value="${consultaInstance?.version}" />
					<fieldset class="form">
						<g:render template="form"/>
					</fieldset>
					<div class="form-actions">
						<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
						<g:actionSubmit class="btn" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
					</div>
				</g:form>
			</div>
			<div id="modalCadastroPaciente">
				<g:form name="cadastarPacienteRapido" controller="paciente" action="cadastroRapido">
					<fieldset class="form">
						<div class="control-group">
							<label class="control-label" for="nome">
								Nome do Paciente
							</label>
							<div class="controls">
								<g:field type="text" name="nome" required="" class="input-xlarge"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="telefoneContato">
								Telefone de Contato
							</label>
							<div class="controls">
								<g:field type="text" name="telefoneContato" required=""/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="telefoneContato">
								E-mail
							</label>
							<div class="controls">
								<g:field type="email" name="email"/>
							</div>
						</div>
					</fieldset>
					<div class="form-actions">
						<g:submitButton name="enviar" value="Cadastrar"/>
					</div>
				</g:form>
			</div>
		</div>
	</div>
<g:javascript>
	configuracoesGerais.ativarMenu("consultaMenu");
</g:javascript>
	</body>
</html>
