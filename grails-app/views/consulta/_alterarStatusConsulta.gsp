<div id="dialogAlteraConsulta" title="Alteração de Status da Consulta">
	<g:formRemote method="post" name="frmAlteraStatus" before="removeMaskMoney();"
		url="[controller:'alteracaoStatusConsulta',action:'save']"
		onSuccess="resetForm('#frmAlteraStatus');fecharAlterarStatusConsulta('#dialogAlteraConsulta',data);">
		<div class='col-md-12'>
			<g:hiddenField name="consulta.id" id="consulta"/>
			<g:hiddenField name="statusConsultaAnterior.id" id="statusConsultaAnterior"/>
			<div class="row">
				<div class='col-md-12'>
					<div class='form-group'>
						<label  for="statusConsulta">
							Novo Consulta
						</label>
						<g:select id="statusConsultaAtual" name="statusConsultaAtual.id" from="${br.edu.unirn.consulta.StatusConsulta.list()}" optionKey="id" class="form-control input-sm"/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class='col-md-12'>
					<div class='form-group'>
						<label class="control-label" for="motivo"> Motivo </label>
						<g:textArea name="motivo" rows="6" style="width: 100%;" class="form-control"/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class='col-md-12'>
					<g:actionSubmit class="btn btn-success pull-right" value="Realizar Alteração"/>
				</div>
			</div>
		</div>
	</g:formRemote>
</div>

