<%@page import="br.edu.unirn.comum.Clinica"%>
<%@page import="br.edu.unirn.comum.Paciente"%>
<%@page import="br.edu.unirn.consulta.Consulta"%>
<%@page import="br.edu.unirn.consulta.StatusConsulta"%>
<meta name="layout" content="main" />
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<title>Recep&ccedil;&atilde;o</title>
		<resource:tooltip />
	</head>
	<body>
	<div class="row">
	<div class="col-lg-12 col-md-12">
	    
	    <ol class="breadcrumb">
	      <li class="active">Vis&atilde;o Geral</li>
	    </ol>
	
	</div><!-- /.col-md-12 -->
	</div>
	<div class="row">
		<div class="col-md-offset-1 col-md-9">
		    
		    <div class="panel panel-primary">
			
		        <div class="panel-heading">
		           <h5><i class="icon-share-alt icon-lg"></i> Atalhos</h5>
		        </div><!-- /.panel-heading -->
		        
		        <div class="panel-body">
			        <div class="shortcuts"> <a href="${createLink(controller:'paciente',action:'create') }" class="shortcut"><i class="shortcut-icon icon-male"></i><span class="shortcut-label">Novo Paciente</span> </a>
			        <a href="${createLink(controller:'consulta',action:'create') }" class="shortcut"><i class="shortcut-icon icon-stethoscope"></i><span class="shortcut-label">Nova Consulta</span> </a>
			        <a href="${createLink(controller:'filaEspera',action:'create') }" class="shortcut"><i class="shortcut-icon icon-list"></i> <span class="shortcut-label">Nova Fila de Espera</span> </a>
			        </div>
		        </div><!-- /.panel-body -->
		        
		
		    </div><!-- /.panel -->
		
		</div><!-- /.col-md-12 -->
	</div>
	<div class="row">
		<div class="col-md-offset-1 col-md-3">
		    
		    <div class="panel panel-primary">
			
		        <div class="panel-heading">
		           <h5><i class="icon-bar-chart icon-lg"></i> Resumo</h5>
		        </div><!-- /.panel-heading -->
		        
			        <div class="list-group" style="overflow: auto; max-height: 307px;">
			        	<a href="javascript:void(0);" class="list-group-item disabledItem">
					    <span class="badge">${Paciente.count()}</span>
					    Pacientes Cadastrados
					  	</a>
					  	<a href="javascript:void(0);" class="list-group-item disabledItem">
					    <span class="badge">${Consulta.count()}</span>
					    Total de Consultas
					  	</a>
			        	<a href="${createLink(controller:'consulta',action:'buscaAvancada',params:['dataInicialLongBuscaAvancada':listaData.dataAtual]) }" class="list-group-item">
					    <span class="badge">${totalConsultaHoje}</span>
					    Consultas de Hoje
					  	</a>
					  	<a href="${createLink(controller:'consulta',action:'buscaAvancada',params:['dataInicialLongBuscaAvancada':listaData.dataInicioMes,'dataFimLongBuscaAvancada':listaData.dataFimMes]) }" class="list-group-item">
					    <span class="badge">${totalConsultaHoje}</span>
					    Consultas do M&ecirc;s
					  	</a>	
					</div>
		        
		
		    </div><!-- /.panel -->
		
		</div><!-- /.col-md-12 -->
		<div class="col-md-3">
		    
		    <div class="panel panel-primary">
			
		        <div class="panel-heading">
		           <h5><i class="icon-tasks icon-lg"></i> Consultas por Status do Dia</h5>
		        </div><!-- /.panel-heading -->
		        
			        <div class="list-group" style="overflow: auto; max-height: 307px;">
			          <g:each in="${StatusConsulta.list()}" var="statusConsulta">
			        	<a href="${createLink(controller:'consulta',action:'buscaAvancada',params:['statusConsultaIdBuscaAvancada':statusConsulta.id,'dataInicialLongBuscaAvancada':listaData.dataAtual]) }" class="list-group-item">
					    <span class="badge">${totalConsultaPorStatus[statusConsulta.id] }</span>
					    ${statusConsulta.descricao}
					  </a>	
			          </g:each>
					</div>
		        
		
		    </div><!-- /.panel -->
		
		</div><!-- /.col-md-12 -->
		<div class="col-md-3">
		    
		    <div class="panel panel-primary">
			
		        <div class="panel-heading">
		           <h5><i class="icon-hospital icon-lg"></i> Consultas por Cl&iacute;nica</h5>
		        </div><!-- /.panel-heading -->
		        
			        <div class="list-group" style="overflow: auto; max-height: 307px;">
			          <g:each in="${Clinica.list()}" var="clinca">
			        	<a href="${createLink(controller:'consulta',action:'buscaAvancada',params:['statusConsultaIdBuscaAvancada':clinca.id,'dataInicialLongBuscaAvancada':listaData.dataAtual]) }" class="list-group-item">
					    <span class="badge">${totalConsultaPorClinica[clinca.id] }</span>
					    ${clinca.descricao}
					  </a>	
			          </g:each>
					</div>
		        
		
		    </div><!-- /.panel -->
		
		</div><!-- /.col-md-12 -->
	</div>
<g:javascript>
	configuracoesGerais.ativarMenu("inicioMenu");
</g:javascript>
	</body>
</html>