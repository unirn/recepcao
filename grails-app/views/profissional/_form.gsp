<%@ page import="br.edu.unirn.comum.Profissional" %>
<%@ page import="br.edu.unirn.comum.Pessoa" %>

<div class="control-group ${hasErrors(bean: pessoaInstance, field: 'nome', 'error')} ">
	<label class="control-label" for="nome">
		<g:message code="pessoa.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
		<g:textField name="nome" maxlength="200" value="${profissionalInstance?.pessoa?.nome}" required="" class="input-xxlarge"/>
	</div>
</div>

<div class="control-group ${hasErrors(bean: pessoaInstance, field: 'telefoneContato', 'error')} ">
	<label class="control-label" for="telefoneContato">
		<g:message code="pessoa.telefoneContato.label" default="Telefone Contato" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
		<g:textField name="telefoneContato" value="${profissionalInstance?.pessoa?.telefoneContato}" required=""/>
	</div>
</div>

<div class="control-group ${hasErrors(bean: pessoaInstance, field: 'telefoneCelular', 'error')} ">
	<label class="control-label" for="telefoneCelular">
		<g:message code="pessoa.telefoneCelular.label" default="Telefone Celular" />
		
	</label>
	<div class="controls">
		<g:textField name="telefoneCelular" value="${profissionalInstance?.pessoa?.telefoneCelular}"/>
	</div>
</div>

<div class="control-group ${hasErrors(bean: pessoaInstance, field: 'rg', 'error')} ">
	<label class="control-label" for="rg">
		<g:message code="pessoa.rg.label" default="RG" />
	</label>
	<div class="controls">
		<g:field name="rg" type="number" value="${profissionalInstance?.pessoa?.rg}"/>
	</div>
</div>

<div class="control-group ${hasErrors(bean: pessoaInstance, field: 'cpf', 'error')} ">
	<label class="control-label" for="cpf">
		<g:message code="pessoa.cpf.label" default="CPF" />
		
	</label>
	<div class="controls">
		<g:field name="cpf" type="number" value="${profissionalInstance?.pessoa?.cpf}"/>
	</div>
</div>

<div class="control-group ${hasErrors(bean: pessoaInstance, field: 'email', 'error')} ">
	<label class="control-label" for="email">
		<g:message code="pessoa.email.label" default="E-mail" />
		
	</label>
	<div class="controls">
		<g:field type="pessoa.email" name="email" value="${profissionalInstance?.pessoa?.email}"/>
	</div>
</div>

<div class="control-group ${hasErrors(bean: pessoaInstance, field: 'sexo', 'error')} required">
	<label class="control-label" for="sexo">
		<g:message code="pessoa.sexo.label" default="Sexo" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
		<g:select name="sexo" from="${br.edu.unirn.tipos.Sexo?.values()}" keys="${br.edu.unirn.tipos.Sexo.values()*.name()}" required="" value="${profissionalInstance?.pessoa?.sexo?.name()}"/>
	</div>
</div>

<div class="control-group ${hasErrors(bean: pessoaInstance, field: 'dataNascimento', 'error')} required">
	<label class="control-label" for="dataNascimento">
		<g:message code="pessoa.dataNascimento.label" default="Data Nascimento" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
		<g:JQueryDatePicker name="dataNascimento"  value="${profissionalInstance?.pessoa?.dataNascimento}"  required=""/>
	</div>
</div>

<div class="control-group ${hasErrors(bean: pessoaInstance, field: 'logradouro', 'error')} ">
	<label class="control-label" for="logradouro">
		<g:message code="pessoa.logradouro.label" default="Logradouro" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
		<g:textField name="logradouro" value="${profissionalInstance?.pessoa?.logradouro}" required=""/>
	</div>
</div>

<div class="control-group ${hasErrors(bean: pessoaInstance, field: 'numero', 'error')} ">
	<label class="control-label" for="numero">
		<g:message code="pessoa.numero.label" default="Número" />
		
	</label>
	<div class="controls">
		<g:textField name="numero" value="${profissionalInstance?.pessoa?.numero}" class="input-small"/>
	</div>
</div>

<div class="control-group ${hasErrors(bean: pessoaInstance, field: 'bairro', 'error')} ">
	<label class="control-label" for="bairro">
		<g:message code="pessoa.bairro.label" default="Bairro" />
		
	</label>
	<div class="controls">
		<g:textField name="bairro" value="${profissionalInstance?.pessoa?.bairro}"/>
	</div>
</div>

<div class="control-group ${hasErrors(bean: pessoaInstance, field: 'cidade', 'error')} required">
	<label class="control-label" for="cidade">
		<g:message code="pessoa.cidade.label" default="Cidade" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
		<g:select id="pessoa.cidade" name="cidade.id" from="${br.edu.unirn.comum.Cidade.list()}" optionKey="id" required="" value="${profissionalInstance?.pessoa?.cidade?.id}" class="many-to-one"/>
	</div>
</div>

<div class="control-group ${hasErrors(bean: pessoaInstance, field: 'cep', 'error')} ">
	<label class="control-label" for="cep">
		<g:message code="pessoa.cep.label" default="CEP" />
		
	</label>
	<div class="controls">
		<g:textField name="cep" value="${profissionalInstance?.pessoa?.cep}"/>
	</div>
</div>

<div class="control-group ${hasErrors(bean: profissionalInstance, field: 'papel', 'error')} required papel" >
	<label class="control-label" for="papel">
		<g:message code="profissional.papel.label" default="Papel" />
		<span class="required-indicator">*</span>
	</label>
	<div class="controls">
	<g:select id="papel" name="papel.id" from="${br.edu.unirn.comum.Papel.list()}" optionKey="id" required="" value="${profissionalInstance?.papel?.id}" class="many-to-one" noSelection="${[null:'-- Selecione --']}" />
	</div>
</div>

<div class="control-group ${hasErrors(bean: profissionalInstance, field: 'clinica', 'error')} papelAlunoProfessor" >
	<label class="control-label" for="clinica">
		<g:message code="profissional.clinica.label" default="Clínica" />
	</label>
	<div class="controls">
	<g:select id="clinica" name="clinica.id" from="${br.edu.unirn.comum.Clinica.list()}" optionKey="id" required="" value="${profissionalInstance?.clinica?.id}" class="many-to-one" noSelection="${[null:'-- Selecione --']}"/>
</div>
</div>

<div class="control-group ${hasErrors(bean: profissionalInstance, field: 'semestre', 'error')} papelAluno " >
	<label class="control-label" for="semestre">
		<g:message code="profissional.semestre.label" default="Semestre" />
		
	</label>
	<div class="controls">
		<g:select name="semestre" from="${br.edu.unirn.tipos.Semestre.values()}" keys="${br.edu.unirn.tipos.Semestre.values()*.name()}" value="${profissionalInstance?.semestre?.name()}" noSelection="${[null:'-- Selecione --']}"/>
	</div>
</div>
