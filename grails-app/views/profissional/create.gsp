<%@ page import="br.edu.unirn.comum.Profissional" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'profissional.label', default: 'Profissional')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="create-profissional" class="contentCrud" role="main">
			<g:hasErrors bean="${profissionalInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${profissionalInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
						<span>Cadastrar Profissional</span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
							<div class="alert" role="status">
								 <button type="button" class="close" data-dismiss="alert">&times;</button>
								 <strong>${flash.message}</strong>
							</div>
					</g:if>
					<g:form action="save" class="form-horizontal" class="form-horizontal">
					<fieldset>
						<g:render template="form" />
						<div class="form-actions">
							<g:submitButton name="create" class="btn btn-primary"
								value="${message(code: 'default.button.create.label', default: 'Create')}" />
						</div>
					</fieldset>
					</g:form>
				</div>
			</div>	
		</div>
	</body>
</html>
