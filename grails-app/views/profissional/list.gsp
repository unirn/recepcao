
<%@ page import="br.edu.unirn.comum.Profissional" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'profissional.label', default: 'Profissional')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="list-profissional" class="contentCrud" role="main">
			<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
					<span>Profissionais</span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
						<div class="alert" role="status">
							 <button type="button" class="close" data-dismiss="alert">&times;</button>
							 <strong>${flash.message}</strong>
						</div>
					</g:if>
					<table class="table table-hover table-condensed table-bordered">
			<thead>
				<tr>
					<g:sortableColumn property="pessoa.nome" title="Nome Profissional" />
					<g:sortableColumn property="id" title="ID" />
					<th>Vinculo Profissional</th>
					<th>Clínica</th>
					<th>Semestre</th>
					<th>Telefone Contato</th>
					<th>Telefone Celular</th>
					<th>Editar</th>
					<th>Remover</th>
				</tr>
			</thead>
			<tbody>
				<g:each in="${profissionalInstanceList}" var="profissionalInstance">
					<tr>
						<td><g:link action="show" id="${profissionalInstance.id}">
								${fieldValue(bean: profissionalInstance, field: "pessoa")}
							</g:link></td>
						<td>
							${fieldValue(bean: profissionalInstance, field: "id")}
						</td>
						
						<td>
							${fieldValue(bean: profissionalInstance, field: "papel")}
						</td>
						
						<td>
							${fieldValue(bean: profissionalInstance, field: "clinica")}
						</td>
						
						<td>
							${fieldValue(bean: profissionalInstance, field: "semestre")}
						</td>
						
						<td>
							${profissionalInstance.pessoa.telefoneContato}
						</td>

						<td>
							${profissionalInstance.pessoa.telefoneCelular}
						</td>

						<td><g:link class="btn" action="edit"
								id="${profissionalInstance?.id}">
								<g:message code="default.button.edit.label" default="Edit" />
							</g:link></td>
						<td><g:form>
								<g:hiddenField name="id" value="${profissionalInstance?.id}" />
								<g:actionSubmit class="btn" action="delete"
									value="${message(code: 'default.button.delete.label', default: 'Delete')}"
									onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</g:form></td>

					</tr>
				</g:each>
			</tbody>
		</table>
			<div class="pagination pagination-centered">
				<ul>
					<li><g:paginate total="${profissionalInstanceTotal}" /></li>
				</ul>
			</div>
		</div>
	</div>
</div>
	</body>
</html>
