<%@ page import="br.edu.unirn.comum.Profissional" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'profissional.label', default: 'Profissional')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="edit-profissional" class="contentCrud" role="main">
			<g:hasErrors bean="${profissionalInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${profissionalInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
						<span>Editar Profissional</span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
						<div class="alert" role="status">
							 <button type="button" class="close" data-dismiss="alert">&times;</button>
							 <strong>${flash.message}</strong>
						</div>
					</g:if>	
					<g:form method="post" class="form-horizontal" >
						<g:hiddenField name="id" value="${profissionalInstance?.id}" />
						<g:hiddenField name="version" value="${profissionalInstance?.version}" />
						<fieldset class="form">
							<g:render template="form"/>
						</fieldset>	
						<div class="form-actions">
							<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
							<g:actionSubmit class="btn" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</div>
					</g:form>
				</div>
			</div>
		</div>
	</body>
</html>
