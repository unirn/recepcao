
<%@ page import="br.edu.unirn.comum.Profissional" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'profissional.label', default: 'Profissional')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<g:set value="${profissionalInstance.pessoa}" var="pessoaInstance"/>
	
		<div id="show-profissional" class="content scaffold-show well" role="main">
			<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
					<span>Profissional</span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
						<div class="alert" role="status">
							 <button type="button" class="close" data-dismiss="alert">&times;</button>
							 <strong>${flash.message}</strong>
						</div>
					</g:if>
					<ol class="property-list profissional">
						
						
						<g:if test="${profissionalInstance?.papel}">
						<li class="control-group">
							<span id="papel-label" class="property-label"><g:message code="profissional.papel.label" default="Papel" /></span>
							
								<span class="property-value" aria-labelledby="papel-label">${profissionalInstance?.papel?.encodeAsHTML()}</span>
		
						</li>
						</g:if>
					  
					  </li>
						<g:if test="${pessoaInstance?.nome}">
						<li class="control-group">
							<span id="nome-label" class="property-label"><g:message code="pessoa.nome.label" default="Nome" /></span>
							
								<span class="property-value" aria-labelledby="nome-label"><g:fieldValue bean="${pessoaInstance}" field="nome"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.nomePai}">
						<li class="control-group">
							<span id="nomePai-label" class="property-label"><g:message code="pessoa.nomePai.label" default="Nome Pai" /></span>
							
								<span class="property-value" aria-labelledby="nomePai-label"><g:fieldValue bean="${pessoaInstance}" field="nomePai"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.nomeMae}">
						<li class="control-group">
							<span id="nomeMae-label" class="property-label"><g:message code="pessoa.nomeMae.label" default="Nome Mae" /></span>
							
								<span class="property-value" aria-labelledby="nomeMae-label"><g:fieldValue bean="${pessoaInstance}" field="nomeMae"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.ocupacao}">
						<li class="control-group">
							<span id="ocupacao-label" class="property-label"><g:message code="pessoa.ocupacao.label" default="Ocupacao" /></span>
							
								<span class="property-value" aria-labelledby="ocupacao-label"><g:fieldValue bean="${pessoaInstance}" field="ocupacao"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.rg}">
						<li class="control-group">
							<span id="rg-label" class="property-label"><g:message code="pessoa.rg.label" default="Rg" /></span>
							
								<span class="property-value" aria-labelledby="rg-label"><g:fieldValue bean="${pessoaInstance}" field="rg"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.cpf}">
						<li class="control-group">
							<span id="cpf-label" class="property-label"><g:message code="pessoa.cpf.label" default="Cpf" /></span>
							
								<span class="property-value" aria-labelledby="cpf-label"><g:fieldValue bean="${pessoaInstance}" field="cpf"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.email}">
						<li class="control-group">
							<span id="email-label" class="property-label"><g:message code="pessoa.email.label" default="Email" /></span>
							
								<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${pessoaInstance}" field="email"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.sexo}">
						<li class="control-group">
							<span id="sexo-label" class="property-label"><g:message code="pessoa.sexo.label" default="Sexo" /></span>
							
								<span class="property-value" aria-labelledby="sexo-label"><g:fieldValue bean="${pessoaInstance}" field="sexo"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.dataNascimento}">
						<li class="control-group">
							<span id="dataNascimento-label" class="property-label"><g:message code="pessoa.dataNascimento.label" default="Data Nascimento" /></span>
							
								<span class="property-value" aria-labelledby="dataNascimento-label"><g:formatDate date="${pessoaInstance?.dataNascimento}" /></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.grauInstrucao}">
						<li class="control-group">
							<span id="grauInstrucao-label" class="property-label"><g:message code="pessoa.grauInstrucao.label" default="Grau Instrucao" /></span>
							
								<span class="property-value" aria-labelledby="grauInstrucao-label"><g:fieldValue bean="${pessoaInstance}" field="grauInstrucao"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.logradouro}">
						<li class="control-group">
							<span id="logradouro-label" class="property-label"><g:message code="pessoa.logradouro.label" default="Logradouro" /></span>
							
								<span class="property-value" aria-labelledby="logradouro-label"><g:fieldValue bean="${pessoaInstance}" field="logradouro"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.bairro}">
						<li class="control-group">
							<span id="bairro-label" class="property-label"><g:message code="pessoa.bairro.label" default="Bairro" /></span>
							
								<span class="property-value" aria-labelledby="bairro-label"><g:fieldValue bean="${pessoaInstance}" field="bairro"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.cidade}">
						<li class="control-group">
							<span id="cidade-label" class="property-label"><g:message code="pessoa.cidade.label" default="Cidade" /></span>
							
								<span class="property-value" aria-labelledby="cidade-label">${pessoaInstance?.cidade?.encodeAsHTML()}</span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.telefoneContato}">
						<li class="control-group">
							<span id="telefoneContato-label" class="property-label"><g:message code="pessoa.telefoneContato.label" default="Telefone Contato" /></span>
							
								<span class="property-value" aria-labelledby="telefoneContato-label"><g:fieldValue bean="${pessoaInstance}" field="telefoneContato"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.cep}">
						<li class="control-group">
							<span id="cep-label" class="property-label"><g:message code="pessoa.cep.label" default="Cep" /></span>
							
								<span class="property-value" aria-labelledby="cep-label"><g:fieldValue bean="${pessoaInstance}" field="cep"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.dateCreated}">
						<li class="control-group">
							<span id="dateCreated-label" class="property-label"><g:message code="pessoa.dateCreated.label" default="Date Created" /></span>
							
								<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${pessoaInstance?.dateCreated}" /></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.lastUpdated}">
						<li class="control-group">
							<span id="lastUpdated-label" class="property-label"><g:message code="pessoa.lastUpdated.label" default="Last Updated" /></span>
							
								<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${pessoaInstance?.lastUpdated}" /></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.numero}">
						<li class="control-group">
							<span id="numero-label" class="property-label"><g:message code="pessoa.numero.label" default="Numero" /></span>
							
								<span class="property-value" aria-labelledby="numero-label"><g:fieldValue bean="${pessoaInstance}" field="numero"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${pessoaInstance?.telefoneCelular}">
						<li class="control-group">
							<span id="telefoneCelular-label" class="property-label"><g:message code="pessoa.telefoneCelular.label" default="Telefone Celular" /></span>
							
								<span class="property-value" aria-labelledby="telefoneCelular-label"><g:fieldValue bean="${pessoaInstance}" field="telefoneCelular"/></span>
							
						</li>
						</g:if>
						
						<g:if test="${profissionalInstance?.clinica}">
						<li class="control-group">
							<span id="clinica-label" class="property-label"><g:message code="profissional.clinica.label" default="Clinica" /></span>
							
								<span class="property-value" aria-labelledby="clinica-label">${profissionalInstance?.clinica?.encodeAsHTML()}</span>
							
						</li>
						</g:if>
						
						<g:if test="${profissionalInstance?.semestre}">
						<li class="control-group">
							<span id="semestre-label" class="property-label"><g:message code="profissional.semestre.label" default="Semestre" /></span>
							
								<span class="property-value" aria-labelledby="semestre-label">${profissionalInstance?.semestre}</span>
							
						</li>
						</g:if>
					
					</ol>
					<g:form class="form">
						<div class="form-actions">
							<g:hiddenField name="id" value="${profissionalInstance?.id}" />
							<g:link class="btn btn-primary" action="edit" id="${profissionalInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
							<g:actionSubmit class="btn" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</div>
					</g:form>
				</div>
			</div>
		</div>
	</body>
</html>
