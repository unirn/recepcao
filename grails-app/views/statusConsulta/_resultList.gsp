<%@ page import="br.edu.unirn.consulta.StatusConsulta"%>

	<p class="help-block" id="qntdRegistro">Quantidade - ${statusConsultaInstanceTotal}</p>
            
            <table class="table table-hover table-condensed table-bordered">
            <thead>
                <tr>
                    <th style="width:19px;">
						<span id="popoverAlert"></span>
                        <input type="checkbox" id="checkTodos" value="">
                    </th>
	                <g:if test="${actionName == 'buscaSimples'}">
	                    <g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="descricao" title="${message(code: 'statusConsulta.descricao.label', default: 'Descricao')}" />
						<th><g:message code="statusConsulta.clinica.label" default="Clinica" /></th>
						<g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="corListagem" title="${message(code: 'statusConsulta.corListagem.label', default: 'Cor Listagem')}" />
						<g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="tipoStatusConsulta" title="${message(code: 'statusConsulta.tipoStatusConsulta.label', default: 'Tipo Status Consulta')}" />
						
	                </g:if>
	                <g:else>
	                	<g:sortableColumn property="descricao" title="${message(code: 'statusConsulta.descricao.label', default: 'Descricao')}" />
						<th><g:message code="statusConsulta.clinica.label" default="Clinica" /></th>
						<g:sortableColumn property="corListagem" title="${message(code: 'statusConsulta.corListagem.label', default: 'Cor Listagem')}" />
						<g:sortableColumn  property="tipoStatusConsulta" title="${message(code: 'statusConsulta.tipoStatusConsulta.label', default: 'Tipo Status Consulta')}" />
	    	        </g:else>
					<th style="width: 10%;">Editar</th>
                </tr>
            </thead>
            <tbody>
                <g:each in="${statusConsultaInstanceList}" var="statusConsulta">
	                
	                <tr id="${statusConsulta?.id}">
	                	<td>
	                        <input type="checkbox" class="checkSimples" data-registroId="${statusConsulta?.id}" data-campo="${controllerName}ID" value="">
	                    </td>
	                    <td>${fieldValue(bean: statusConsulta, field: "descricao")}</td>
						<td>${fieldValue(bean: statusConsulta, field: "clinica")}</td>
						<td>${fieldValue(bean: statusConsulta, field: "corListagem")}</td>
						<td>${fieldValue(bean: statusConsulta, field: "tipoStatusConsulta")}</td>
	                    <td style="text-align: center;"><g:link class="btn btn-xs btn-default" action="edit" id="${statusConsulta?.id}">Editar</g:link></td>
	                </tr>
	                
	           </g:each>
            </tbody>
        </table>
        <div class="row pagination-table">
	        <div class="pagination pagination-centered col-lg-12">
                <g:if test="${actionName == 'buscaSimples' }">
                    <g:paginate params="${[nomeBuscaSimples: nomeBuscaSimples]}" total="${statusConsultaInstanceTotal}" />
                </g:if>
                <g:else>
                	<g:paginate total="${statusConsultaInstanceTotal}" />
    	        </g:else>
			</div>
		</div>