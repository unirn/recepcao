<%@ page import="br.edu.unirn.consulta.StatusConsulta" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'statusConsulta.label', default: 'StatusConsulta')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row">
<div class="col-lg-12 col-md-12">
    
    <ol class="breadcrumb">
      <li><a href="${createLink(uri: '/')}">Vis&atilde;o Geral</a></li>
      <li><g:link action="list">Status Consulta</g:link></li>
      <li class="active">Cadastrar</li>        
    </ol>

</div><!-- /.col-md-12 -->
</div>
<div class="row">
<div class="col-lg-12 col-md-12">
    
    <div class="panel panel-default">
	
        <div class="panel-heading">
           Novo Status Consulta
        </div><!-- /.panel-heading -->
        
        <div class="panel-body">
        <g:if test="${flash.message}">
			<div class="alert alert-dismissable alert-danger">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  ${flash.message}	
			</div>
		</g:if>
        <g:form action="save" class="form-horizontal formValidate" role="form">
        	<fieldset class="groupFields"> 
						
				<g:render template="form" />
        		
        		<div class="btn-group">
        			   <g:link action="list" class="btn btn-default pull-right"><span class="icon-arrow-left"></span> Voltar</g:link>
                </div> 
                <div class="btn-group">
                        <button type="submit" class="btn btn-success pull-right">Salvar <span class="icon-ok"></span></button>
                </div>  
                
                
            </fieldset>
        </g:form>
            
        </div><!-- /.panel-body -->
        

    </div><!-- /.panel -->

</div><!-- /.col-md-12 -->
</div>
<g:javascript>
	configuracoesGerais.ativarMenu("configuracoesMenu");
</g:javascript>
	</body>
</html>
