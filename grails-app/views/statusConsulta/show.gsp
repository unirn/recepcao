
<%@ page import="br.edu.unirn.consulta.StatusConsulta" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'statusConsulta.label', default: 'StatusConsulta')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="show-statusConsulta" class="contentCrud" role="main">
			<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
					<span><g:message code="default.show.label" args="[entityName]" /></span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
						<div class="alert" role="status">
							 <button type="button" class="close" data-dismiss="alert">&times;</button>
							 <strong>${flash.message}</strong>
						</div>
					</g:if>			
					<ol class="property-list statusConsulta">
					
						<g:if test="${statusConsultaInstance?.descricao}">
						<li class="fieldcontain">
							<span id="descricao-label" class="property-label"><g:message code="statusConsulta.descricao.label" default="Descricao" /></span>
							
								<span class="property-value" aria-labelledby="descricao-label"><g:fieldValue bean="${statusConsultaInstance}" field="descricao"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${statusConsultaInstance?.clinica}">
						<li class="fieldcontain">
							<span id="clinica-label" class="property-label"><g:message code="statusConsulta.clinica.label" default="Clinica" /></span>
							
								<span class="property-value" aria-labelledby="clinica-label"><g:link controller="clinica" action="show" id="${statusConsultaInstance?.clinica?.id}">${statusConsultaInstance?.clinica?.encodeAsHTML()}</g:link></span>
							
						</li>
						</g:if>
					
						<g:if test="${statusConsultaInstance?.corListagem}">
						<li class="fieldcontain">
							<span id="corListagem-label" class="property-label"><g:message code="statusConsulta.corListagem.label" default="Cor Listagem" /></span>
							
								<span class="property-value" aria-labelledby="corListagem-label"><g:fieldValue bean="${statusConsultaInstance}" field="corListagem"/></span>
							
						</li>
						</g:if>
					
						<g:if test="${statusConsultaInstance?.tipoStatusConsulta}">
						<li class="fieldcontain">
							<span id="tipoStatusConsulta-label" class="property-label"><g:message code="statusConsulta.tipoStatusConsulta.label" default="Tipo Status Consulta" /></span>
							
								<span class="property-value" aria-labelledby="tipoStatusConsulta-label"><g:fieldValue bean="${statusConsultaInstance}" field="tipoStatusConsulta"/></span>
							
						</li>
						</g:if>
					
					</ol>
					<g:form class="form">
						<div class="form-actions">
							<g:hiddenField name="id" value="${statusConsultaInstance?.id}" />
							<g:link class="btn btn-primary" action="edit" id="${statusConsultaInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
							<g:actionSubmit class="btn" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</div>
					</g:form>
				</div>
			</div>
		</div>
	</body>
</html>
