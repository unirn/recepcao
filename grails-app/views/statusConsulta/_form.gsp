<%@ page import="br.edu.unirn.consulta.StatusConsulta" %>

<div class="form-group">
    <label class="col-lg-2 col-md-2 control-label" for="descricao">Descri&ccedil;&atilde;o</label>

    <div class="col-lg-3 col-md-3">
    	<g:textField name="descricao" value="${statusConsultaInstance?.descricao}" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm" placeholder="Nome..." />
    </div>
</div> 

<div class="form-group">
    <label class="col-lg-2 col-md-2 control-label" for="clinica.id">Clinica</label>

    <div class="col-lg-3 col-md-3">
    	<g:select id="clinica" name="clinica.id" from="${br.edu.unirn.comum.Clinica.list()}" optionKey="id" value="${statusConsultaInstance?.clinica?.id}" class="form-control input-sm" noSelection="['null': '-- Escolha --']"/>
    </div>
</div> 

<div class="form-group">
    <label class="col-lg-2 col-md-2 control-label" for="corListagem">Cor</label>

    <div class="col-lg-2 col-md-2">
    	<g:textField name="corListagem" value="${statusConsultaInstance?.corListagem}" class="form-control input-sm" placeholder="Cor..."/>
    </div>
    
    <label class="col-lg-2 col-md-2 control-label" for="tipoStatusConsulta">Tipo de Status</label>

    <div class="col-lg-3 col-md-3">
    	<g:select name="tipoStatusConsulta" from="${br.edu.unirn.tipos.TipoStatusConsulta?.values()}" keys="${br.edu.unirn.tipos.TipoStatusConsulta.values()*.name()}"
    	 value="${statusConsultaInstance?.tipoStatusConsulta?.name()}" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm"/>
    </div>
</div> 


