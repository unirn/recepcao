<!doctype html>
<html>
<head>
<meta name="layout" content="main">
<title>Agenda Consulta</title>
</head>
<body>
<div class="col-md-12">
                    
    <ol class="breadcrumb">
	   	<li><g:link action="list" controller="consulta">Consultas</g:link></li> 
	   	<li class="active">Hist&oacute;rico Consulta</li>  
    </ol>

</div><!-- /.col-md-12 -->

<div class="col-md-12">
    
    <div class="panel panel-default">

        <div class="panel-heading">
            <strong>PACIENTE: </strong><span>${pacienteInstance?.pessoa?.nome}</span>
        </div><!-- /.panel-heading -->
        
        <div class="panel-body">
					<h4>Histórico de Alteração de Consulta</h4>
					<table class="table table-hover table-bordered">
						<thead>
							<tr>
								<th>Status Anterior</th>
								<th>Status Atual</th>
								<th>Data Criação</th>
								<th>Motivo</th>
								<th>Clínica</th>
								<th>Dados Consulta</th>
							</tr>
						</thead>
						<tbody>
							<g:each var="alteracaoConsulta" in="${listaAlteracaoConsulta}">
								<tr>
									<td style="background-color: ${alteracaoConsulta.statusConsultaAnterior?.corListagem}" class="td-status">${alteracaoConsulta.statusConsultaAnterior}</td>
									<td style="background-color: ${alteracaoConsulta.statusConsultaAtual?.corListagem}" class="td-status">${alteracaoConsulta.statusConsultaAtual}</td>
									<td>${alteracaoConsulta.dateCreated}</td>
									<td>${alteracaoConsulta.motivo}</td>
									<td>${alteracaoConsulta.consulta.sala.clinica}</td>
									<td><g:link action="show" id="${alteracaoConsulta.consulta.id}" controller="consulta"><g:img file="skin/database_edit.png"/></g:link></td>
								</tr>
							</g:each>
						</tbody>
					</table>
					<h4>Histórico das Últimas Consultas</h4>
					<table class="table table-hover table-bordered">
						<thead>
							<tr>
								<th>Data Consulta</th>
								<th>Horário da Consulta</th>
								<th>Clínica/Sala</th>
								<th>Marcado por</th>
								<th>Turno</th>
								<th>Valor</th>
								<th>Status Pag.</th>
								<th>Dados Consulta</th>
							</tr>
						</thead>
						<tbody>
							<g:each var="consulta" in="${listaConsulta}">
								<tr>
									<td><g:formatDate value="${consulta.dataConsulta}" format="dd/MM/yyyy"/></td>
									<td>${consulta.horarioConsulta}</td>
									<td>${consulta.sala.clinica}/${consulta.sala}</td>
									<td>${consulta.usuarioMarcacao}</td>
									<td>${consulta.turno}</td>
									<td>${consulta.valor}</td>
									<td style="background-color: ${consulta?.statusPagamento?.corListagem}" class="td-status"><a href="#" onclick="alterarPagamentoConsulta(${consulta.id})">${consulta.statusPagamento}</a></td>
									<td><g:link action="show" id="${consulta.id}" controller="consulta"><g:img file="skin/database_edit.png"/></g:link></td>
								</tr>
							</g:each>
						</tbody>
					</table>
				</div>
			</div>
	</div>
</body>
</html>
