<div id="dialogAlteraConsulta" title="Alteração de Status da Consulta">
	<g:formRemote method="post" name="frmAlteraStatus"
		url="[controller:'alteracaoStatusConsulta',action:'save']"
		onComplete="resetForm('#frmAlteraStatus');fecharAlterarStatusConsulta('#dialogAlteraConsulta')">
		<g:hiddenField name="consulta.id" id="consulta"/>
		<g:hiddenField name="statusConsultaAnterior.id" id="statusConsultaAnterior"/>
		<div class="control-group">
			<label class="control-label" for="statusConsulta">
				Novo Consulta
			</label>
			<g:select id="statusConsultaAtual" name="statusConsultaAtual.id" from="${br.edu.unirn.consulta.StatusConsulta.list()}" optionKey="id"/>
		</div>
		<div class="control-group">
			<label class="control-label" for="motivo"> Motivo </label>
			<g:textArea name="motivo" rows="6" style="width: 100%;"/>
		</div>
		
		<div style="text-align: center"  class="control-group">
			<g:actionSubmit class="btn" value="Realizar Alteração"/>
		</div>
	</g:formRemote>
</div>

