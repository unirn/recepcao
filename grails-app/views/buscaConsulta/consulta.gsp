<!doctype html>
<html>
<head>
<meta name="layout" content="main">
<title>Agenda Consulta</title>
</head>
<body>
	<div id="agenda-paciente" class="contentCrud" role="main">
		<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
						<span>Agenda das Clínicas</span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
						<div class="alert" role="status">
							 <button type="button" class="close" data-dismiss="alert">&times;</button>
							 <strong>${flash.message}</strong>
						</div>
					</g:if>
					<g:form action="consulta" class="form-horizontal">
						<fieldset >
							<div
								class="control-group required">
								<label class="control-label" for="dataConsulta"> Data<span
									class="required-indicator">*</span>
								</label>
								<div class="controls">
									<g:jQueryDatePicker id="dataConsulta" name="dataConsulta" value="${consultaInstance?.dataConsulta}" /> a
									<g:jQueryDatePicker id="dataFim" name="dataFim" value="${dataFim}" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="turno">
									Turno
								</label>
								<div class="controls">
									<g:select id="turno" name="turno" from="${br.edu.unirn.tipos.Turno?.values()}" keys="${br.edu.unirn.tipos.Turno.values()*.name()}" value="${turnoInstance?.name()}" noSelection="${[null:'--']}"/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="clinica">
									<g:message code="consulta.clinica.label" default="Clínica" />
								</label>
								<div class="controls">
									<g:select id="clinicaID" name="clinicaID" from="${br.edu.unirn.comum.Clinica.list()}" optionKey="id" required="" value="${clinicaInstance?.id}" noSelection="${[0:'Todas']}"/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="statusConsulta">
									Status Consulta
								</label>
								<div class="controls">
									<g:select id="statusConsulta" name="statusConsulta.id" from="${br.edu.unirn.consulta.StatusConsulta.list()}" optionKey="id" value="${consultaInstance?.statusConsulta?.id}" noSelection="${[0:'Todas']}"/>
								</div>
							</div>
							<div class="form-actions">
								<g:submitButton name="consulta" class="btn btn-primary" value="Consultar" />
							</div>
						</fieldset>
						
					</g:form>
					<g:if test="${listaConsulta}">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Protuário</th>
									<th>Nome</th>
									<th>Tel.Contato</th>
									<th>Data</th>
									<th>Sala</th>
									<th>Horário</th>
									<th>Vis.</th>
									<th>Status Pg.</th>
									<th>Reag.</th>
									<th>Hist.</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<g:each var="consulta" in="${listaConsulta}">
									<tr id="lConsulta${consulta.id}">
										<td>${consulta.id}</td>
										<td>${consulta.paciente.pessoa.nome}</td>
										<td>${consulta.paciente.pessoa.telefoneContato}<g:if test="${consulta.paciente.pessoa.telefoneCelular}">/${consulta.paciente.pessoa.telefoneCelular}</g:if> </td>
										<td><g:formatDate date="${consulta.dataConsulta}" format="dd/MM/yyyy"/> </td>
										<td>${consulta.sala.nome} (${consulta.sala.clinica.descricao})</td>
										<td>${consulta.horarioConsulta}</td>
										<td><g:link action="show" id="${consulta.id}" controller="consulta"><g:img file="skin/database_edit.png"/></g:link></td>
										<td style="background-color: ${consulta?.statusPagamento?.corListagem}"><a href="#" onclick="alterarPagamentoConsulta(${consulta.id})">${consulta.statusPagamento}</a></td>
										<td><g:link action="reagendamento" id="${consulta.id}"><g:img file="skin/next.png" onclick="return confirm('Deseja realmente reagendar a consulta!?');"/></g:link></td>
										<td><g:link action="historicoAlteracaoConsulta" id="${consulta.paciente.id}" controller="alteracaoStatusConsulta"><g:img file="skin/database_edit.png"/></g:link></td>
										<td style="background-color: ${consulta?.statusConsulta?.corListagem}"><a href="#" onclick="alterarStatusConsulta(${consulta.statusConsulta.id},${consulta.id})">${consulta.statusConsulta}</a></td>
									</tr>
								</g:each>
							</tbody>
						</table>
					</g:if>
				</div>
				<g:render template="alteraStatusConsulta"/>
			</div>
		</div>
</body>
</html>
