<%@ page import="br.edu.unirn.comum.Cidade" %>


<div class="form-group">
    <label class="col-lg-2 col-md-2 control-label" for=estado.id">Estado</label>

    <div class="col-lg-1 col-md-1">
    	<g:select id="estado" name="estado.id" from="${br.edu.unirn.comum.Estado.list()}" optionKey="id" required="" value="${cidadeInstance?.estado?.id}" class="form-control input-sm" />
    </div>
    
    <label class="col-lg-1 col-md-1 control-label" for="nome">Nome</label>

    <div class="col-lg-4 col-md-4">
    	<g:textField name="nome" value="${cidadeInstance?.nome}" data-validate="{required: true, messages:{required:'Campo obrigatorio'}}" class="form-control input-sm" placeholder="Cidade..."/>
    </div>
    
</div> 
