
<%@ page import="br.edu.unirn.comum.Cidade" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cidade.label', default: 'Cidade')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="show-cidade" class="contentCrud" role="main">
			<div class="box corner-all">
				<div class="box-header grd-white color-black corner-top">
					<span><g:message code="default.show.label" args="[entityName]" /></span>
				</div>
				<div class="box-body">
					<g:if test="${flash.message}">
						<div class="alert" role="status">
							 <button type="button" class="close" data-dismiss="alert">&times;</button>
							 <strong>${flash.message}</strong>
						</div>
					</g:if>			
					<ol class="property-list cidade">
					
						<g:if test="${cidadeInstance?.estado}">
						<li class="fieldcontain">
							<span id="estado-label" class="property-label"><g:message code="cidade.estado.label" default="Estado" /></span>
							
								<span class="property-value" aria-labelledby="estado-label"><g:link controller="estado" action="show" id="${cidadeInstance?.estado?.id}">${cidadeInstance?.estado?.encodeAsHTML()}</g:link></span>
							
						</li>
						</g:if>
					
						<g:if test="${cidadeInstance?.nome}">
						<li class="fieldcontain">
							<span id="nome-label" class="property-label"><g:message code="cidade.nome.label" default="Nome" /></span>
							
								<span class="property-value" aria-labelledby="nome-label"><g:fieldValue bean="${cidadeInstance}" field="nome"/></span>
							
						</li>
						</g:if>
					
					</ol>
					<g:form class="form">
						<div class="form-actions">
							<g:hiddenField name="id" value="${cidadeInstance?.id}" />
							<g:link class="btn btn-primary" action="edit" id="${cidadeInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
							<g:actionSubmit class="btn" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</div>
					</g:form>
				</div>
			</div>
		</div>
	</body>
</html>
