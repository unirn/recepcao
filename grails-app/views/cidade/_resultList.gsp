<%@ page import="br.edu.unirn.comum.Cidade"%>

	<p class="help-block" id="qntdRegistro">Quantidade - ${cidadeInstanceTotal}</p>
            
            <table class="table table-hover table-condensed table-bordered">
            <thead>
                <tr>
                    <th style="width:19px;">
						<span id="popoverAlert"></span>
                        <input type="checkbox" id="checkTodos" value="">
                    </th>
	                <g:if test="${actionName == 'buscaSimples'}">
	                    <th><g:message code="cidade.estado.label" default="Estado" /></th>
						<g:sortableColumn params="${[nomeBuscaSimples: nomeBuscaSimples]}" property="nome" title="${message(code: 'cidade.nome.label', default: 'Nome')}" />
								
	                </g:if>
	                <g:else>
	                	<th><g:message code="cidade.estado.label" default="Estado" /></th>
						<g:sortableColumn property="nome" title="${message(code: 'cidade.nome.label', default: 'Nome')}" />
	    	        </g:else>
					<th style="width: 10%;">Editar</th>
                </tr>
            </thead>
            <tbody>
                <g:each in="${cidadeInstanceList}" var="cidadeInstance">
	                
	                <tr id="${cidadeInstance?.id}">
	                    <td>
	                        <input type="checkbox" class="checkSimples" data-registroId="${cidadeInstance?.id}" data-campo="${controllerName}ID" value="">
	                    </td>
	                    <td>${fieldValue(bean: cidadeInstance, field: "estado")}</td>
						<td>${fieldValue(bean: cidadeInstance, field: "nome")}</td>
	                    <td style="text-align: center;"><g:link class="btn btn-xs btn-default" action="edit" id="${cidadeInstance?.id}">Editar</g:link></td>
	                </tr>
	                
	           </g:each>
            </tbody>
        </table>
        <div class="row pagination-table">
	        <div class="pagination pagination-centered col-lg-12">
                <g:if test="${actionName == 'buscaSimples' }">
                    <g:paginate params="${[nomeBuscaSimples: nomeBuscaSimples]}" total="${cidadeInstanceTotal}" />
                </g:if>
                <g:else>
                	<g:paginate total="${cidadeInstanceTotal}" />
    	        </g:else>
			</div>
		</div>