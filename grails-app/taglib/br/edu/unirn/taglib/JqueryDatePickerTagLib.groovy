package br.edu.unirn.taglib

import java.text.ParseException;
import java.text.SimpleDateFormat

class JqueryDatePickerTagLib {
		def jQueryDatePicker= {attrs, body->
	
			def out = out
			def name = attrs.name
			def format = attrs.format ?: "dd/mm/yy"
			def formatInput = attrs.formatInput ?: "yyyy-MM-dd HH:mm:ss.S"
			def formatJava = attrs.formatJava ?: "dd/MM/yyyy"
			def value = attrs.value
			def placeholder = attrs.placeholder
			def classField = attrs.classField
			def oncomplete = attrs.oncomplete
			def required = attrs.required?.empty ? true: false
			def id = attrs.id ?: name
			def style = attrs.style		
			
			if(value){
				try{
					value = new SimpleDateFormat(formatInput).parse((String)value)
				}catch (ParseException e) {
					
				}			
			}
			
			def dateString = value ? value.format(formatJava) : ""
			def dayString = value ? value.format('dd') : ""
			def monthString = value ? value.format('MM') : ""
			def yearString = value ? value.format('yyyy') : ""
			
			if (required)
				out.println  "<input type=\"text\" name=\"${name}\" id=\"${id}\" value=\"${dateString}\" required=\"\" style=\"${style}\" data-validate=\"{required: true, messages:{required:'Campo obrigatorio'}}\" class=\"${classField}\" placeholder=\"${placeholder?:'' }\"/>"
			else
				out.println  "<input type=\"text\" name=\"${name}\" id=\"${id}\" value=\"${dateString}\" class=\"${classField}\" style=\"${style}\" placeholder=\"${placeholder?:''}\"/>"
				
			out.println  "<input type=\"hidden\" name=\"${name}_day\" id=\"${id}_day\"/>"
			out.println  "<input type=\"hidden\" name=\"${name}_month\" id=\"${id}_month\"/>"
			out.println  "<input type=\"hidden\" name=\"${name}_year\" id=\"${id}_year\"/>"
			
			out.println  "<script type=\"text/javascript\"> jQuery(document).ready(function() {"
			out.println  "jQuery(\"#${name}\").datepicker({"
			out.println	 "dayNamesMin:['dom','seg','ter','qua','qui','sex','s\\xE1b'],"
			out.println  "monthNames:['Janeiro','Fevereiro','Mar\\xE7o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],"
			out.println  "dateFormat: \"${format}\","
			out.println  "setDate: \"${dateString}\","
			out.println  "onClose: function(dateText,inst) {"
			out.println  "dataObtida = null;"
			out.println  "if(jQuery(\"#${name}\").val() != \"__/__/____\"){"
			out.println  "dataObtida = jQuery.datepicker.parseDate('${format}', dateText);}"
			out.println  "if(dataObtida!=null){"
			out.println  "jQuery(\"#${name}_month\").attr(\"value\",dataObtida.getMonth()+1);"
			out.println  "jQuery(\"#${name}_day\").attr(\"value\",dataObtida.getDate());"
			out.println  "jQuery(\"#${name}_year\").attr(\"value\",dataObtida.getFullYear());"
			out.println  "${oncomplete}"
			out.println  "}"
			out.println  "}"
			out.println  "});"
			if(value){
				out.println  "jQuery(\"#${name}_month\").attr(\"value\",${monthString});"
				out.println  "jQuery(\"#${name}_day\").attr(\"value\",${dayString});"
				out.println  "jQuery(\"#${name}_year\").attr(\"value\",${yearString});"
			}
			out.println  "jQuery(\"#${name}\").focusout(function() {"
			out.println  "dataObtida = null;"
			out.println  "if(jQuery(\"#${name}\").val() != \"__/__/____\"){"
			out.println  "dataObtida = jQuery.datepicker.parseDate('${format}', jQuery(\"#${name}\").val());}"
			out.println  "if(dataObtida!=null){"
			out.println  "jQuery(\"#${name}_month\").attr(\"value\",dataObtida.getMonth()+1);"
			out.println  "jQuery(\"#${name}_day\").attr(\"value\",dataObtida.getDate());"
			out.println  "jQuery(\"#${name}_year\").attr(\"value\",dataObtida.getFullYear());"
			out.println  "}});"
			out.println  "})</script>"
		}
}
