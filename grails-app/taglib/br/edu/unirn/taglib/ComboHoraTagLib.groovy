package br.edu.unirn.taglib

import br.edu.unirn.comum.Clinica
import br.edu.unirn.consulta.Agenda

class ComboHoraTagLib {

	def horarioService
	def agendaService
	
	def comboHora = {attrs, body->
		
			def out = out
			def horaConsulta = attrs.horaConsulta
			def name = attrs.name
			def clinicaID = attrs.clinicaID
			def turnoIN = attrs.turno
			def pacienteID = attrs.pacienteID
			def dataConsulta = attrs.dataConsulta
			
			def agenda = agendaService.buscarAgenda(turnoIN,clinicaID)
			
			if(agenda){
				def lista = horarioService.geracaoListaHorario(agenda.duracao,agenda.horaInicio,agenda.horaFim)
				
				out.println "<select name='${name}' id='${name}' class=\"form-control input-sm\">"
				lista.each {
					def nome
					def disable = ""
					if(dataConsulta){
						println 'Aqui!'
						nome = horarioService.verificarDisponibilidadeClinica(agenda.clinica, it, dataConsulta,pacienteID,turnoIN)
					}
					if(nome)
						disable = "disabled='disabled'"
					
					def selected = horaConsulta?.equals(it)?"selected='selected'":""
					if(nome)
						out.println "<option value='${it}' ${selected} ${disable}>${it} - ${nome}</option>"
					else
						out.println "<option value='${it}' ${selected} ${disable}>${it}</option>"
				}
				out.println "</select>"
		}else{
			out.println "<select disabled='disabled' name='${name}' id='${name}' class=\"form-control input-sm\">"
			out.println "<option>Defina Clinica/Data/Turno</option>"
			out.println "</select>"
		}
			
	}
}
