package br.edu.unirn.taglib

import br.edu.unirn.comum.Pessoa

class JqueryAutoCompleteTagLib {

	def pacienteService
	def pessoaService
	
	def JQueryAutoCompletePaciente = {attrs, body->
		def out = out
		def value = attrs.value
		def onselect = attrs.onselect
		def required = attrs.required?.empty ? true: false

		if (required)
			out.println  "<input type=\"text\" name=\"pacienteAutoComplete\" id=\"pacienteAutoComplete\" data-validate=\"{required: true, messages:{required:'Campo obrigatorio'}}\" required=\"\" maxlenght=\"200\" class=\"form-control input-sm\"/>"
		else
			out.println  "<input type=\"text\" name=\"pacienteAutoComplete\" id=\"pacienteAutoComplete\" maxlenght=\"200\" class=\"form-control input-sm\"/>"
			
		out.println  "<input type=\"hidden\" name=\"paciente.id\" id=\"paciente\"/>"
		
		out.println  "<script type=\"text/javascript\"> \$(document).ready(function() {"
		out.println  "\$(\"#pacienteAutoComplete\").autocomplete({"
		out.println  "source: function(request, response){"
		out.println  "\$.ajax({"
		out.println  "url: \"/recepcao/paciente/autoCompletePaciente\","
		out.println  "data: request,"
		out.println  "success: function(data){"
		out.println  "response(data);"
		out.println  "}"
		out.println  "});"
		out.println  "},"
		out.println  "minLength: 2,"
		out.println  "select: function(event, ui) { "
		out.println  "var id = ui.item.id;"
		out.println  "\$(\"#paciente\").val(id);"
		if(onselect)
			out.println  "${onselect};"
		out.println  "}"
		out.println  "});"
		if(value){
			def paciente = pacienteService.obterPaciente(value)
			out.println  "\$(\"#paciente\").val(${value});"
			out.println  "\$(\"#pacienteAutoComplete\").val(\"${paciente.pessoa.nome}\");"
		}
		
		out.println  "});</script>"
		
	}
	
	def jQueryAutoCompletePessoa= {attrs, body->
			def out = out
			def value = attrs.value
			def name = attrs.name==null ? "":attrs.name
			def onselect = attrs.onselect
			def required = attrs.required?.empty ? true: false
			
			if (required)
				out.println  "<input type=\"text\" name=\"pessoaAutoComplete\" id=\"pessoaAutoComplete\" required=\"\" value=\"${name}\" maxlenght=\"200\" class=\"form-control input-sm\"/>"
			else
				out.println  "<input type=\"text\" name=\"pessoaAutoComplete\" id=\"pessoaAutoComplete\" maxlenght=\"200\" value=\"${name}\" class=\"form-control input-sm\"/>"
				
			out.println  "<input type=\"hidden\" name=\"pessoa.id\" id=\"pessoa\" value=\"0\"/>"
			
			out.println  "<script type=\"text/javascript\"> \$(document).ready(function() {"
			out.println  "\$(\"#pessoaAutoComplete\").autocomplete({"
			out.println  "source: function(request, response){"
		    out.println  "\$.ajax({"
			out.println  "url: \"/recepcao/paciente/autoCompletePessoa\","
			out.println  "data: request,"
			out.println  "success: function(data){"
			out.println  "	if (data!=null){"
			out.println  "		response(data);"
			out.println  "	}else{"
			out.println  "   \$(\"#pessoa\").val(0);"
			out.println  "  }"
			out.println  "}"
			out.println  "});"
			out.println  "},"
			out.println  "minLength: 2," 
			out.println  "select: function(event, ui) { "
			out.println  "var id = ui.item.id;"
			out.println  "\$(\"#pessoa\").val(id);"
			if(onselect)
				out.println  "${onselect};"
			out.println  "}"
			out.println  "});"
			if(value){
				def pessoa = Pessoa.get(value)
				out.println  "\$(\"#pessoa\").val(${value});"
				out.println  "\$(\"#pessoaAutoComplete\").val(\"${pessoa.nome}\");"
			}
			out.println  "\$(\"#pessoaAutoComplete\").keyup(function() {"
            out.println  "	if(\$(\"#pessoaAutoComplete\").val().length<2){"
			out.println  "		\$(\"#pessoa\").val(0);"
			out.println  "	}"
            out.println  "});"
			
			out.println  "});</script>"
		}
}
