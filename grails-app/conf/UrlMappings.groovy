class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(controller:"index")
		
		"500"(view:'/error')
		
		//Login
		"/login/$action?" (controller: "login" )
		"/logout/$action?"(controller: "logout")
	}
}
