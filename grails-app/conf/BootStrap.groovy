import org.junit.internal.runners.statements.FailOnTimeout;

import grails.util.Environment;
import grails.util.GrailsUtil
import antlr.StringUtils;
import br.edu.unirn.comum.Cidade
import br.edu.unirn.comum.Clinica
import br.edu.unirn.comum.Estado
import br.edu.unirn.comum.Papel
import br.edu.unirn.comum.Sala
import br.edu.unirn.comum.VinculoInstituicao
import br.edu.unirn.consulta.Agenda
import br.edu.unirn.consulta.StatusConsulta
import br.edu.unirn.consulta.StatusPagamento
import br.edu.unirn.seguranca.Usuario;
import br.edu.unirn.tipos.TipoStatusConsulta
import br.edu.unirn.tipos.TipoUsuario;
import br.edu.unirn.tipos.Turno

class BootStrap {

	def init = { servletContext ->
		
		if(Environment.developmentMode){
			carregarEstado()
			carregarClinicas()
			carregarStatusConsulta()
			carregarUsuarioPadrao()
			carregarStatusPagamento()
			def senha = br.edu.unirn.utils.StringUtils.toMD5('admin')
			Usuario.findByLogin("admin")?:new Usuario(nome:'Administrador',email:'admin@admin.com',login:'Admin',senha:senha,tipoUsuario: TipoUsuario.ADMINISTRADOR).save(flush:true)
		}
		carregarPapel()
		carregarVinculoInstituicao()
		
	}
	def destroy = {
	}
	
	void carregarStatusPagamento(){
		StatusPagamento.findByNome("PAGO")?: new StatusPagamento(nome:"PAGO",corListagem:"green").save(failOnError: true)
		StatusPagamento.findByNome("PENDENTE")?: new StatusPagamento(nome:"PENDENTE",corListagem:"yellow").save(failOnError: true)
		StatusPagamento.findByNome("LIVRE")?: new StatusPagamento(nome:"LIVRE",corListagem:"blue").save(failOnError: true)
	}
	
	void carregarPapel(){
		if(!Papel.get(1)){
			 def p = new Papel(nome:"Aluno")
			 p.id = 1
			 p.save(flush:true,failOnError: true)
		}
		if(!Papel.get(2)){
			def p = new Papel(nome:"Professor")
			p.id = 2
			p.save(flush:true,failOnError: true)
	   }
		if(!Papel.get(3)){
			def p = new Papel(nome:"Coordenador")
			p.id = 3
			p.save(flush:true,failOnError: true)
	   }
	}

	void carregarUsuarioPadrao(){
		/**
		Permissao.findByAuthority("nenhuma")?: new Permissao(authority:"nenhuma").save(failOnError:true)
		Permissao.findByAuthority("recepcao")?: new Permissao(authority:"recepcao").save(failOnError:true)
		Permissao.findByAuthority("coordenacao_recepcao")?: new Permissao(authority:"coordenacao_recepcao").save(failOnError:true)
		Permissao.findByAuthority("coordenacao")?: new Permissao(authority:"coordenacao").save(failOnError:true)
		Permissao.findByAuthority("assistente_social")?: new Permissao(authority:"assistente_social").save(failOnError:true)
		Permissao.findByAuthority("professor")?: new Permissao(authority:"professor").save(failOnError:true)
		Permissao.findByAuthority("aluno")?: new Permissao(authority:"aluno").save(failOnError:true)
		def permisao = Permissao.findByAuthority("admin")?: new Permissao(authority:"admin").save(failOnError:true)
		def usuario = Usuario.findByUsername("admin")?: new Usuario(username:"admin",accountExpired: false, accountLocked: false, password: "admin" ,enabled: true,nome:"Administrador",email:"admin@unirn.edu.br").save(failOnError:true)
		UsuarioPermissao.get(usuario.id,permisao.id)?:UsuarioPermissao.create(usuario,permisao)
		*/
		 	
	}
	void carregarStatusConsulta(){
		if(!StatusConsulta.findByDescricao("A CONFIRMAR")){
			def s = new StatusConsulta(descricao:"A CONFIRMAR",corListagem:"yellow",tipoStatusConsulta:TipoStatusConsulta.CONFIRMACAO)
			s.id = 1
			s.save(flush:true,failOnError: true)
		}
		if(!StatusConsulta.findByDescricao("CONFIRMADO")){
			def s = new StatusConsulta(descricao:"CONFIRMADO",corListagem:"green",tipoStatusConsulta:TipoStatusConsulta.CONFIRMACAO)
			s.id = 2
			s.save(flush:true,failOnError: true)
		}
		if(!StatusConsulta.findByDescricao("REAGENDADA")){
			def s = new StatusConsulta(descricao:"REAGENDADA",corListagem:"blue",tipoStatusConsulta:TipoStatusConsulta.DESMARQUE)
			s.id = 3
			s.save(flush:true,failOnError: true)
		}
		if(!StatusConsulta.findByDescricao("DESMARCADO")){
			def s = new StatusConsulta(descricao:"DESMARCADO",corListagem:"red",tipoStatusConsulta:TipoStatusConsulta.DESMARQUE)
			s.id = 4
			s.save(flush:true,failOnError: true)
		}
		if(!StatusConsulta.findByDescricao("FALTA")){
			def s = new StatusConsulta(descricao:"FALTA",corListagem:"red",tipoStatusConsulta:TipoStatusConsulta.FALTA)
			s.id = 5
			s.save(flush:true,failOnError: true)
		}
	}
	
	void carregarClinicas(){
		def clinicaFisioterapia = Clinica.findByDescricao("Fisioterapia")?: new Clinica(descricao:"Fisioterapia",valor:10).save(failOnError: true)
		def clinicaPsicologia = Clinica.findByDescricao("Psicologia")?: new Clinica(descricao:"Psicologia",valor:10).save(failOnError: true)
		def clinicaOdontologia = Clinica.findByDescricao("Odontologia")?: new Clinica(descricao:"Odontologia",valor:10).save(failOnError: true)
		def clinicaNutricao = Clinica.findByDescricao("Nutricao")?: new Clinica(descricao:"Nutricao",valor:10).save(failOnError: true)
		def clinicaFarmacia = Clinica.findByDescricao("Farmacia")?: new Clinica(descricao:"Farmacia",valor:10).save(failOnError: true)
		def clinicaEdFisica = Clinica.findByDescricao("Ed. Fisica")?: new Clinica(descricao:"Ed. Fisica",valor:10).save(failOnError: true)
		def clinicaEnfermagem = Clinica.findByDescricao("Enfermagem")?: new Clinica(descricao:"Enfermagem",valor:10).save(failOnError: true)
		
		Sala.findByNome("Sala Fisioterapia")?:new Sala(nome:"Sala Fisioterapia",clinica: clinicaFisioterapia).save(failOnError: true)
		Sala.findByNome("Sala Psicologia")?:new Sala(nome:"Sala Psicologia",clinica: clinicaPsicologia).save(failOnError: true)
		Sala.findByNome("Sala Odontologia")?:new Sala(nome:"Sala Odontologia",clinica: clinicaOdontologia).save(failOnError: true)
		Sala.findByNome("Sala Nutricao")?:new Sala(nome:"Sala Nutricao",clinica: clinicaNutricao).save(failOnError: true)
		Sala.findByNome("Sala Farmacia")?:new Sala(nome:"Sala Farmacia",clinica: clinicaFarmacia).save(failOnError: true)
		Sala.findByNome("Sala Ed. Fisica")?:new Sala(nome:"Sala Ed. Fisica",clinica: clinicaEdFisica).save(failOnError: true)
		Sala.findByNome("Sala Enfermagem")?:new Sala(nome:"Sala Enfermagem",clinica: clinicaEnfermagem).save(failOnError: true)
		
		Agenda.findByClinica(clinicaFisioterapia)?: new Agenda(duracao:0.4,clinica:clinicaFisioterapia,horaInicio:"08:00",horaFim:"13:00",turno: Turno.MANHA).save(failOnError: true)
		Agenda.findByClinica(clinicaPsicologia)?: new Agenda(duracao:1,clinica:clinicaPsicologia,horaInicio:"08:00",horaFim:"13:00",turno: Turno.MANHA).save(failOnError: true)
		Agenda.findByClinica(clinicaOdontologia)?: new Agenda(duracao:0.4,clinica:clinicaOdontologia,horaInicio:"08:00",horaFim:"13:00",turno: Turno.MANHA).save(failOnError: true)
		Agenda.findByClinica(clinicaNutricao)?: new Agenda(duracao:1,clinica:clinicaNutricao,horaInicio:"08:00",horaFim:"13:00",turno: Turno.MANHA).save(failOnError: true)
		Agenda.findByClinica(clinicaFarmacia)?: new Agenda(duracao:0.4,clinica:clinicaFarmacia,horaInicio:"08:00",horaFim:"13:00",turno: Turno.MANHA).save(failOnError: true)
		Agenda.findByClinica(clinicaEdFisica)?: new Agenda(duracao:1,clinica:clinicaEdFisica,horaInicio:"08:00",horaFim:"13:00",turno: Turno.MANHA).save(failOnError: true)
		Agenda.findByClinica(clinicaEnfermagem)?: new Agenda(duracao:0.4,clinica:clinicaEnfermagem,horaInicio:"08:00",horaFim:"13:00",turno: Turno.MANHA).save(failOnError: true)
	}
	
	void carregarVinculoInstituicao(){
		if(!VinculoInstituicao.get(1)){
			def v = new VinculoInstituicao(nome:"Aluno")
			v.id = 1
			v.save(failOnError: true,flush:true)
		}
		if(!VinculoInstituicao.get(2)){
			def v = new VinculoInstituicao(nome:"Professor")
			v.id = 2
			v.save(failOnError: true,flush:true)
		}
		if(!VinculoInstituicao.get(3)){
			def v = new VinculoInstituicao(nome:"Outros")
			v.id = 3
			v.save(failOnError: true,flush:true)
		}
		if(!VinculoInstituicao.get(4)){
			def v = new VinculoInstituicao(nome:"Funcionario")
			v.id = 4
			v.save(failOnError: true,flush:true)
		}		
	}
	
	void carregarEstado(){

		Estado.findBySigla("AC")?:new Estado(sigla:"AC",nome:"Acre").save(failOnError: true)
		Estado.findBySigla("AL")?:new Estado(sigla:"AL",nome:"Alagoas").save(failOnError: true)
		Estado.findBySigla("AM")?:new Estado(sigla:"AM",nome:"Amazonas").save(failOnError: true)
		Estado.findBySigla("AP")?:new Estado(sigla:"AP",nome:"Amap�").save(failOnError: true)
		Estado.findBySigla("BA")?:new Estado(sigla:"BA",nome:"Bahia").save(failOnError: true)
		Estado.findBySigla("CE")?:new Estado(sigla:"CE",nome:"Cear�").save(failOnError: true)
		Estado.findBySigla("DF")?:new Estado(sigla:"DF",nome:"Distrito Federal").save(failOnError: true)
		Estado.findBySigla("ES")?:new Estado(sigla:"ES",nome:"Esp�rito Santo").save(failOnError: true)
		Estado.findBySigla("GO")?:new Estado(sigla:"GO",nome:"Goi�s").save(failOnError: true)
		Estado.findBySigla("MA")?:new Estado(sigla:"MA",nome:"Maranh�o").save(failOnError: true)
		Estado.findBySigla("MG")?:new Estado(sigla:"MG",nome:"Minas Gerais").save(failOnError: true)
		Estado.findBySigla("MS")?:new Estado(sigla:"MS",nome:"Mato Grosso do Sul").save(failOnError: true)
		Estado.findBySigla("MT")?:new Estado(sigla:"MT",nome:"Mato Grosso").save(failOnError: true)
		Estado.findBySigla("PA")?:new Estado(sigla:"PA",nome:"Par�").save(failOnError: true)
		Estado.findBySigla("PB")?:new Estado(sigla:"PB",nome:"Para�ba").save(failOnError: true)
		Estado.findBySigla("PE")?:new Estado(sigla:"PE",nome:"Pernambuco").save(failOnError: true)
		Estado.findBySigla("PI")?:new Estado(sigla:"PI",nome:"Piau�").save(failOnError: true)
		Estado.findBySigla("PR")?:new Estado(sigla:"PR",nome:"Paran�").save(failOnError: true)
		Estado.findBySigla("RJ")?:new Estado(sigla:"RJ",nome:"Rio de Janeiro").save(failOnError: true)

		def unidadeRN = Estado.findBySigla("RN")?:new Estado(sigla:"RN",nome:"Rio Grande do Norte").save(failOnError: true)

		Estado.findBySigla("RR")?:new Estado(sigla:"RR",nome:"Roraima").save(failOnError: true)
		Estado.findBySigla("RO")?:new Estado(sigla:"RO",nome:"Rond�nia").save(failOnError: true)
		Estado.findBySigla("RS")?:new Estado(sigla:"RS",nome:"Rio Grande do Sul").save(failOnError: true)
		Estado.findBySigla("SC")?:new Estado(sigla:"SC",nome:"Santa Catarina").save(failOnError: true)
		Estado.findBySigla("SE")?:new Estado(sigla:"SE",nome:"Sergipe").save(failOnError: true)
		Estado.findBySigla("SP")?:new Estado(sigla:"SP",nome:"S�o Paulo").save(failOnError: true)
		Estado.findBySigla("TO")?:new Estado(sigla:"TO",nome:"Tocantins").save(failOnError: true)

		Cidade.findByNome("Natal")?:new Cidade(nome:"Natal",estado:unidadeRN).save(failOnError: true)
		Cidade.findByNome("Mossor�")?:new Cidade(nome:"Mossor�",estado:unidadeRN).save(failOnError: true)
		Cidade.findByNome("Parnamirim")?:new Cidade(nome:"Parnamirim",estado:unidadeRN).save(failOnError: true)
		Cidade.findByNome("S�o Gon�alo")?:new Cidade(nome:"S�o Gon�alo",estado:unidadeRN).save(failOnError: true)

	}
}
