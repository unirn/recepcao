package br.edu.unirn.filter

class SegurancaFilters {

	def filters = {
		all(controller:'*', action:'*') {
			before = {
				if(!session.usuario && !controllerName.equals("login") && !controllerName.equals("service")){
					redirect(controller:"login",action:"auth")
					return false
				}
			}
			after = { Map model ->
			}
			afterView = { Exception e ->
			}
		}
	}
}
