dataSource {
    pooled = true
    driverClassName = "org.h2.Driver"
    username = "sa"
    password = ""
	loggingSql = true
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        dataSource {
			driverClassName = "org.postgresql.Driver"
			username = "clinicas"
			password = "cl1n1c@sUn1RN"
            dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
			//url = "jdbc:postgresql://desenvolvimento.unirn.edu.br:5432/clinicas_desenvolvimento"
			url = "jdbc:postgresql://127.0.0.1:5432/clinicas_new"
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
        }
    }
    production {
        dataSource {
			driverClassName = "org.postgresql.Driver"
			username = "clinicas"
			password = "cl1n1c@sUn1RN"
			loggingSql = false
            dbCreate = "update"
			url = "jdbc:postgresql://desenvolvimento.unirn.edu.br:5432/clinicas"
            pooled = true
            properties {
               maxActive = -1
               minEvictableIdleTimeMillis=1800000
               timeBetweenEvictionRunsMillis=1800000
               numTestsPerEvictionRun=3
               testOnBorrow=true
               testWhileIdle=true
               testOnReturn=true
               validationQuery="SELECT 1"
            }
        }
    }
}
