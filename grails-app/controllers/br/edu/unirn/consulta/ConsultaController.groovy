package br.edu.unirn.consulta

import java.text.SimpleDateFormat;

import br.edu.unirn.comum.Paciente
import br.edu.unirn.comum.Pessoa;
import br.edu.unirn.comum.Sala;
import br.edu.unirn.tipos.Turno;
import br.edu.unirn.utils.StringUtils;
import grails.converters.JSON;

import org.springframework.dao.DataIntegrityViolationException

class ConsultaController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
    def index() {
        redirect(action: "list", params: params)
    }
	
	def buscaSimples(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		params.offset = (params.offset ? params.offset.toInteger() : 0)
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
			
		def consultaCriteria = Consulta.createCriteria()
		def result = consultaCriteria.list(params){
			between("dataConsulta",sdf.parse(params.dataInicialBuscaSimples),sdf.parse(params.dataFimBuscaSimples))
		}
		render(view:"list",model:[consultaInstanceList: result, consultaInstanceTotal:result.totalCount,dataInicialBuscaSimples:params.dataInicialBuscaSimples,dataFimBuscaSimples:params.dataFimBuscaSimples])
	}
	def buscaAvancada(Integer max){
		params.max = Math.min(max ?: 10, 100)
		params.offset = (params.offset ? params.offset.toInteger() : 0)
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
		
		def consultaCriteria = Consulta.createCriteria()
		def result = consultaCriteria.list(params){
			createAlias("sala","sala")
			createAlias("sala.clinica","clinica")
			createAlias("paciente","p")
			createAlias("p.pessoa","pe")
			
			if(params.dataInicialBuscaAvancada && params.dataFimBuscaAvancada){
				between("dataConsulta",sdf.parse(params.dataInicialBuscaAvancada),sdf.parse(params.dataFimBuscaAvancada))
			}else if(params.dataInicialBuscaAvancada){
				eq("dataConsulta",sdf.parse(params.dataInicialBuscaAvancada))
			}else if(params.dataFimBuscaAvancada){
				eq("dataConsulta",sdf.parse(params.dataFimBuscaAvancada))
			}
			if(params.statusConsultaIdBuscaAvancada){
				eq("statusConsulta.id",params.statusConsultaIdBuscaAvancada.toInteger())
			}
			if(params.clinicaIdBuscaAvancada){
				eq("clinica.id",params.clinicaIdBuscaAvancada.toLong())
			}
			if(params.dataInicialLongBuscaAvancada){
				eq("dataConsulta",new Date(params.dataInicialLongBuscaAvancada?.toLong()))
			}
			if(params.dataInicialLongBuscaAvancada && params.dataFimLongBuscaAvancada){
				between("dataConsulta",new Date(params.dataInicialLongBuscaAvancada?.toLong()),new Date(params.dataFimLongBuscaAvancada?.toLong()))
			}
			if(params.nomePacienteBuscaAvancada){
				ilike("pe.nomeAscii", StringUtils.toAscii(params.nomePacienteBuscaAvancada)+"%")
			}
			
		}
		render(view:"list",model:[consultaInstanceList: result, consultaInstanceTotal:result.totalCount,dataInicialBuscaAvancada: params.dataInicialBuscaAvancada,dataFimBuscaAvancada:params.dataFimBuscaAvancada,turnoBuscaAvancada:params.turnoBuscaAvancada,clinicaIdBuscaAvancada:params.clinicaIdBuscaAvancada,statusConsultaIdBuscaAvancada:params.statusConsultaIdBuscaAvancada])
	}
	
    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		
		def consultaCriteria = Consulta.createCriteria()
		def result = consultaCriteria.list(params){
			if(!params.sort){
				order("dataConsulta", "desc")
			}
		}
        [consultaInstanceList: result, consultaInstanceTotal: Consulta.count()]
    }

    def create(Integer id) {
		if(id){
			def consulta = Consulta.get(id)
			
			def novoStatus = StatusConsulta.findByDescricao("REAGENDADA")
			AlteracaoStatusConsulta alteracaoStatusConsulta = new AlteracaoStatusConsulta()
			alteracaoStatusConsulta.consulta = consulta
			alteracaoStatusConsulta.statusConsultaAnterior = consulta.statusConsulta
			alteracaoStatusConsulta.statusConsultaAtual = novoStatus
			alteracaoStatusConsulta.usuarioMarcacao = session.usuario
			alteracaoStatusConsulta.motivo = "Nova consulta reagendada."
			
			consulta.statusConsulta = novoStatus
			
			alteracaoStatusConsulta.save(flush:true)
			consulta.save(flush:true)
			
			def novaConsulta = new Consulta()
			novaConsulta.statusConsulta = StatusConsulta.findByDescricao("A CONFIRMAR")
			novaConsulta.sala = consulta.sala
			novaConsulta.salaIndicacao = consulta.salaIndicacao
			novaConsulta.motivo = consulta.motivo
			novaConsulta.paciente = consulta.paciente
			novaConsulta.justificativa = consulta.justificativa
			novaConsulta.dataConsulta = new Date()
			
        	[consultaInstance: novaConsulta]
		}else{
			[consultaInstance: new Consulta(params)]
		}
    }

	def reagendamento(Long id){
		redirect(action: "create",controller: "consulta",id:id)
	}
	
    def save() {
        def consultaInstance = new Consulta(params)
		consultaInstance.statusConsulta = StatusConsulta.findByDescricao("A CONFIRMAR")
		consultaInstance.duracao = Agenda.findByClinica(consultaInstance.sala.clinica).duracao
		consultaInstance.usuarioMarcacao = session.usuario
		
		consultaInstance.statusPagamento = StatusPagamento.findByNome("PENDENTE")
		consultaInstance.valor = consultaInstance?.sala?.clinica?.valor
		
		if(!params.horarioConsulta){
			flash.message = "N&atilde;o existe hor&aacute;rio para esta data"
			render(view: "create", model: [consultaInstance: consultaInstance])
			return
		}
		
        if (!consultaInstance.save(flush: true)) {
            render(view: "create", model: [consultaInstance: consultaInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'consulta.label', default: 'Consulta'), consultaInstance.id])
        redirect(action: "show", id: consultaInstance.id)
    }

    def show(Long id) {
        def consultaInstance = Consulta.get(id)
        if (!consultaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'consulta.label', default: 'Consulta'), id])
            redirect(action: "list")
            return
        }

        [consultaInstance: consultaInstance]
    }

    def edit(Long id) {
        def consultaInstance = Consulta.get(id)
        if (!consultaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'consulta.label', default: 'Consulta'), id])
            redirect(action: "list")
            return
        }

        [consultaInstance: consultaInstance]
    }

    def update(Long id, Long version) {
        def consultaInstance = Consulta.get(id)
        if (!consultaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'consulta.label', default: 'Consulta'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (consultaInstance.version > version) {
                consultaInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'consulta.label', default: 'Consulta')] as Object[],
                          "Another user has updated this Consulta while you were editing")
                render(view: "edit", model: [consultaInstance: consultaInstance])
                return
            }
        }

        consultaInstance.properties = params

        if (!consultaInstance.save(flush: true)) {
            render(view: "edit", model: [consultaInstance: consultaInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'consulta.label', default: 'Consulta'), consultaInstance.id])
        redirect(action: "show", id: consultaInstance.id)
    }

    def delete(Long id) {
        def consultaInstance = Consulta.get(id)
        if (!consultaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'consulta.label', default: 'Consulta'), id])
            redirect(action: "list")
            return
        }

        try {
            consultaInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'consulta.label', default: 'Consulta'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'consulta.label', default: 'Consulta'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def obterDadosConsulta(){
		render Consulta.get(Long.parseLong(params.id)) as JSON
	}
	
	def marcarConsultaFilaEspera(Long id){
		def filaEsperaInstance = FilaEspera.get(id)
		def consultaInstance = new Consulta()
		
		consultaInstance.paciente = Paciente.findByPessoa(filaEsperaInstance.pessoa)
		consultaInstance.turno = filaEsperaInstance.turno
		consultaInstance.sala = Sala.findByClinica(filaEsperaInstance.clinica)
		consultaInstance.motivo = filaEsperaInstance.motivo
		
		if(consultaInstance.paciente){
			render(view:"create", model:[consultaInstance:consultaInstance])
		}
	}
}
