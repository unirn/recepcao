package br.edu.unirn.consulta

import org.springframework.dao.DataIntegrityViolationException

import br.edu.unirn.comum.Clinica

class StatusConsultaController {
	def statusConsultaService
	
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def listaStatusConsulta = {
		Long valor = Long.parseLong(params.id)
		def clinica = Clinica.get(valor)
		def listaStatus =statusConsultaService.buscarStatusClinica(clinica)
		
		render g.select(optionKey: "id", from: listaStatus, name:"statusConsulta")
		return
	}
	
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [statusConsultaInstanceList: StatusConsulta.list(params), statusConsultaInstanceTotal: StatusConsulta.count()]
    }
	
	def buscaSimples(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		params.offset = (params.offset ? params.offset.toInteger() : 0)
			
		def statusConsultaCriteria = StatusConsulta.createCriteria()
		def result = statusConsultaCriteria.list(params){
			ilike('descricao',"${params.nomeBuscaSimples}%")
		}
		render(view:"list",model:[statusConsultaInstanceList: result, statusConsultaInstanceTotal:result.totalCount,nomeBuscaSimples:params.nomeBuscaSimples])
	}
	
    def create() {
        [statusConsultaInstance: new StatusConsulta(params)]
    }

    def save() {
        def statusConsultaInstance = new StatusConsulta(params)
        if (!statusConsultaInstance.save(flush: true)) {
            render(view: "create", model: [statusConsultaInstance: statusConsultaInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'statusConsulta.label', default: 'StatusConsulta'), statusConsultaInstance.id])
        redirect(action: "show", id: statusConsultaInstance.id)
    }

    def show(Long id) {
        def statusConsultaInstance = StatusConsulta.get(id)
        if (!statusConsultaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'statusConsulta.label', default: 'StatusConsulta'), id])
            redirect(action: "list")
            return
        }

        [statusConsultaInstance: statusConsultaInstance]
    }

    def edit(Long id) {
        def statusConsultaInstance = StatusConsulta.get(id)
        if (!statusConsultaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'statusConsulta.label', default: 'StatusConsulta'), id])
            redirect(action: "list")
            return
        }

        [statusConsultaInstance: statusConsultaInstance]
    }

    def update(Long id, Long version) {
        def statusConsultaInstance = StatusConsulta.get(id)
        if (!statusConsultaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'statusConsulta.label', default: 'StatusConsulta'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (statusConsultaInstance.version > version) {
                statusConsultaInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'statusConsulta.label', default: 'StatusConsulta')] as Object[],
                          "Another user has updated this StatusConsulta while you were editing")
                render(view: "edit", model: [statusConsultaInstance: statusConsultaInstance])
                return
            }
        }

        statusConsultaInstance.properties = params

        if (!statusConsultaInstance.save(flush: true)) {
            render(view: "edit", model: [statusConsultaInstance: statusConsultaInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'statusConsulta.label', default: 'StatusConsulta'), statusConsultaInstance.id])
        redirect(action: "show", id: statusConsultaInstance.id)
    }

    def delete(Long id) {
        def statusConsultaInstance = StatusConsulta.get(id)
        if (!statusConsultaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'statusConsulta.label', default: 'StatusConsulta'), id])
            redirect(action: "list")
            return
        }

        try {
            statusConsultaInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'statusConsulta.label', default: 'StatusConsulta'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'statusConsulta.label', default: 'StatusConsulta'), id])
            redirect(action: "show", id: id)
        }
    }
}
