package br.edu.unirn.consulta

class BuscaProfissionalController {

    def profissionalService
	
	def index() {
		render(view: "consulta", params: params)
	}
	
	def consulta(){
		if(String.valueOf(params.id) || params.nome){
			def resultado = profissionalService.buscaProfissional(params)
			
			if(resultado.isEmpty()){
				flash.message = "Sem resultados encontrados!"
			}
			[nome:params.nome,idenficador:params.id,profissionalLista:resultado]
		}else{
			flash.message =  "Selecione ao menos 1 filtro!"
		}
	}
}
