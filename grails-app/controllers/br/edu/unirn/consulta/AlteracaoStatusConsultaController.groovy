package br.edu.unirn.consulta

import grails.converters.JSON
import br.edu.unirn.comum.Paciente

class AlteracaoStatusConsultaController {

	def save() {
		def alteracaoStatusConsulta = new AlteracaoStatusConsulta(params)
		alteracaoStatusConsulta.usuarioMarcacao = session.usuario
		def consulta = Consulta.get(alteracaoStatusConsulta.consulta.id)
		
		if (alteracaoStatusConsulta.save(flush: true)) {
			consulta.statusConsulta = alteracaoStatusConsulta.statusConsultaAtual
			consulta.save(flush: true)
		}
		render ([status:consulta.statusConsulta.descricao,cor:consulta.statusConsulta?.corListagem,consulta:consulta.id] as JSON)
	}
	
	def historicoAlteracaoConsulta(){
		def paciente = Paciente.get(params.id)
		def query = {
			createAlias("consulta","consulta")
			createAlias("consulta.paciente","paciente")
			eq("paciente.id",paciente.id)	
		}
		
		//def listaAlteracaoConsulta = AlteracaoStatusConsulta.createCriteria().list(query)
		def listaAlteracaoConsulta = AlteracaoStatusConsulta.createCriteria().list(query)
		
		[listaAlteracaoConsulta:listaAlteracaoConsulta,pacienteInstance:paciente,listaConsulta:Consulta.findAllByPaciente(paciente)]	
	}
}
