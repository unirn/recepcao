package br.edu.unirn.consulta

import org.springframework.dao.DataIntegrityViolationException

import br.edu.unirn.comum.Paciente;
import br.edu.unirn.comum.Sala;
import br.edu.unirn.tipos.StatusFilaEspera
import br.edu.unirn.tipos.Turno;

class FilaEsperaController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		
		def filaEsperaCriteria = FilaEspera.createCriteria()
		def result = filaEsperaCriteria.list(params){
			ne("statusFilaEspera",StatusFilaEspera.DESISTIU)
		}
        [filaEsperaInstanceList: result, filaEsperaInstanceTotal: result.totalCount]
    }
	
	def buscaSimples(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		params.offset = (params.offset ? params.offset.toInteger() : 0)
			
		def filaEsperaCriteria = FilaEspera.createCriteria()
		def result = filaEsperaCriteria.list(params){
			ilike("nome","${params.nomeBuscaSimples}%")
		}
		render(view:"list",model:[filaEsperaInstanceList: result,filaEsperaInstanceTotal:result.totalCount,nomeBuscaSimples:params.nomeBuscaSimples])
	}
	
	def buscaAvancada(Integer max){
		params.max = Math.min(max ?: 10, 100)
		params.offset = (params.offset ? params.offset.toInteger() : 0)

		def filaEsperaCriteria = FilaEspera.createCriteria()
		def result = filaEsperaCriteria.list(params){
			if(params.nomeBuscaAvancada){
				ilike("nome","${params.nomeBuscaAvancada}%")
			}
			if(params.clinicaBuscaAvancada){
				eq("clinica.id",params.clinicaBuscaAvancada.toLong())
			}
			if(params.turnoBuscaAvancada){
				eq("turno",Turno[params.turnoBuscaAvancada])
			}
			if(params.statusBuscaAvancada){
				eq("statusFilaEspera", StatusFilaEspera[params.statusBuscaAvancada])
			}
			
		}
		render(view:"list",model:[filaEsperaInstanceList: result,filaEsperaInstanceTotal:result.totalCount,nomeBuscaAvancada:params.nomeBuscaAvancada,clinicaBuscaAvancada:params.clinicaBuscaAvancada,turnoBuscaAvancada:params.turnoBuscaAvancada,statusBuscaAvancada:params.statusBuscaAvancada])
	}

    def create() {
        [filaEsperaInstance: new FilaEspera(params)]
    }

    def save() {
        def filaEsperaInstance = new FilaEspera(params)
		filaEsperaInstance.statusFilaEspera = StatusFilaEspera.AGUARDANDO
		filaEsperaInstance.usuarioMarcacao = session.usuario
		if (filaEsperaInstance.pessoa.id == 0 ){
			filaEsperaInstance.pessoa = null
			filaEsperaInstance.nome = params.pessoaAutoComplete
		}
        if (!filaEsperaInstance.save(flush: true)) {
            render(view: "create", model: [filaEsperaInstance: filaEsperaInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'filaEspera.label', default: 'FilaEspera'), filaEsperaInstance.id])
        redirect(action: "show", id: filaEsperaInstance.id)
    }

    def show(Long id) {
        def filaEsperaInstance = FilaEspera.get(id)
        if (!filaEsperaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'filaEspera.label', default: 'FilaEspera'), id])
            redirect(action: "list")
            return
        }

        [filaEsperaInstance: filaEsperaInstance]
    }

    def edit(Long id) {
        def filaEsperaInstance = FilaEspera.get(id)
        if (!filaEsperaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'filaEspera.label', default: 'FilaEspera'), id])
            redirect(action: "list")
            return
        }

        [filaEsperaInstance: filaEsperaInstance]
    }

    def update(Long id, Long version) {
        def filaEsperaInstance = FilaEspera.get(id)
        if (!filaEsperaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'filaEspera.label', default: 'FilaEspera'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (filaEsperaInstance.version > version) {
                filaEsperaInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'filaEspera.label', default: 'FilaEspera')] as Object[],
                          "Another user has updated this FilaEspera while you were editing")
                render(view: "edit", model: [filaEsperaInstance: filaEsperaInstance])
                return
            }
        }

        filaEsperaInstance.properties = params
		
		if(filaEsperaInstance?.pessoa?.id == 0){
			filaEsperaInstance.pessoa = null
		}

        if (!filaEsperaInstance.save(flush: true)) {
            render(view: "edit", model: [filaEsperaInstance: filaEsperaInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'filaEspera.label', default: 'FilaEspera'), filaEsperaInstance.id])
        redirect(action: "show", id: filaEsperaInstance.id)
    }

    def delete(Long id) {
        def filaEsperaInstance = FilaEspera.get(id)
        if (!filaEsperaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'filaEspera.label', default: 'FilaEspera'), id])
            redirect(action: "list")
            return
        }

        filaEsperaInstance.statusFilaEspera = StatusFilaEspera.DESISTIU 
		filaEsperaInstance.save(flush:true)
		
        flash.message = "Status da Lista de Espera modificado para Desistiu."
        redirect(action: "list")
      
    }
	
	def deleteList(Long id) {
		params.list("filaEsperaID").each{
			def filaEsperaInstance = FilaEspera.get(it.toLong())
			if (!filaEsperaInstance) {
				render(status:500,text:"Registro n&atilde;o encontrado!")
				return
			}
			try {
				filaEsperaInstance.statusFilaEspera = StatusFilaEspera.DESISTIU
				filaEsperaInstance.save(flush: true)
				
				render(status:200,text:"Alterado com Sucesso!")
				return
			}
			catch (DataIntegrityViolationException e) {
				render(status:200,text:message(code: 'default.not.deleted.message', args: [message(code: 'filaEspera.label', default: 'Fila Espera'), id]))
				return
			}
		}

	}
	
	def marcar(Long id){
		def filaEsperaInstance = FilaEspera.get(id)
		
		def pacienteInstance
		
		filaEsperaInstance.statusFilaEspera = StatusFilaEspera.MARCOU
		filaEsperaInstance.save(flush:true)
		
		if (filaEsperaInstance.pessoa){
			pacienteInstance = Paciente.findByPessoa(filaEsperaInstance.pessoa)
		}
	
		if(pacienteInstance){
			//Caso paciente exista, marcar consulta
			redirect(controller:"consulta", action:"marcarConsultaFilaEspera", id:id)
		}else{
			//Caso contrario criar paciente
			redirect(controller:"paciente", action:"marcarConsultaFilaEspera", id:id)
		}
	}
}
