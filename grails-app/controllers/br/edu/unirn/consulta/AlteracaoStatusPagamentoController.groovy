package br.edu.unirn.consulta

import grails.converters.JSON

class AlteracaoStatusPagamentoController {

	def update(){
		def idconsulta = Long.parseLong(params.consulta.id)
		def idstatusPagamento = Long.parseLong(params.statusPagamento.id)
		
		def consulta = Consulta.get(idconsulta)
		def paciente = consulta.paciente
		
		if(!paciente.pessoa.nome || !paciente.pessoa.dataNascimento || 
			!paciente.pessoa.ocupacao || !paciente.pessoa.logradouro || !paciente.pessoa.telefoneContato
			|| !paciente.pessoa.grauInstrucao){
			render([statusPaciente: false,pacienteId: paciente.id] as JSON)
			return
		}
		def statusConsulta = StatusPagamento.get(idstatusPagamento)
		def valor = Float.parseFloat(params.valor)
		
		if(consulta.save(flush:true)){
			consulta.statusPagamento = statusConsulta
			consulta.valor = valor
		}
		render([statusPaciente:true, status: consulta.statusPagamento.nome, cor: consulta.statusPagamento.corListagem, consulta: consulta.id] as JSON)
	}	
}
