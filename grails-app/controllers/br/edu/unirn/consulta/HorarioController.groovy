package br.edu.unirn.consulta

import grails.converters.JSON
import java.text.SimpleDateFormat;

import br.edu.unirn.tipos.Turno

class HorarioController {

	def agendaService
	def horarioService
	
	//http://127.0.0.1:8080/recepcao/horario/gerarComboHorario?dataConsulta=06/09/2012&clinicaID=1&turno=MANHA
   def gerarComboHorario = {
		def clinicaID = params.clinicaID
		def salaID = params.salaID
		def dataConsulta = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataConsulta)
		
		def turno
		Turno[] turnos = Turno.values()
		for (int i = 0; i < turnos.length; i++) {
			if(turnos[i].toString().equals(params.turno)){
				turno = turnos[i]
				break
			}
		}
		
		def agenda = agendaService.buscarAgenda(turno,clinicaID)
		
		def retorno="<select name='horarioConsulta' id='horarioConsulta'>"
		
		if(agenda){
			def lista = horarioService.geracaoListaHorario(agenda.duracao,agenda.horaInicio,agenda.horaFim)
			def disponivel = false
			lista.each{
				
				def nome
				if(dataConsulta){
					nome = horarioService.verificarDisponibilidadeClinica(agenda.clinica, it, dataConsulta,null,turno,salaID)
				}
				
				if(nome){
					retorno+= "<option value='${it}' disabled>${it} - ${nome}</option>"
				}else{
					retorno+= "<option value='${it}'>${it}</option>"
					disponivel = true
				}
			}
			if(!disponivel){
				retorno+= "<option value='' selected>Nenhum Horario Disponivel</option>"
			}
			retorno+= "</select>"
			
			render retorno
			return
		}
		render "null"
	}
   
   //http://127.0.0.1:8080/recepcao/horario/gerarHorario?dataConsulta=06/09/2012&clinicaID=1&turno=MANHA
   def gerarHorario = {
	   def clinicaID = params.clinicaID
	   def dataConsulta = new SimpleDateFormat("dd/MM/yyyy").parse(params.dataConsulta)
	   
	   def turno
	   Turno[] turnos = Turno.values()
	   for (int i = 0; i < turnos.length; i++) {
		   if(turnos[i].toString().equals(params.turno)){
			   turno = turnos[i]
			   break
		   }
	   }
	   
	   def agenda = agendaService.buscarAgenda(turno,clinicaID)
	   def retorno = []
	   
	   if(agenda){
		   def lista = horarioService.geracaoListaHorario(agenda.duracao,agenda.horaInicio,agenda.horaFim)
		   lista.each {
			   def existe
			   if(dataConsulta){
				   existe = horarioService.verificarDisponibilidadeClinica(agenda.clinica, it, dataConsulta,null,turno)
				   if(!existe){
					   retorno.add(it)
				  }
			   }
			   
		   }
		   
		   render retorno as JSON
		   return
	   }
	   render "null"
   }
}
