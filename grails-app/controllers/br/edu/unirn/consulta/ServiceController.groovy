package br.edu.unirn.consulta

import grails.converters.JSON

class ServiceController {
	//Servi�o WS Android
	def consultaService

	//http://localhost:8080/recepcao/service/consultaPaciente/?cpf=123
	def consultaPaciente = { 
		def cpf = Long.parseLong(params.cpf)
		render consultaService.consultaPorPaciente(cpf) as JSON
	}
	
	//http://localhost:8080/recepcao/service/buscarConsultaPerido/?datainicio=2012-10-12&datafim=2012-10-19&idclinica=1
	def buscarConsultaPerido = {
		def idclinica = Long.parseLong(params.idclinica)
		if(idclinica == 0){
			render consultaService.buscarConsultaPeridoGeral(params) as JSON
		}
		render consultaService.buscarConsultaPerido(params) as JSON
	}
	
	//http://localhost:8080/recepcao/service/buscarConsultaMes
	def buscarConsultaMes = {
		render consultaService.buscarConsultaMes() as JSON
	}
	
	//http://localhost:8080/recepcao/service/buscarConsultaDia
	def buscarConsultaDia = {
		render consultaService.buscarConsultaDia() as JSON
	}
	
	//http://localhost:8080/recepcao/service/alterarStatusConsulta/?idConsulta=1&idStatus=2
	def alterarStatusConsulta = {
		render consultaService.alterarStatusConsulta(params)
	}
}
