package br.edu.unirn.inicio

import br.edu.unirn.comum.Clinica;
import br.edu.unirn.comum.Paciente;
import br.edu.unirn.consulta.Consulta
import br.edu.unirn.consulta.StatusConsulta
import br.edu.unirn.utils.CalendarUtils

class IndexController {

	def consultaService
	
    def index() {
		def dataFimMes = CalendarUtils.dataUltimoDiaMes().getTime()
		def listaData = [:]
		listaData.put("dataAtual", new Date().getTime())
		listaData.put("dataInicioMes",CalendarUtils.dataPrimeiroDiaMes().getTime())
		listaData.put("dataFimMes",CalendarUtils.dataUltimoDiaMes().getTime())
		
		def totalConsultaPorStatus = [:]
		def totalConsultaPorClinica = [:]
		
		StatusConsulta.list().each { status ->
			def quantidade = Consulta.executeQuery(
				"select count(id) from Consulta where dataConsulta=:dataAtual and statusConsulta.id=:idStatus",[dataAtual:new Date().clearTime(),idStatus:status.id])
			totalConsultaPorStatus.put(status.id, quantidade[0])
		}
		Clinica.list().each {clinica->
			def quantidade = Consulta.executeQuery(
				"select count(id) from Consulta where dataConsulta=:dataAtual and sala.clinica.id=:idClinica",[dataAtual:new Date().clearTime(),idClinica:clinica.id])
			totalConsultaPorClinica.put(clinica.id, quantidade[0])
			
		}
		[totalConsultaHoje:consultaService.totalConsultaHoje(),totalConsultaMes:consultaService.totalConsultaMes(),
			listaData:listaData,totalConsultaPorStatus:totalConsultaPorStatus,totalConsultaPorClinica:totalConsultaPorClinica]
	}
}
