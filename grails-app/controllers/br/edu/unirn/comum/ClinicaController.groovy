package br.edu.unirn.comum

import org.springframework.dao.DataIntegrityViolationException


class ClinicaController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		def clinicaCriteria = Clinica.createCriteria()
		def result = clinicaCriteria.list(params){
			eq("ativo", true)
		}
        [clinicaInstanceList: result, clinicaInstanceTotal: result.totalCount]
    }
	
	def buscaSimples(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		params.offset = (params.offset ? params.offset.toInteger() : 0)
			
		def clinicaCriteria = Clinica.createCriteria()
		def result = clinicaCriteria.list(params){
			eq("ativo", true)
			ilike('descricao',"${params.nomeBuscaSimples}%")
		}
		render(view:"list",model:[clinicaInstanceList: result, clinicaInstanceTotal:result.totalCount,nomeBuscaSimples:params.nomeBuscaSimples])
	}
	
    def create() {
        [clinicaInstance: new Clinica(params)]
    }

    def save() {
        def clinicaInstance = new Clinica(params)
		clinicaInstance.ativo = true 
        if (!clinicaInstance.save(flush: true)) {
            render(view: "create", model: [clinicaInstance: clinicaInstance])
            return
        }
		
		
        flash.message = message(code: 'default.created.message', args: [message(code: 'clinica.label', default: 'Clinica'), clinicaInstance.id])
        redirect(action: "show", id: clinicaInstance.id)
    }

    def show(Long id) {
        def clinicaInstance = Clinica.get(id)
        if (!clinicaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'clinica.label', default: 'Clinica'), id])
            redirect(action: "list")
            return
        }

        [clinicaInstance: clinicaInstance]
    }

    def edit(Long id) {
        def clinicaInstance = Clinica.get(id)
        if (!clinicaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'clinica.label', default: 'Clinica'), id])
            redirect(action: "list")
            return
        }

        [clinicaInstance: clinicaInstance]
    }

    def update(Long id, Long version) {
        def clinicaInstance = Clinica.get(id)
        if (!clinicaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'clinica.label', default: 'Clinica'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (clinicaInstance.version > version) {
                clinicaInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'clinica.label', default: 'Clinica')] as Object[],
                          "Another user has updated this Clinica while you were editing")
                render(view: "edit", model: [clinicaInstance: clinicaInstance])
                return
            }
        }

        clinicaInstance.properties = params

        if (!clinicaInstance.save(flush: true)) {
            render(view: "edit", model: [clinicaInstance: clinicaInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'clinica.label', default: 'Clinica'), clinicaInstance.id])
        redirect(action: "show", id: clinicaInstance.id)
    }

    def delete(Long id) {
        def clinicaInstance = Clinica.get(id)
        if (!clinicaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'clinica.label', default: 'Clinica'), id])
            redirect(action: "list")
            return
        }

        try {
            clinicaInstance.ativo = false
			clinicaInstance.save(flush: true)
			
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'clinica.label', default: 'Clinica'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'clinica.label', default: 'Clinica'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def deleteList(Long id) {
		params.list("clinicaID").each{
			def clinicaInstance = Clinica.get(it.toLong())
			if (!clinicaInstance) {
				render(status:500,text:"Clinica n&atilde;o encontrada!")
				return
			}
			try {
				clinicaInstance.ativo = false
				clinicaInstance.save(flush: true)
				
				render(status:200,text:"Inativada com Sucesso!")
				return
			}
			catch (DataIntegrityViolationException e) {
				render(status:200,text:message(code: 'default.not.deleted.message', args: [message(code: 'clinica.label', default: 'Clinica'), id]))
				return
			}
		}

	}
}
