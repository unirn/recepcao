package br.edu.unirn.comum

import org.springframework.dao.DataIntegrityViolationException


class SalaController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		def salaCriteria = Sala.createCriteria()
		def result = salaCriteria.list(params){
			eq("ativo", true)
		}
        [salaInstanceList: result, salaInstanceTotal: result.totalCount]
    }
	
	def buscaSimples(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		params.offset = (params.offset ? params.offset.toInteger() : 0)
			
		def salaCriteria = Sala.createCriteria()
		def result = salaCriteria.list(params){
			eq("ativo", true)
			ilike('nome',"${params.nomeBuscaSimples}%")
		}
		render(view:"list",model:[salaInstanceList: result, salaInstanceTotal:result.totalCount,nomeBuscaSimples:params.nomeBuscaSimples])
	}

    def create() {
        [salaInstance: new Sala(params)]
    }

    def save() {
        def salaInstance = new Sala(params)
        if (!salaInstance.save(flush: true)) {
            render(view: "create", model: [salaInstance: salaInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'sala.label', default: 'Sala'), salaInstance.id])
        redirect(action: "show", id: salaInstance.id)
    }

    def show(Long id) {
        def salaInstance = Sala.get(id)
        if (!salaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sala.label', default: 'Sala'), id])
            redirect(action: "list")
            return
        }

        [salaInstance: salaInstance]
    }

    def edit(Long id) {
        def salaInstance = Sala.get(id)
        if (!salaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sala.label', default: 'Sala'), id])
            redirect(action: "list")
            return
        }

        [salaInstance: salaInstance]
    }

    def update(Long id, Long version) {
        def salaInstance = Sala.get(id)
        if (!salaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sala.label', default: 'Sala'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (salaInstance.version > version) {
                salaInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'sala.label', default: 'Sala')] as Object[],
                          "Another user has updated this Sala while you were editing")
                render(view: "edit", model: [salaInstance: salaInstance])
                return
            }
        }

        salaInstance.properties = params

        if (!salaInstance.save(flush: true)) {
            render(view: "edit", model: [salaInstance: salaInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'sala.label', default: 'Sala'), salaInstance.id])
        redirect(action: "show", id: salaInstance.id)
    }

    def delete(Long id) {
        def salaInstance = Sala.get(id)
        if (!salaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sala.label', default: 'Sala'), id])
            redirect(action: "list")
            return
        }

        try {
            salaInstance.ativo = false
			salaInstance.save(flush: true)
			
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'sala.label', default: 'Sala'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'sala.label', default: 'Sala'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def deleteList(Long id) {
		params.list("salaID").each{
			def salaInstance = Sala.get(it.toLong())
			if (!salaInstance) {
				render(status:500,text:"Sala n&atilde;o encontrada!")
				return
			}
			try {
				salaInstance.ativo = false
				salaInstance.save(flush: true)
				
				render(status:200,text:"Inativada com Sucesso!")
				return
			}
			catch (DataIntegrityViolationException e) {
				render(status:200,text:message(code: 'default.not.deleted.message', args: [message(code: 'sala.label', default: 'Sala'), id]))
				return
			}
		}
	}
	
	def gerarComboSalaClinica(){
		Long valor = Long.parseLong(params.id)
		def name = params.name
		def clinica = Clinica.get(valor)
		def listaSala = Sala.findAllByClinica(clinica)
		if(listaSala){
			render g.select(optionKey: "id", from: listaSala, id: "${name}", name: "${name}.id", noSelection : [null:'-- Selecione --'])
			return
		}else
			render "null"
	}
}
