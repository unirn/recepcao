package br.edu.unirn.comum

import org.springframework.dao.DataIntegrityViolationException

class ProfissionalController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [profissionalInstanceList: Profissional.list(params), profissionalInstanceTotal: Profissional.count()]
    }

    def create() {
        [profissionalInstance: new Profissional(params)]
    }

   def save() {
	   	if(params.semestre.equals("null")){
	   		params.semestre = null
		}
		def pessoaInstance = new Pessoa(params)
        def profissionalInstance = new Profissional(params)
		profissionalInstance.pessoa = pessoaInstance
		def mensagemErroPessoa
		
		if(!pessoaInstance.validate()){
			pessoaInstance.errors.allErrors.each {
				mensagemErroPessoa += it.toString()
			}
			flash.message = mensagemErroPessoa
			render(view: "create", model: [profissionalInstance: profissionalInstance])
			return
		}
		
        if (!profissionalInstance.save(flush: true)) {
            render(view: "create", model: [profissionalInstance: profissionalInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'paciente.label', default: 'Paciente'), profissionalInstance.id])
		redirect(action: "show", id: profissionalInstance.id)
    }

    def show(Long id) {
        def profissionalInstance = Profissional.get(id)
        if (!profissionalInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'profissional.label', default: 'Profissional'), id])
            redirect(action: "list")
            return
        }
		
        [profissionalInstance: profissionalInstance]
    }

    def edit(Long id) {
        def profissionalInstance = Profissional.get(id)
        if (!profissionalInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'profissional.label', default: 'Profissional'), id])
            redirect(action: "list")
            return
        }

        [profissionalInstance: profissionalInstance]
    }

    def update(Long id, Long version) {
        def profissionalInstance = Profissional.get(id)
        if (!profissionalInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'profissional.label', default: 'Profissional'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (profissionalInstance.version > version) {
                profissionalInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'profissional.label', default: 'Profissional')] as Object[],
                          "Another user has updated this Profissional while you were editing")
                render(view: "edit", model: [profissionalInstance: profissionalInstance])
                return
            }
        }
		if(params.semestre.equals("null")){
			params.semestre = null
	    }
        profissionalInstance.properties = params
		
        def pessoaInstance = Pessoa.get(profissionalInstance.pessoa.id)
        pessoaInstance.properties = params
		profissionalInstance.pessoa = pessoaInstance
		
        if (!profissionalInstance.save(flush: true)) {
            render(view: "edit", model: [profissionalInstance: profissionalInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'profissional.label', default: 'Profissional'), profissionalInstance.id])
        redirect(action: "show", id: profissionalInstance.id)
    }

    def delete(Long id) {
        def profissionalInstance = Profissional.get(id)
        if (!profissionalInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'profissional.label', default: 'Profissional'), id])
            redirect(action: "list")
            return
        }

        try {
            profissionalInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'profissional.label', default: 'Profissional'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'profissional.label', default: 'Profissional'), id])
            redirect(action: "show", id: id)
        }
    }
}
