package br.edu.unirn.comum

import br.edu.unirn.service.PacienteService;

class BuscaPacienteController {

	def pacienteService
	
    def index() {
		render(view: "consulta", params: params)
	}
	
	def consulta(){
		if(String.valueOf(params.numeroProntuario) || params.nome){
			def resultado = pacienteService.buscaPaciente(params)
			
			if(resultado.isEmpty()){
				flash.message= "Sem resultado encontrado!"
			}
			[nome:params.nome,identificador:params.numeroProntuario,pacienteLista:resultado]
		}else{
			flash.message= "Selecione ao menos 1 filtro!"
		}
	}
}
