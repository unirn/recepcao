package br.edu.unirn.comum

import grails.converters.JSON

import org.springframework.dao.DataIntegrityViolationException

import br.edu.unirn.consulta.FilaEspera;
import br.edu.unirn.utils.StringUtils;

class PacienteController {

	def pacienteService
	def pessoaService
	
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }
	
	def autoCompletePaciente = {
		render pacienteService.pacienteAutoComplete(params) as JSON
	}
	
	def autoCompletePessoa = {
		render pessoaService.pessoaAutoComplete(params) as JSON
	}
	
	def buscaSimples(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		params.offset = (params.offset ? params.offset.toInteger() : 0)
			
		def pacienteCriteria = Paciente.createCriteria()
		def result = pacienteCriteria.list(params){
			createAlias('pessoa','p')
			eq("inativo",false)
			ilike('p.nome',"${params.nomeBuscaSimples}%")
			
		}
		render(view:"list",model:[pacienteInstanceList: result, pacienteInstanceTotal:result.totalCount,nomeBuscaSimples:params.nomeBuscaSimples])
	}
	
	def buscaAvancada(Integer max){
		params.max = Math.min(max ?: 10, 100)
		params.offset = (params.offset ? params.offset.toInteger() : 0)
		
		def pacienteCriteria = Paciente.createCriteria()
		def result = pacienteCriteria.list(params){
			createAlias('pessoa','p')
			eq("inativo",false)
			if(params.nomeBuscaAvancada){
				ilike('p.nome',"${params.nomeBuscaAvancada}%")
			}
			if(params.pacienteIdBuscaAvancada){
				eq('numeroProntuario',params.pacienteIdBuscaAvancada.toLong())
			}
		}
		render(view:"list",model:[pacienteInstanceList: result, pacienteInstanceTotal:result.totalCount,pacienteIdBuscaAvancada:params.pacienteIdBuscaAvancada,nomeBuscaAvancada:params.nomeBuscaAvancada])
	}
	
    def list(Integer max) {
    	params.max = Math.min(max ?: 10, 100)
		def pacienteCriteria = Paciente.createCriteria()
		def result = pacienteCriteria.list(params){
			eq("inativo", false)
		}
        [pacienteInstanceList: result, pacienteInstanceTotal: result.totalCount]
    }

    def create() {
        [pacienteInstance: new Paciente(params)]
    }

    def save() {
		def pessoaInstance = new Pessoa(params)
		pessoaInstance.nomeAscii = StringUtils.toAscii(pessoaInstance.nome)
        
		def pacienteInstance = new Paciente(params)
		pacienteInstance.pessoa = pessoaInstance
		pacienteInstance.usuarioCadastro = session.usuario
		
		def mensagemErroPessoa
		
		if(!pessoaInstance.validate()){
			pessoaInstance.errors.allErrors.each {
				mensagemErroPessoa += it.toString()
			}
			flash.error = "Erro ao Cadastrar!"
			render(view: "create", model: [pacienteInstance: pacienteInstance])
			return
		}
		
        if (!pacienteInstance.save(flush: true)) {
			flash.error = "Erro ao Cadastrar!"
            render(view: "create", model: [pacienteInstance: pacienteInstance])
            return
        }
		
		if(!pacienteInstance.numeroProntuario){
			pacienteInstance.numeroProntuario = pacienteInstance.id
			pacienteInstance.save(flush:true)
		}
		
        flash.message = "Cadastrado com Sucesso!"
		flash.nome = pacienteInstance?.pessoa?.nome
		flash.id = pacienteInstance?.id
        redirect(action: "list")
    }
	
	def cadastroRapido(){
		def pessoaInstance = new Pessoa(params)
		pessoaInstance.nomeAscii = StringUtils.toAscii(pessoaInstance.nome)
		def pacienteInstance = new Paciente(params)
		pacienteInstance.pessoa = pessoaInstance
		pacienteInstance.usuarioCadastro = session.usuario
		pacienteInstance.vinculoInstituicao = VinculoInstituicao.get(3)
		
		if(!pacienteInstance.validate()){
			pacienteInstance.errors.each {
				println it
			}
		}
		
		pacienteInstance.save(flush: true)
		
		if(!pacienteInstance.numeroProntuario){
			pacienteInstance.numeroProntuario = pacienteInstance.id
			pacienteInstance.save(flush:true)
		}
		def retorno = [:]
		retorno.put("nome",pessoaInstance.nome)
		retorno.put("id",pacienteInstance.id)
		
		render retorno as JSON
	}

    def show(Long id) {
        def pacienteInstance = Paciente.get(id)
        if (!pacienteInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'paciente.label', default: 'Paciente'), id])
            redirect(action: "list")
            return
        }

        [pacienteInstance: pacienteInstance]
    }

    def edit(Long id) {
        def pacienteInstance = Paciente.get(id)
        if (!pacienteInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'paciente.label', default: 'Paciente'), id])
            redirect(action: "list")
            return
        }

        [pacienteInstance: pacienteInstance]
    }

    def update(Long id, Long version) {
        def pacienteInstance = Paciente.get(id)
        if (!pacienteInstance) {
            flash.error = "Paciente n&atilde;o encontrado!"
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (pacienteInstance.version > version) {
                pacienteInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'paciente.label', default: 'Paciente')] as Object[],
                          "Another user has updated this Paciente while you were editing")
                render(view: "edit", model: [pacienteInstance: pacienteInstance])
                return
            }
        }

        pacienteInstance.properties = params
		
		//Corrigindo duplicidade de pessoas ao editar um paciente;Buscar pessoa pelo id e alterar seus atributos para add no paciente.
		def pessoaInstance = Pessoa.get(pacienteInstance.pessoa.id)
		pessoaInstance.properties = params
		pacienteInstance.pessoa = pessoaInstance

        if (!pacienteInstance.save(flush: true)) {
			flash.error = "Ocorreu um erro ao Alterar!"
            render(view: "edit", model: [pacienteInstance: pacienteInstance])
            return
        }

		flash.message = "Alterado com Sucesso!"
		flash.nome = pacienteInstance?.pessoa?.nome
		flash.id = pacienteInstance?.id
        redirect(action: "list")
    }
	def delete(Long id){
		def pacienteInstance = Paciente.get(id)
		if (!pacienteInstance) {
			flash.error = "Paciente n&atilde;o encontrado!"
			redirect(action: "list")
			return
		}
		try {
			pacienteInstance.inativo = true
			pacienteInstance.save(flush: true)
			flash.message = "Inativado com Sucesso!"
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'paciente.label', default: 'Paciente'), id])
			redirect(action: "list")
		}
	}
	
    def deleteList(Long id) {
		params.list("pacienteID").each{
			def pacienteInstance = Paciente.get(it.toLong())
			if (!pacienteInstance) {
				render(status:500,text:"Paciente n&atilde;o encontrado!")
				return
			}
			try {
				pacienteInstance.inativo = true
				pacienteInstance.save(flush: true)
				
				render(status:200,text:"Inativado com Sucesso!")
				return
			}catch (DataIntegrityViolationException e) {
				render(status:200,text:message(code: 'default.not.deleted.message', args: [message(code: 'paciente.label', default: 'Paciente'), id]))
				return
			}
		}

    }
	
	def marcarConsultaFilaEspera(Long id){
		def filaEsperaInstance = FilaEspera.get(id)
		def pacienteInstance = new Paciente()
		
		pacienteInstance.pessoa = new Pessoa()
		pacienteInstance.pessoa.nome = filaEsperaInstance.nome
		pacienteInstance.pessoa.observacao = filaEsperaInstance.motivo
		pacienteInstance.pessoa.telefoneContato = filaEsperaInstance.telefoneContato
		
		pacienteInstance.encaminhado = filaEsperaInstance.clinica
		
		render(view:"create", model:[pacienteInstance:pacienteInstance]) 
	}
}
