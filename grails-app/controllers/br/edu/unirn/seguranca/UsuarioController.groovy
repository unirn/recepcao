package br.edu.unirn.seguranca

import org.springframework.dao.DataIntegrityViolationException

class UsuarioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [usuarioInstanceList: Usuario.list(params), usuarioInstanceTotal: Usuario.count()]
    }
	
	def buscaSimples(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		params.offset = (params.offset ? params.offset.toInteger() : 0)
			
		def usuarioCriteria = Usuario.createCriteria()
		def result = usuarioCriteria.list(params){
			or{
				ilike('nome',"${params.nomeBuscaSimples}%")
				ilike('login',"${params.nomeBuscaSimples}%")
			}
		}
		render(view:"list",model:[usuarioInstanceList: result, usuarioInstanceTotal:result.totalCount,nomeBuscaSimples:params.nomeBuscaSimples])
	}
	
    def create() {
        [usuarioInstance: new Usuario(params)]
    }

    def save() {
		if(params.senha){
			if(!params.senha.equals(params.senhaConfirm)){
				flash.message = "Verifique o campo Senha"
				render(view: "create", model: [usuarioInstance: new Usuario(params)])
			}
			params.senha = params.senha.encodeAsMD5();
		}
        def usuarioInstance = new Usuario(params)
		if (!usuarioInstance.save(flush: true,failOnErrors:true)) {
            render(view: "create", model: [usuarioInstance: usuarioInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'usuario.label', default: 'Usuario'), usuarioInstance.id])
        redirect(action: "show", id: usuarioInstance.id)
    }
	
	def solicitar() {
		[usuarioInstance:new Usuario(params)]
	}
	
	def efetivarSolicitacao() {
		def usuarioInstance = new Usuario(params)
		usuarioInstance.enabled = false
		UsuarioPermissao.create(usuarioInstance, Permissao.findByAuthority("nenhuma"))
		if (!usuarioInstance.save(flush: true)) {
			render(view: "solicitar", model: [usuarioInstance: usuarioInstance])
			return
		}

		flash.message = "Solicitado cadastro de Usuario! Aguarde libera��o!"
		redirect(action: "solicitar")
	}

    def show(Long id) {
        def usuarioInstance = Usuario.get(id)
        if (!usuarioInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'usuario.label', default: 'Usuario'), id])
            redirect(action: "list")
            return
        }

        [usuarioInstance: usuarioInstance]
    }

    def edit(Long id) {
        def usuarioInstance = Usuario.get(id)
        if (!usuarioInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'usuario.label', default: 'Usuario'), id])
            redirect(action: "list")
            return
        }

        [usuarioInstance: usuarioInstance]
    }

    def update(Long id, Long version) {
		if(params.senha){
			if(!params.senha.equals(params.senhaConfirm)){
				println "ENTROU"
				flash.message = "Verifique o campo Senha"
				render(view: "edit", model: [usuarioInstance: new Usuario(params)])
				return
			}
			params.senha = params.senha.encodeAsMD5();
		}
        def usuarioInstance = Usuario.get(id)
		
        if (!usuarioInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'usuario.label', default: 'Usuario'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (usuarioInstance.version > version) {
                usuarioInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'usuario.label', default: 'Usuario')] as Object[],
                          "Another user has updated this Usuario while you were editing")
                render(view: "edit", model: [usuarioInstance: usuarioInstance])
                return
            }
        }

        usuarioInstance.properties = params

        if (!usuarioInstance.save(flush: true)) {
            render(view: "edit", model: [usuarioInstance: usuarioInstance])
            return
        }
        flash.message = message(code: 'default.updated.message', args: [message(code: 'usuario.label', default: 'Usuario'), usuarioInstance.id])
        redirect(action: "show", id: usuarioInstance.id)
    }

    def delete(Long id) {
        def usuarioInstance = Usuario.get(id)
        if (!usuarioInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'usuario.label', default: 'Usuario'), id])
            redirect(action: "list")
            return
        }

        usuarioInstance.habilitado = false
        try {
            usuarioInstance.save(flush: true)
            flash.message = message(code: 'default.updated.message', args: [message(code: 'usuario.label', default: 'Usuario'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.updated.message', args: [message(code: 'usuario.label', default: 'Usuario'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def habilitar(Long id){
		def usuarioInstance = Usuario.get(id)
		if (!usuarioInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'usuario.label', default: 'Usuario'), id])
			redirect(action: "list")
			return
		}

		usuarioInstance.habilitado = true
		try {
			usuarioInstance.save(flush: true)
			flash.message = message(code: 'default.updated.message', args: [message(code: 'usuario.label', default: 'Usuario'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.updated.message', args: [message(code: 'usuario.label', default: 'Usuario'), id])
			redirect(action: "show", id: id)
		}
	}
}
