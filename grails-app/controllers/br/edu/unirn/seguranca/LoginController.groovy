package br.edu.unirn.seguranca

import br.edu.unirn.utils.StringUtils;

class LoginController {

	def auth={}
	
	def autenticar(){
		def login = params.login
		def senha = params.senha
		def senhaMD5 = StringUtils.toMD5(senha)
		def usuario = Usuario.findByLoginAndSenha(login,senhaMD5)
		
		if(usuario?.habilitado){
			usuario.ultimoAcesso = new Date()
			usuario.countAcesso = usuario.countAcesso+1
			usuario.save(flush:true) 
			session.usuario = usuario
			redirect(controller:"index")
		}else{
			flash.mensagem = "Usuario e/ou senha invalido"
			redirect(action:"auth")
		}
	}
	
	def logout(){
		session.invalidate()
		redirect(action:"auth")
	}
	
}
