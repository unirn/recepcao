package br.edu.unirn.chamado

class ChamadoController {

	def scaffold = true
	
	static allowedMethods = [aberturaChamado: "POST"]
	
	def mailService
	
    def aberturaChamado(){
		def usuario = session.usuario
		def titulo = "[RECEPCAO] ${params.titulo} - Usu�rio: ${usuario.nome}"
		def mensagem = "Data Envio: ${new Date()}\nMensagem: ${params.mensagem}\nUser: ${usuario.nome}"
		
		def chamado = new Chamado()
		chamado.titulo = params.titulo
		chamado.descricao = params.mensagem
		chamado.usuario = usuario
		chamado.save(flush:true)
		
		mailService.sendMail {
			to "nucleo_dev_bsi@googlegroups.com"
			from "recepcao@unirn.edu.br"
			bcc "romulofc@unirn.edu.br"
			subject titulo
			html mensagem
		}
		render status:200,text:'Enviado Com sucesso!'
	}
}
