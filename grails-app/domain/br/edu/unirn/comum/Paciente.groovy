package br.edu.unirn.comum

import br.edu.unirn.seguranca.Usuario

class Paciente {
	
	Pessoa pessoa
	VinculoInstituicao vinculoInstituicao
	Clinica encaminhado
	Usuario usuarioCadastro
	Long numeroProntuario
	boolean inativo = false
	
    static constraints = {
		vinculoInstituicao(nullable:false)
		encaminhado(nullable:true)
		numeroProntuario(nullable:true) 
    }
	
	static mapping = {
		pessoa cascade:"all"
	}
	
	String toString(){
		pessoa.nome	
	}
}
