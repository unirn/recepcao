package br.edu.unirn.comum


class Sala {

	String nome
	Clinica clinica
	boolean ativo = true
	
    static constraints = {
		nome(nullable:false,blank:false,unique:true)
		clinica(nullable:false)
    }
	
	static mapping = {
		sort('nome')
	}
	
	String toString(){
		nome	
	}
}
