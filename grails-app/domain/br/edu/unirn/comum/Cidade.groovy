package br.edu.unirn.comum

class Cidade {
	
	String nome
	Estado estado
	
    static mapping = {
		cache true
		version false
    }
	
	String toString(){
		nome + " - " + estado.sigla 
	}

}
