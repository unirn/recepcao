package br.edu.unirn.comum

import br.edu.unirn.tipos.GrauInstrucao
import br.edu.unirn.tipos.Sexo
import br.edu.unirn.utils.StringUtils

class Pessoa {

	String nome
	String nomeAscii
	String nomePai
	String nomeMae
	Integer rg
	Long cpf
	String email
	Sexo sexo
	Date dataNascimento
	String ocupacao
	GrauInstrucao grauInstrucao
	String observacao
	
	//Endere�o
	String logradouro
	String bairro
	Cidade cidade
	String cep
	String numero

	//Contato
	String telefoneContato
	String telefoneCelular
	
	Date dateCreated
	Date lastUpdated
		
    static constraints = {
		nome(nullable:false,blank:false,size:0..200)
		nomePai(nullable:true,size:0..200)
		nomeMae(nullable:true,blank:true,size:0..200)
		ocupacao(nullable:true,blank:true,size:0..200)
		rg(nullable:true)
		cpf(nullable:true)
		email(nullable:true,email:true)
		sexo(nullable:true)
		dataNascimento(nullable:true)
		grauInstrucao(nullable:true)
		logradouro(blank:false,nullable:true)
		bairro(blank:false,nullable:false)
		cidade(blank:true,nullable:true)
		bairro(blank:true,nullable:true)
		telefoneContato(blank:false,nullable:false)
		observacao(blank:true,nullable:true)
		nomeAscii(blank:true,nullable:true)
		cep(nullable:true)
		numero(nullable:true)
		telefoneCelular(nullable:true)
    }
	
	static mapping = {
		nomeAscii(index:'idx_nomeAscii_funcionario')
		sort "nomeAscii"
	}
	
	String toString(){
		nome
	}
	
		
	def beforeInsert() {
		nomeAscii = StringUtils.toAscii(nome)
	}

	def beforeUpdate() {
		if (isDirty('nome')) {
			nomeAscii = StringUtils.toAscii(nome)
		}
	}
}
