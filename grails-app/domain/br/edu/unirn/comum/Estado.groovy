package br.edu.unirn.comum

class Estado {
	
	String nome
	String sigla

    static constraints = {
		nome(nullable:false)
		sigla(nullable:false,unique:true)
		
		
    }
	static mapping ={
		cache(usage:'read-only')
		version false
	}
	
	String toString(){
		"${sigla}"
	}
}
