package br.edu.unirn.seguranca

import br.edu.unirn.comum.Pessoa;
import br.edu.unirn.tipos.TipoUsuario;

class Usuario {

	String nome
	String email
	String login
	String senha
	int countAcesso = 0
	Date ultimoAcesso
	boolean habilitado = true
	TipoUsuario tipoUsuario
	
	static constraints = {
		login blank: false, unique: true
		senha blank: false
		nome blank:false
		ultimoAcesso nullable:true 
		email email:true, blank:false
	}
		
	String toString(){
		nome
	}
	
	/*def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('senha')) {
			encodePassword()
		}
	}*/
}
