package br.edu.unirn.consulta

import java.util.Date;

import br.edu.unirn.seguranca.Usuario;

class AlteracaoStatusConsulta {

	Consulta consulta
	StatusConsulta statusConsultaAnterior
	StatusConsulta statusConsultaAtual
	String motivo
	
	Usuario usuarioMarcacao
	
	Date dateCreated
	
    static constraints = {
    }
}
