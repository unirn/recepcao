package br.edu.unirn.consulta

import br.edu.unirn.comum.Clinica
import br.edu.unirn.tipos.Turno

class Agenda {

	Clinica clinica
	Float duracao
	String horaInicio
	String horaFim
	Turno turno
	boolean ativo = true
	
    static constraints = {
		horaInicio(blank:false,matches:"([01]?[0-9]|2[0-3]):[0-5][0-9]")
		horaFim(blank:false,matches:"([01]?[0-9]|2[0-3]):[0-5][0-9]")
    }
	
	static mapping = {
	}
}
