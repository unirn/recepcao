package br.edu.unirn.consulta

import java.util.Date;

import br.edu.unirn.comum.Paciente
import br.edu.unirn.comum.Sala
import br.edu.unirn.seguranca.Usuario;
import br.edu.unirn.tipos.Turno;
import br.edu.unirn.utils.CalendarUtils;


class Consulta {

	Paciente paciente
	Date dataConsulta
	String horarioConsulta
	Sala sala
	Sala salaIndicacao
	String motivo
	String justificativa
	Float duracao
	StatusConsulta statusConsulta
	Turno turno
	
	Float valor
	StatusPagamento statusPagamento
	
	Usuario usuarioMarcacao
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
		statusConsulta(nullable:true)
		statusPagamento(nullable:true)
		salaIndicacao(nullable:true)
    }
	
}
