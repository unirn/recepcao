package br.edu.unirn.consulta

import br.edu.unirn.tipos.TipoStatusConsulta;

class StatusPagamento {

   String nome
   String corListagem
	
    static constraints = {
		nome(blank:false)
    }
	
	static mapping = {
		cache true
	}
	
	String toString(){
		nome	
	}
}
