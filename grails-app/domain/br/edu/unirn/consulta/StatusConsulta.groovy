package br.edu.unirn.consulta

import br.edu.unirn.comum.Clinica;
import br.edu.unirn.tipos.TipoStatusConsulta;

class StatusConsulta {

	Integer id
	String descricao
	String corListagem
	Clinica clinica
	TipoStatusConsulta tipoStatusConsulta
	
	static mapping = {
		cache(usage:'read-write')
		version false
		id(generator:'assigned')
	}
	
    static constraints = {
		descricao(blank:false)
		clinica(nullable:true)
		id(generator:'assigned')
    }
	
	String toString(){
		descricao	
	}
}
