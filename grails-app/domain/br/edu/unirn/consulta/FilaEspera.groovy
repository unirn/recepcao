package br.edu.unirn.consulta

import br.edu.unirn.comum.Clinica;
import br.edu.unirn.comum.Pessoa
import br.edu.unirn.seguranca.Usuario;
import br.edu.unirn.tipos.StatusFilaEspera;
import br.edu.unirn.tipos.Turno;

class FilaEspera {
	
	String nome
	String motivo
	String telefoneContato
	Integer idade
	Turno turno
	StatusFilaEspera statusFilaEspera
	
	Pessoa pessoa
	Clinica clinica
	
	Usuario usuarioMarcacao
	
	Date dateCreated

    static constraints = {
		pessoa(nullable:true)
		motivo(blank:true)
		nome(blank:true,nullable:true)
		telefoneContato(blank:false)
    }
	
	static mapping = {
		sort "dateCreated", order: "desc"
	}
}
