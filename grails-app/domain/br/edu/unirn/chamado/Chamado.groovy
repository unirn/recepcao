package br.edu.unirn.chamado

import br.edu.unirn.seguranca.Usuario

class Chamado {

	String titulo
	String descricao
	Usuario usuario
	
    static constraints = {
		titulo(blank:true)
		descricao(blank:true)
    }
}
