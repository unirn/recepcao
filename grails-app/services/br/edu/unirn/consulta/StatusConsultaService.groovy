package br.edu.unirn.consulta

class StatusConsultaService {

    def buscarStatusClinica(clinica) {
		def query = {
			or{
				eq("clinica",clinica)
				isNull("clinica")
			}
		}
		if(clinica)
			StatusConsulta.createCriteria().list(query)
		else
			StatusConsulta.getAll()
    }
}
