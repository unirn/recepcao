package br.edu.unirn.comum

class PessoaService {

     def pessoaAutoComplete(params){
		def query = {
			or {
				ilike("nomeAscii", "%${params.term}%")
			}
			projections {
				property("id")
				property("nome")
			}
		}
		def plist = Pessoa.createCriteria().list(query)
		def pessoaList = []
		plist.each {
			def pessoa = [:]
			pessoa.put("id", it[0])
			pessoa.put("label", it[1])
			pessoaList.add(pessoa)
		}
		return pessoaList
	}
}
