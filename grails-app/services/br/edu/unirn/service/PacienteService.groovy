package br.edu.unirn.service

import br.edu.unirn.comum.Paciente
import br.edu.unirn.comum.Pessoa;

class PacienteService {

   def pacienteAutoComplete(params){
		def query = {
			createAlias("pessoa","pessoa")
			ilike("pessoa.nomeAscii", "${params.term}%")
			eq("inativo", false)
			projections {
				property("id")
				property("pessoa.nome")
			}
		}
		def plist = Paciente.createCriteria().list(query)
		def pacienteList = []
		plist.each {
			def paciente = [:]
			paciente.put("id", it[0])
			paciente.put("label", it[1])
			pacienteList.add(paciente)
		}
		return pacienteList
	}

	def obterPaciente(id){
		def query = {
			createAlias("pessoa","pessoa")
			eq("id", id)
			eq("inativo", false)
			projections {
				property("pessoa.nome")
			}
		}
		def plist = Paciente.createCriteria().get(query)

		def paciente = new Paciente()
		paciente.pessoa = new Pessoa()
		paciente.pessoa.nome=plist
		return paciente
	}
	
	def buscaPaciente(params){
		def query = {
			createAlias("pessoa","pessoa")
			eq("inativo", false)
			if(params?.numeroProntuario){
				eq("numeroProntuario",Long.valueOf(params.numeroProntuario))
			}
			if(params?.nome){
				ilike("pessoa.nomeAscii","%"+params.nome+"%")
			}
		}
		
		Paciente.createCriteria().list(query)
		
	}
}
