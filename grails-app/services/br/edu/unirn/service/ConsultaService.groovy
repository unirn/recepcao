package br.edu.unirn.service
import java.text.SimpleDateFormat

import br.edu.unirn.consulta.Consulta
import br.edu.unirn.consulta.StatusConsulta;
import br.edu.unirn.utils.CalendarUtils


class ConsultaService {
	
	SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy")

    def consultaPorPaciente(cpf) {
		def query = {
			createAlias("sala","sala")
			createAlias("sala.clinica","clinica")
			createAlias("paciente","pac")
			createAlias("pac.pessoa","pes")
			eq("pes.cpf",cpf)
			order("dataConsulta", "cresc")
			projections {
				property("id")
				property("pes.nome")
				property("dataConsulta")
				property("horarioConsulta")
				property("clinica.descricao")
				property("statusConsulta")
			}
		}
		
		def consultaLista = Consulta.createCriteria().list(query)
		def resultado = []
		consultaLista.each{ valor ->
			def consulta = [:]
			int idx = 0
			consulta.put("id", valor[idx++])
			consulta.put("paciente", valor[idx++])
			consulta.put("dataConsulta", valor[idx++].toString().substring(0, 10))
			consulta.put("horarioConsulta", valor[idx++])
			consulta.put("clinica", valor[idx++])
			consulta.put("statusConsulta", valor[idx++].toString())
			resultado.add(consulta)
		}
		resultado
    }
	
	
	def buscarConsultaPerido(params){
		def calendarUtils = new CalendarUtils()
		
		def datainicio = calendarUtils.dataInicioDia(new Date().parse("yyyy-MM-dd",params.datainicio))
		def datafim = calendarUtils.dataFimDia(new Date().parse("yyyy-MM-dd",params.datafim))
		
		def idclinica = Long.parseLong(params.idclinica)
		
		def query = {
			createAlias("sala","sala")
			createAlias("sala.clinica","clinica")
			createAlias("paciente","pac")
			createAlias("pac.pessoa","pes")
			eq("sala.clinica.id",idclinica)
			between('dataConsulta', datainicio, datafim)
			order("dataConsulta", "cresc")
		projections {
			property("id")
			property("pes.nome")
			property("dataConsulta")
			property("horarioConsulta")
			property("clinica.descricao")
			property("statusConsulta")
		}
	}
	def consultaLista = Consulta.createCriteria().list(query)
	def resultado = []
	consultaLista.each{ valor ->
		def consulta = [:]
		int idx = 0
		consulta.put("id", valor[idx++])
		consulta.put("paciente", valor[idx++])
		consulta.put("dataConsulta", valor[idx++].toString().substring(0, 10))
		consulta.put("horarioConsulta", valor[idx++])
		consulta.put("clinica", valor[idx++])
		consulta.put("statusConsulta", valor[idx++].toString())
		resultado.add(consulta)
	}
	resultado
   //return Consulta.createCriteria().list(query)
	}
	
	def buscarConsultaPeridoGeral(params){
		def calendarUtils = new CalendarUtils()
		
		def datainicio = calendarUtils.dataInicioDia(new Date().parse("yyyy-MM-dd",params.datainicio))
		def datafim = calendarUtils.dataFimDia(new Date().parse("yyyy-MM-dd",params.datafim))
		
		def idclinica = Long.parseLong(params.idclinica)
		
		def query = {
			createAlias("sala","sala")
			createAlias("sala.clinica","clinica")
			createAlias("paciente","pac")
			createAlias("pac.pessoa","pes")
			between('dataConsulta', datainicio, datafim)
			order("dataConsulta", "cresc")
		projections {
			property("id")
			property("pes.nome")
			property("dataConsulta")
			property("horarioConsulta")
			property("clinica.descricao")
			property("statusConsulta")
		}
	}
	def consultaLista = Consulta.createCriteria().list(query)
	def resultado = []
	consultaLista.each{ valor ->
		def consulta = [:]
		int idx = 0
		consulta.put("id", valor[idx++])
		consulta.put("paciente", valor[idx++])
		consulta.put("dataConsulta", valor[idx++].toString().substring(0, 10))
		consulta.put("horarioConsulta", valor[idx++])
		consulta.put("clinica", valor[idx++])
		consulta.put("statusConsulta", valor[idx++].toString())
		resultado.add(consulta)
	}
	resultado
   //return Consulta.createCriteria().list(query)
	}
	
	def buscarConsultaMes(){
		def calendarUtils = new CalendarUtils()
			
		def query = {
			createAlias("sala","sala")
			createAlias("sala.clinica","clinica")
			createAlias("paciente","pac")
			createAlias("pac.pessoa","pes")
			between("dataConsulta",calendarUtils.dataPrimeiroDiaMes(),calendarUtils.dataUltimoDiaMes())
			order("dataConsulta", "cresc")
		projections {
			property("id")
			property("pes.nome")
			property("dataConsulta")
			property("horarioConsulta")
			property("clinica.descricao")
			property("statusConsulta")
		}
	}
	def consultaLista = Consulta.createCriteria().list(query)
	def resultado = []
	consultaLista.each{ valor ->
		def consulta = [:]
		int idx = 0
		consulta.put("id", valor[idx++])
		consulta.put("paciente", valor[idx++])
		consulta.put("dataConsulta", valor[idx++].toString().substring(0, 10))
		consulta.put("horarioConsulta", valor[idx++])
		consulta.put("clinica", valor[idx++])
		consulta.put("statusConsulta", valor[idx++].toString())
		resultado.add(consulta)
	}
	resultado
	}
	
	def buscarConsultaDia(){
		def calendarUtils = new CalendarUtils()
			
		def query = {
			createAlias("sala","sala")
			createAlias("sala.clinica","clinica")
			createAlias("paciente","pac")
			createAlias("pac.pessoa","pes")
			between("dataConsulta",calendarUtils.dataInicioDia(new Date()),calendarUtils.dataFimDia(new Date()))
			order("dataConsulta", "cresc")
		projections {
			property("id")
			property("pes.nome")
			property("dataConsulta")
			property("horarioConsulta")
			property("clinica.descricao")
			property("statusConsulta")
		}
	}
	def consultaLista = Consulta.createCriteria().list(query)
	def resultado = []
	consultaLista.each{ valor ->
		def consulta = [:]
		int idx = 0
		consulta.put("id", valor[idx++])
		consulta.put("paciente", valor[idx++])
		consulta.put("dataConsulta", valor[idx++].toString().substring(0, 10))
		consulta.put("horarioConsulta", valor[idx++])
		consulta.put("clinica", valor[idx++])
		consulta.put("statusConsulta", valor[idx++].toString())
		resultado.add(consulta)
	}
	resultado
	}
	
	
	def alterarStatusConsulta(params){
		def status = StatusConsulta.get(Long.parseLong(params.idstatus))
		def consulta = Consulta.get(Long.parseLong(params.idconsulta))
		println status.descricao
		consulta.statusConsulta = status
		return 'Consulta:  '+ status.descricao
	}
	
	
	def totalConsultaHoje(){
		def calendarUtils = new CalendarUtils()
		
		def query = {
			between("dataConsulta",calendarUtils.dataInicioDia(new Date()),calendarUtils.dataFimDia(new Date()))
			projections{
				count('id')
			}
		}
		Consulta.createCriteria().get(query)
	}
	
	def totalConsultaMes(){
		def calendarUtils = new CalendarUtils()
		
		def query = {
			between("dataConsulta",calendarUtils.dataInicioDia(new Date()),calendarUtils.dataFimDia(new Date()))
			projections{
				count('id')
			}
		}
		Consulta.createCriteria().get(query)
	}
}
