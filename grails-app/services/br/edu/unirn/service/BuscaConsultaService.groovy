package br.edu.unirn.service

import br.edu.unirn.consulta.Consulta
import br.edu.unirn.consulta.StatusConsulta
import br.edu.unirn.utils.CalendarUtils;

class BuscaConsultaService {

    def buscarConsulta(consultaInstance,clinica,turno,dataFim) {
		def query = {
			createAlias("sala","sala")
			createAlias("sala.clinica","clinica")
			and {
				if(dataFim){
					between("dataConsulta",CalendarUtils.dataInicioDia(consultaInstance.dataConsulta),CalendarUtils.dataFimDia(dataFim))
				}else{
					eq("dataConsulta",consultaInstance.dataConsulta)
				}					
				
				if(consultaInstance?.statusConsulta?.id>0){
					eq("statusConsulta.id",consultaInstance.statusConsulta.id)
				}
				
				if(clinica)
					eq("clinica.id",clinica.id)
					
				if(turno)
					eq("turno",turno)
			}
			order("dataConsulta", "asc")
			order("horarioConsulta", "asc")
		}
		return Consulta.createCriteria().list(query)
    }
}
