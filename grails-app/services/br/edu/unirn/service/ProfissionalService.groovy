package br.edu.unirn.service

import br.edu.unirn.comum.Profissional
import br.edu.unirn.comum.Pessoa;

class ProfissionalService {

	def buscaProfissional(params){
		def query = {
			createAlias("pessoa","pessoa")
			if(params?.id){
				eq("id",Long.valueOf(params.id))
			}
			if(params?.nome){
				ilike("pessoa.nome",params.nome+"%")
			}
		}
		
		Profissional.createCriteria().list(query)
	}
}
