package br.edu.unirn.service

import java.text.SimpleDateFormat

import br.edu.unirn.consulta.Consulta;
import br.edu.unirn.consulta.StatusConsulta
import br.edu.unirn.tipos.TipoStatusConsulta;

class HorarioService {

    def geracaoListaHorario(double duracao, horaInicio, horaFim) {
		def h = (int)Math.floor(duracao)
		def m
		if(h)
			m = (int)((duracao*10)%(h*10))*10
		else
			m = (int)duracao*100
		

		def calInicio = Calendar.instance
		calInicio.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horaInicio.split(":")[0]))
		calInicio.set(Calendar.MINUTE, Integer.parseInt(horaInicio.split(":")[1]))

		def calFim = Calendar.instance
		calFim.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horaFim.split(":")[0]))
		calFim.set(Calendar.MINUTE, Integer.parseInt(horaFim.split(":")[1]))

		def lista = []
		
		while (calInicio.compareTo(calFim)<0){
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm")
			lista.add(sdf.format(calInicio.time))
			calInicio.add(Calendar.MINUTE, m)
			calInicio.add(Calendar.HOUR_OF_DAY, h)
		}
		lista
    }
	
	def verificarDisponibilidadeClinica(clinica,horario,dataConsulta,pacienteID,turno,salaID=null){
			def query = {
				createAlias("sala","sala")
				createAlias("paciente","paciente")
				createAlias("paciente.pessoa","pessoa")
				createAlias("sala.clinica","clinica")
				and{
					eq("turno",turno)
					eq("clinica.id",clinica.id)
					if(salaID!=null)
						eq("sala.id",Long.parseLong(salaID))
					inList("statusConsulta",StatusConsulta.findAllByTipoStatusConsulta(TipoStatusConsulta.CONFIRMACAO))
					eq("horarioConsulta",horario)
					eq("dataConsulta",dataConsulta)
					if(pacienteID)
						ne("paciente.id",pacienteID)
					}
				maxResults(1)
				projections {
					property("pessoa.nome")
				}
			}
			def nome = Consulta.createCriteria().get(query)
			return nome
	}
}
