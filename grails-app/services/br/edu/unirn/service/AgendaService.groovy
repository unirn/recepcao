package br.edu.unirn.service

import br.edu.unirn.consulta.Agenda

class AgendaService {

    def buscarAgenda(turnoIN,clinicaID) {
		//println clinicaID
		def queryAgenda ={
			and{
				eq("turno",turnoIN)
				if(clinicaID)
					eq("clinica.id",Long.parseLong("${clinicaID}"))
				
			}
		}
		def agenda = Agenda.createCriteria().get(queryAgenda)
		return agenda

    }
}
