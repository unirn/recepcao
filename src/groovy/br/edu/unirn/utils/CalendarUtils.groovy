package br.edu.unirn.utils

class CalendarUtils {

	static Date dataInicioDia(data){
		def ymdFmt = new java.text.SimpleDateFormat("yyyy-MM-dd")
		def dateYmd = ymdFmt.format(data)
		def dateTimeFormat = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		def startDate = dateTimeFormat.parse("${dateYmd} 00:00:00");
		startDate
	}
	
	static Date dataFimDia(data){
		def ymdFmt = new java.text.SimpleDateFormat("yyyy-MM-dd")
		def dateYmd = ymdFmt.format(data)
		def dateTimeFormat = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		def endDate = dateTimeFormat.parse("${dateYmd} 23:59:59");
		endDate
	}
	
	static Date dataPrimeiroDiaMes(){
        def calendar = GregorianCalendar.instance
        calendar.setTime(new Date());  
        calendar.set(Calendar.DAY_OF_MONTH, 1);  
  
        calendar.getTime();
	}
	
	static Date dataUltimoDiaMes(){
		Calendar calendar = GregorianCalendar.instance
		calendar.setTime(new Date())
		Calendar retorno = GregorianCalendar.instance
		def lastDay = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)
		retorno.set(Calendar.DATE, lastDay)
		retorno.getTime()
	}
	
}
