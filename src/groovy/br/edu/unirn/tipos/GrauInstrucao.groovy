package br.edu.unirn.tipos

enum GrauInstrucao {
	NM("Nenhum"),
	AF("Analfabeto"),
	FC("Fundamental Completo"),
	FI("Fundamental Incompleto"),
	MI("Medio Incompleto"),
	MC("Medio Completo"),
	SI("Superior Incompleto"),
	SC("Superior Completo"),
	ME("Mestrado"),
	DR("Doutorado"),
	OU("Outros")

	String descricao

	GrauInstrucao(String descricao){
		this.descricao = descricao
	}

	String toString(){
		descricao
	}
}
