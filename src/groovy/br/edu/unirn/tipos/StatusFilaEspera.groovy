package br.edu.unirn.tipos

enum StatusFilaEspera {

	AGUARDANDO("Aguardando"),DESISTIU("Desistiu"),MARCOU("Marcou"),SEM_CONTATO("Sem contato")
	
	String nome
	
	public StatusFilaEspera(String nome){
		this.nome = nome
	}
	
	String toString(){
		nome
	}
	
}
