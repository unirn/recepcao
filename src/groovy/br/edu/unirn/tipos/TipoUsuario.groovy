package br.edu.unirn.tipos

enum TipoUsuario {
	
	ADMINISTRADOR("Administrador"),COMUM("Comum"),RECEPCAO("Recepcao"),COORDENACAO_RECEPCAO("Coord. Recepcao"),COORDENACAO("Coordenacao"),
	ASSISTENTE_SOCIAL("Assit. Social"),PROFESSOR("Professor"),ALUNO("Aluno")
	
	private String descricao
	
	TipoUsuario(descricao){
		this.descricao = descricao
	}
}
