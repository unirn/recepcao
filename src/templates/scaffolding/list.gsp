<% import grails.persistence.Event %>
<%=packageName%>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="list-${domainClass.propertyName}" class="contentCrud" role="main">
			<div class="box corner-all">
					<div class="box-header grd-white color-black corner-top">
						<span><g:message code="default.list.label" args="[entityName]" /></span>
					</div>
					<div class="box-body">
						<g:if test="\${flash.message}">
							<div class="alert" role="status">
								 <button type="button" class="close" data-dismiss="alert">&times;</button>
								 <strong>\${flash.message}</strong>
							</div>
						</g:if>
						<table class="table table-hover table-condensed table-bordered">
							<thead>
								<tr>
								<%  excludedProps = Event.allEvents.toList() << 'id' << 'version'
									allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
									props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) && it.type != null && !Collection.isAssignableFrom(it.type) }
									Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
									props.eachWithIndex { p, i ->
										if (i < 6) {
											if (p.isAssociation()) { %>
									<th><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}" /></th>
								<%      } else { %>
									<g:sortableColumn property="${p.name}" title="\${message(code: '${domainClass.propertyName}.${p.name}.label', default: '${p.naturalName}')}" />
								<%  }   }   } %>
								<th>Editar</th>
								<th>Remover</th>
								</tr>
							</thead>
							<tbody>
							<g:each in="\${${propertyName}List}" var="${propertyName}">
								<tr>
								<%  props.eachWithIndex { p, i ->
										if (i == 0) { %>
									<td><g:link action="show" id="\${${propertyName}.id}">\${fieldValue(bean: ${propertyName}, field: "${p.name}")}</g:link></td>
								<%      } else if (i < 6) {
											if (p.type == Boolean || p.type == boolean) { %>
									<td><g:formatBoolean boolean="\${${propertyName}.${p.name}}" /></td>
								<%          } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
									<td><g:formatDate date="\${${propertyName}.${p.name}}" format="dd/MM/yyyy" /></td>
								<%          } else { %>
									<td>\${fieldValue(bean: ${propertyName}, field: "${p.name}")}</td>
								<%  }   }   } %>
									<td><g:link class="btn" action="edit" id="\${${propertyName}?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link></td>
									
									<td>
										<g:form>
											<g:hiddenField name="id" value="\${${propertyName}?.id}" />
											<g:actionSubmit class="btn" action="delete" value="\${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('\${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
										</g:form>
									</td>
								</tr>
							</g:each>
							</tbody>
					</table>
					<div class="pagination pagination-centered">
						<g:paginate total="\${${propertyName}Total}" />
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
