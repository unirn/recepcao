package br.edu.unirn.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class StringUtils {

	static final String PLAIN_ASCII = 
			"AaEeIiOoUu" // grave
			+ "AaEeIiOoUuYy" // acute
			+ "AaEeIiOoUuYy" // circumflex
			+ "AaIiOoUuNn" // tilde
			+ "AaEeIiOoUuYy" // umlaut
			+ "Aa" // ring
			+ "Cc" // cedilla
	;

	static final String UNICODE = 
			"\u00C0\u00E0\u00C8\u00E8\u00CC\u00EC\u00D2\u00F2\u00D9\u00F9" // grave
			+ "\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DD\u00FD" // acute
			+ "\u00C2\u00E2\u00CA\u00EA\u00CE\u00EE\u00D4\u00F4\u00DB\u00FB\u0176\u0177" // circumflex
			+ "\u00C3\u00E3\u0128\u0129\u00D5\u00F5\u0168\u0169\u00D1\u00F1" // tilde
			+ "\u00C4\u00E4\u00CB\u00EB\u00CF\u00EF\u00D6\u00F6\u00DC\u00FC\u0178\u00FF" // umlaut
			+ "\u00C5\u00E5" // ring
			+ "\u00C7\u00E7" // cedilla
	;
	/** 
	 * Converte a string para ascii
	 */
	public static String toAscii(String valorEntrada) {
		if (valorEntrada == null) {
			return valorEntrada;
		}
		StringBuffer sb = new StringBuffer();
		int n = valorEntrada.length();
		for (int i = 0; i < n; i++) {
			char c = valorEntrada.charAt(i);
			int pos = UNICODE.indexOf(c);
			if (pos > -1) {
				sb.append(PLAIN_ASCII.charAt(pos));
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}
	
	public static String toMD5(String senha){
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance( "MD5" );
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		md.update( senha.getBytes() );  
		BigInteger hash = new BigInteger( 1, md.digest() );  
		return hash.toString(16);
	}
	

}
