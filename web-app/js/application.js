$(document).ready(
		function() {
			//Config Geral
			configuracoesGerais.init();
			$(".formValidate").validate();
			
			
			$('[name*=telefone]').mask('(99) 9999-9999')
			$('[name*=cep]').mask('99999-999')
			$('[name^=hora]').mask('99:99')
			$('input[name^=data][type=text]').mask('99/99/9999')
			$('input#corListagem').colorpicker();
			$("input[name^='valor']").maskMoney({thousands:'.', decimal:','})
			
			
			
			$('#dialogAlteraConsulta').dialog({
				autoOpen : false,
				create: function(event,ui){
					$(".ui-dialog-titlebar-close").html("&times;")
				},
				height : 370,
				width : 450,
				modal : true,
				resizable: false
			});
			
			$('#abrirChamado').dialog({
				autoOpen : false,
				create: function(event,ui){
					$(".ui-dialog-titlebar-close").html("&times;")
				},
				height : 370,
				width : 450,
				modal : true,
				resizable: false
			});
			
			$("#modalCadastroPaciente").dialog({
				autoOpen : false,
				create: function(event,ui){
					$(".ui-dialog-titlebar-close").html("&times;")
				},
				height : 300,
				width : 450,
				modal : true,
				resizable: false
			});
			
			$('#alteraPagamentoConsulta').dialog({
				autoOpen : false,
				create: function(event,ui){
					$(".ui-dialog-titlebar-close").html("&times;")
				},
				height : 250,
				width : 450,
				modal : true,
				resizable: false
			});
			
			$('#terminarCadastroPaciente').dialog({
				autoOpen : false,
				create: function(event,ui){
					$(".ui-dialog-titlebar-close").html("&times;")
				},
				height : 250,
				width : 450,
				modal : true,
				resizable: false
			});
			
			$("#linkAbrirChamado").click(function() {
				$("#abrirChamado").dialog("open");
			});
			
			$("#linkCadastroPaciente").click(function() {
				$("#modalCadastroPaciente").dialog("open");
			});
			
			$("#cadastarPacienteRapido").submit(function(e){
				e.preventDefault();
				var nome = $("#cadastarPacienteRapido #nome").val().length
				var telefone = $("#cadastarPacienteRapido #telefoneContato").val().length
				
				if(nome>0 && telefone>0){
					$.ajax({
						data: $("#cadastarPacienteRapido").serialize(),
						url: "/recepcao/paciente/cadastroRapido",
						cache:false,
						type:"POST",
						success: function(data){
							$("form #paciente").val(data.id)
							$("form #pacienteAutoComplete").val(data.nome)
							$("#modalCadastroPaciente").dialog("close");
							resetForm("#cadastarPacienteRapido");
						}
					})
					
				}
			});
			
			
			if($('#dadosConsulta').length){
				var valorClinica = $('#dadosConsulta #clinica option:selected').val()
				var valorSala =  $('#dadosConsulta #sala option:selected').val()
				if(valorClinica>0){
					atualizarComboSala(valorClinica,'sala',valorSala);
				}
				var valorClinicaIndicacao = $('#dadosConsulta #clinicaIndicacao option:selected').val();
				var valorSalaIndicacao =  $('#dadosConsulta #salaIndicacao option:selected').val()
				if(valorClinicaIndicacao>0){
					atualizarComboSala(valorClinicaIndicacao,'salaIndicacao',valorSalaIndicacao);
				}
				atualizarComboHorario();
			}
			//Mostrar campos se o papel escolhido for igual a Aluno no cadastro do profissional
			$('.papelAluno').hide();
			$('.papelAlunoProfessor').hide();
			$('.papel').change(function() {
				 var op = $('option:selected',this).val();
				
				 if(op == 1){
					 	$('.papelAlunoProfessor').show();
				        $('.papelAluno').show();
				 }else{
					 if(op==2){
						 $('.papelAlunoProfessor').show();
						 $('.papelAluno').hide();
					 }else{
						  $('.papelAluno').hide();
						  $('.papelAlunoProfessor').hide();
						  $(".papelAlunoProfessor option:eq(0)").attr("selected","selected");
						  $(".papelAluno option:eq(0)").attr("selected","selected");
						  
					 }
					 
				 }
				 
				 
		});
		
	});
$('#dadosConsulta #clinica').change(function(){
	atualizarComboHorario();
	var valor = $('#dadosConsulta #clinica option:selected').val();
	atualizarComboSala(valor,'sala','null');
});

$('#dadosConsulta #turno').change(function(){
	atualizarComboHorario();
});

$('#dadosConsulta #sala').change(function(){
	atualizarComboHorario();
});

$('#dadosConsulta #clinicaIndicacao').change(function(){
	var valor = $('#dadosConsulta #clinicaIndicacao option:selected').val();
	atualizarComboSala(valor,'salaIndicacao','null');
});

$('#agenda-paciente #clinicaID').change(function(){
	var clinica = $('#agenda-paciente #clinicaID').val();
	var listaStatus = $('#agenda-paciente #statusConsulta')
	atualizarComboStatusConsulta(clinica,listaStatus);
});

function atualizarComboStatusConsulta(value,destino) {
	$.ajax({
		url : "/recepcao/statusConsulta/listaStatusConsulta",
		data :"id=" + value,
		cache : false,
		success : function(html) {
			destino.html(html);
		}
	});
}

function atualizarComboSala(value,nameDest,valorDefault) {
	if(value!="null"){
		$.ajax({
			url : "/recepcao/sala/gerarComboSalaClinica",
			data :"name=" + nameDest + "&id=" + value,
			cache : false,
			success : function(html) {
				if (html != "null") {
					$("#dadosConsulta #"+nameDest).html(html);
					$("#dadosConsulta #"+nameDest).show();
					$("#dadosConsulta #"+nameDest+" option[value='"+valorDefault+"']").attr("selected", "selected");
				}else{
					$("#dadosConsulta #"+nameDest).hide();
				}
			}
		});
	}else{
		$("#dadosConsulta #"+nameDest).hide();
	}
}

function atualizarComboHorario() {
	var clinica = $('#dadosConsulta #clinica').val();
	var sala = $('#dadosConsulta #sala').val();
	var dataConsulta =   $("#dataConsulta").val();
	var turno = $('#dadosConsulta #turno').val();
		

	if (clinica != "null" && dataConsulta != "//" && sala!="null") {
		$.ajax({
			url : "/recepcao/horario/gerarComboHorario",
			data :"dataConsulta=" + dataConsulta + "&clinicaID=" + clinica + "&turno=" +turno+ "&salaID=" +sala,
			cache : false,
			success : function(html) {
				if (html != "null") {
					$("#dadosConsulta #horarioConsulta").html(html);
					$("#dadosConsulta #horarioConsulta").removeAttr('disabled')
				}else{
					var html="<select disabled='disabled' name='horarioConsulta' id='horarioConsulta'>"
					html+= "<option>Defina Agenda Clinica/Data/Turno</option>"
					html+= "</select>"
					html+= "<a>Definir Agenda</a>"
					$("#dadosConsulta #horarioConsulta").html(html);
					$("#dadosConsulta #horarioConsulta").attr('disabled','disabled')
				}
			}
		});
	}else if(sala=="null"){
			$("#dadosConsulta #horarioConsulta").attr('disabled','disabled')
			$("#dadosConsulta #horarioConsulta").empty()
	}
}

function alterarStatusConsulta(idStatus,idConsulta){
	$("#dialogAlteraConsulta #consulta").val(idConsulta);
	$("#dialogAlteraConsulta #statusConsultaAnterior").val(idStatus);
	$("#statusConsultaAtual option[value="+idStatus+"]").remove();
	$("#dialogAlteraConsulta").dialog("open");
}

function alterarPagamentoConsulta(idConsulta){
	$("#frmAlteraPagamento #consulta").val(idConsulta);
	$.ajax({
		url : "/recepcao/consulta/obterDadosConsulta",
		data :"id="+idConsulta,
		cache : false,
		success : function(json) {
				$("#frmAlteraPagamento #valor").val(json.valor);
				$("#frmAlteraPagamento #valorAtual").html(json.valor);
				$("#frmAlteraPagamento #statusPagamento option[value='"+json.statusPagamento.id+"']").attr("selected", "selected");
				$("#alteraPagamentoConsulta").dialog("open");
			}
		}
	)
}



function closeDialog(idDialog) {
	$(idDialog).dialog("close");
}

function resetForm(idForm) {
	$(idForm).each(function() {
		this.reset();
	});
}

function refreshPage() {
	location.reload();
}

function fecharAlterarStatusConsulta(id,data){
	closeDialog(id);
	var $td = $("#Consulta"+data.consulta).find("td:last");
	$td.css("background-color",data.cor);
	$td.find("a").html(data.status);
}

function fecharAlterarStatusPagamento(id,data){
	if(data.statusPaciente){
		closeDialog(id);
		var $td = $("#Consulta"+data.consulta).find("td#consulta-"+data.consulta);
		$td.css("background-color",data.cor);
		$td.find("a").html(data.status);
	}else{
		closeDialog(id);
		var $btn = $("#btnConcluirCadastro"),
			href = $btn.attr('href')+ "/"+data.pacienteId;		
		$btn.attr("href",href);
		$('#terminarCadastroPaciente').dialog("open")
		
	}

}

function removeMaskMoney(){
	$("input[name^='valor']").val($("input[name^='valor']").maskMoney('unmasked')[0]);
}

var configuracoesGerais = {
		init : function(){
			$("a.abrirBuscaAvancada").click(this.btnBuscaAvancada);
			$("input#checkTodos").click(this.listnerCheckTodos);
			$("button#deleteChecked").click(this.btnDeleteChecked);
			$("button.linkDelete").click(this.btnDeleteEdit);
		},
		ativarMenu: function(menu){
			$("ul.mainnav").find(".active").removeClass("active");
			$("ul.mainnav li#"+menu).addClass("active");
		},
		btnBuscaAvancada : function(){
			if($("fieldset.form-buscaAvancada").hasClass("in")){
				$("fieldset.form-buscaAvancada").removeClass("in");
				$("fieldset.form-buscaAvancada").fadeOut("fast");
			}else{
				$("fieldset.form-buscaAvancada").addClass("in");
				$("fieldset.form-buscaAvancada").fadeIn("fast");
			}
			
		},
		listnerCheckTodos : function(){
			if($(this).attr("checked") == "checked"){
				$("input.checkSimples").each(function(){
					$(this).attr("checked",true);
				})
			}else{
				$("input.checkSimples").each(function(){
					$(this).attr("checked",false);
				})
			}
		},
		btnDeleteChecked : function(){
			if(confirm("Deseja Inativar Registro?")){
				var qntd = $("input.checkSimples:checked").size();
				var controller = $(this).data("controller");
				var formData = new FormData();
				var trId = [];
				var qntdAnt = $("p#qntdRegistro").html().split(" - ")[1];
				$("input.checkSimples:checked").each(
						function(){ 
							formData.append($(this).attr("data-campo"),$(this).attr("data-registroId"));
							trId.push($(this).attr("data-registroId"));
				});
				formData.append("check",true);
				if(qntd > 0 ){
					$("p#qntdRegistro").text("Quantidade - "+(qntdAnt-qntd));
					$.ajax({
						url:"/recepcao/"+controller+"/deleteList",
						data:formData,
						type : 'POST',
						contentType : false,
						processData : false,
						success:function(data){
							$.each(trId, function(index, value) {
								$("tr#"+value).remove();
							});
						},
						error: function(data){
						}
					});
				}else{
					$("#popoverAlert").popover({
						html:true,
						title:"<span class='icon-exclamation-sign'><span> Aten\xE7\xE3o!",
						content:"Selecione ao menos um Registro.",
						container:"fieldset",
						placement:"top"
					});
					$("#popoverAlert").popover('show');
					setTimeout(function(){
						$("#popoverAlert").popover('hide');
					},2000);
				}
			}
		},
		btnDeleteEdit : function(){
			if(confirm("Deseja Inativar Registro?")){
				$("form[name='delete']").submit();
			}
		}
}

